/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Utils.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 11:30 AM
 */

#ifndef UTILS_H
#define UTILS_H

#define _USE_MATH_DEFINES

#include <opencv2/opencv.hpp>
#include <opencv2/line_descriptor.hpp>

#include <vector>
#include <stdio.h>
#include <math.h>

#include "math/Point2D.h"
#include "cv/FMatch.h"
//#include "cv/Detector.h"
//#include "math/Shape2D.h"
//#include "math/Matrix.hpp"



#include <iostream>
#include <opencv2/core/core.hpp>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#define RAD2DEG(rad) (((rad) * 180)/M_PI)
#define DEG2RAD(deg) (((deg) * M_PI)/180)
#define FOV2FOCAL(pixelssensorsize, fov) ((pixelssensorsize) / (2 * tan((fov) / 2)))// http://books.google.co.il/books?id=bXzAlkODwa8C&pg=PA48&lpg=PA48&dq=expressing+focal+length+in+pixels&source=bl&ots=gY4972kxAC&sig=U1BUeNHhOHmYIrDrO0YDb1DrNng&hl=en&sa=X&ei=45dLU9u9DIyv7QbN2oGIDA&ved=0CGsQ6AEwCA#v=onepage&q=expressing%20focal%20length%20in%20pixels&f=false


//#define LOG_TAG "Utils"
//#ifdef _DEBUG
#ifdef _ANDROID
#include <android/log.h>
#define LOG_TAG "Hologram/recognition/TemplateDetector"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
//#endif



using namespace std;

namespace sapphire {
    class Matrix;
    class Feature2D;
    class Image;


    namespace utils {

        int ConvertFromOCV(vector<cv::KeyPoint> & _keyPointsOCV, vector<Feature2D>& _keyPoints);
        int ConvertToOCV(vector<Feature2D>& _keyPoints, vector<cv::KeyPoint> & _keyPointsOCV);

        int ConvertFromOCV(vector<cv::line_descriptor::KeyLine> & _keyLinesOCV, vector<Feature2D>& _keyLines);
        int ConvertToOCV(vector<Feature2D>& _keyLines, vector<cv::line_descriptor::KeyLine> & _keyLinesOCV);

        int ConvertFromOCV(cv::Mat & _matrixOCV, Matrix& _matrix);
        int ConvertToOCV(Matrix& _matrix, cv::Mat & _matrixOCV);

        int ConvertFromOCV(cv::KeyPoint& _keyPointOCV, Feature2D& _keyPoint);
        int ConvertToOCV(Feature2D& _keyPoint, cv::KeyPoint & _keyPointOCV);

        int ConvertFromOCV(cv::line_descriptor::KeyLine& _keyLineOCV, Feature2D& _keyLine);
        int ConvertToOCV(Feature2D& _keyLine, cv::line_descriptor::KeyLine & _keyLineOCV);

        int ConvertFromOCV(cv::Point2f& _point2DfOCV, Point2D<float>& _point2Df);

        int ConvertFromOCV(std::vector<cv::Point2f> & _points2DfOCV,std::vector<Point2D<float> >& _points2Df);


//        int ConvertFromOCV(std::vector<cv::Point2f> & _points2DfOCV, Shape2D& _shape2D);

        int ConvertToOCV(Point2D<float>& _point2Df, cv::Point2f& _point2DfOCV);


        int ConvertToOCV(Image& _image, cv::Mat & _imageOCV);

        int ConvertToOCV(vector<FMatch>& _matches, vector<cv::DMatch> & _matchesOCV);

        int ConvertToOCV(std::vector<Point2D<float> >& _points2Df, std::vector<cv::Point2f> & _points2DfOCV);


        //        int ConvertMeshToCAO(sapphire::Mesh& _objToConvert, string _caoString);


        int LoadImageFromFile(string _filename, int _imageFormat, Image& _outImage);

        int FeaturesClustering(vector<FMatch>& _matches, std::vector<Feature2D>& _queryKeyPoints, std::vector<Feature2D>& _trainKeyPoints, map<int, vector< pair<Point2D<float>, Point2D<float> > > >& _outClusters, vector<Matrix> & _outlabelsVect, int _branching = 32, int _iterations = 11, cvflann::flann_centers_init_t _centers_init = cvflann::FLANN_CENTERS_RANDOM, float _cb_index = 0.2);

        bool HomographyIsGood(Matrix& _H);
        int EstimateHomography(vector<Point2D<float> >& _srcPoints, vector<Point2D<float> >& _dstPoints, Matrix& _outH, int _method = 0, double _ransacReprojThreshold = 3, int _maxIters = 2000, double _confidence = 0.995);

        int HomographyProject(Matrix& _H, vector<Point2D<float> >& _pointsToProject, vector<Point2D<float> >& _projectedPoints);


        Matrix CreateRTMatrix(Matrix & _rtVector);
        Matrix CreateRTVector(Matrix & _rtMatrix);
        Matrix ConvertQuaternionToRT(Matrix& _quaternion);
        Matrix ConvertQuaternionToEuler(Matrix& _quaternion);
        Matrix ComputeRTMean(Matrix& _rt1,Matrix& _rt2,double _weight1=1.0,double _weight2=1.0);
        Matrix ComputeRTMean(deque<Matrix>& _rts);

        Matrix ConvertRHSToLHS(Matrix & _rhs);


		int ConvertMat2Tex(Matrix& _image, Matrix& _texture2D, bool _textureWithAlpha);
		int ConvertTex2Mat(Matrix& _texture2D, Matrix& _image, bool _textureWithAlpha);

        int SwapPoints2D(vector<Point2D<float> >& _p1, vector<Point2D<float> >& _p2);

        int ComputeStatistics(vector<Feature2D> & _keyPoints,Image& _image,int _numHBuckets,int _numVBuckets,vector<int>& _numValuesOut,vector<Point2D<float> > & _meanOut,vector<Point2D<float> >& _varianceOut,vector<Point2D<float> >& _meanDist,vector<Point2D<float> >& _normalizedVar);
        int EvaluateReferenceObjectQuality(string _yamlConfig,string _sectionName,Image &_texture,int _numHBuckets,int _numVBuckets,vector<Feature2D> & _keypointsOut,vector<int>& _numValuesOut,vector<Point2D<float> >& _meanValuesOut,vector<Point2D<float> > & _varValuesOut,Matrix & _densitiesOut, int _desideredNumFeatures,double & _entropyOut,vector<Point2D<float> >& _meanDist,vector<Point2D<float> >& _normalizedVar);
        int ComputeDistribution(vector<Feature2D> & _keypointsOut,Image & _texture,Matrix & _densitiesOut,int _desideredNumFeatures,double &_entropyOut);
        int DrawStatistics(Image& _image,vector<sapphire::Feature2D>& _keypoints,vector<int>& _numValues,vector<Point2D<float> >& _meanValues, vector<Point2D<float> >& _varValues,Matrix & _densities,Image& _imageOut,int _drawType);

        //        int DisplayShapes2D(Mesh& _meshToProject, Matrix& _transform,Image& _outImg,Camera& _camera=Camera(),int _uniqueFaceId=-1);

    };


}



#endif /* UTILS_H */


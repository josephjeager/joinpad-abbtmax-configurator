/*
 *    Copyright 2016 Anand Muralidhar
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

#ifndef My_LOGGER_H
#define My_LOGGER_H

//#include <android/log.h>
#ifdef _ANDROID_
#define LOG_TAG "AssimpAndroid"
#define  MyLOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  MyLOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define  MyLOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG,__VA_ARGS__)
#define  MyLOGI(...)  __android_log_print(ANDROID_LOG_INFO   , LOG_TAG,__VA_ARGS__)
#define  MyLOGW(...)  __android_log_print(ANDROID_LOG_WARN   , LOG_TAG,__VA_ARGS__)
#define  MyLOGF(...)  __android_log_print(ANDROID_LOG_FATAL   , LOG_TAG,__VA_ARGS__)
#define  MyLOGSIMPLE(...)
#else
#define  MyLOGD(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGE(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGV(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGI(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGW(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGF(...)  printf(__VA_ARGS__);printf("\n")
#define  MyLOGSIMPLE(...);printf("\n")
#endif

#endif //My_LOGGER_H

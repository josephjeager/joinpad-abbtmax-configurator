//
// Created by marzorati on 7/13/17.
//

#ifndef INFO_H
#define INFO_H

#include <vector>
#include <iostream>
#include <iomanip>

namespace sapphire {

    class Info {
    public:

        Info(std::string _stringInfo=""/*,vector<float> _floatVecInfo=0*/) {
            stringInfo_ = _stringInfo;
            //            floatVecInfo_=_floatVecInfo;
        }


        virtual ~Info(){};

    public:

        std::string stringInfo_;
        //        vector<float> floatVecInfo_;
    };

}

#endif //INFO_H

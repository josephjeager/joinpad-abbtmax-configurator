/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DetectorFactory.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 11:06 PM
 */

#ifndef DETECTORFACTORY_H
#define DETECTORFACTORY_H


#include "OCVFeatureDescriptor.h"
#include "LBDFeatureDescriptor.h"

#include "OCVFeatureDetector.h"
#include "LBDFeatureDetector.h"
#include "LSDFeatureDetector.h"

#define OCVFEATUREDETECTOR      1
#define LBDFEATUREDETECTOR      2
#define LSDFEATUREDETECTOR      3


#define OCVFEATUREDESCRIPTOR    1
#define LBDFEATUREDESCRIPTOR    2



namespace sapphire {

    class DetectorFactory {
    public:

        static FeatureDetector* CreateFeatureDetector(int _featureDetectorType) {
            switch (_featureDetectorType) {
                case OCVFEATUREDETECTOR:
                    return new OCVFeatureDetector;
                    break;
//                    case LBDFEATUREDETECTOR:
//                    return new LBDFeatureDetector;
//                    break;
//                    case LSDFEATUREDETECTOR:
//                    return new LSDFeatureDetector;
//                    break;
            }
            throw "DetectorFactory::invalid featureDetectortype.";
        }

        static FeatureDescriptor* CreateFeatureDescriptor(int _featureDescriptorType) {
            switch (_featureDescriptorType) {
                case OCVFEATUREDESCRIPTOR:
                    return new OCVFeatureDescriptor;
                    break;
//                    case LBDFEATUREDESCRIPTOR:
//                    return new LBDFeatureDescriptor;
//                    break;
            }
            throw "DetectorFactory::invalid featureDescriptortype.";
        }

    };

}

#endif /* DETECTORFACTORY_H */


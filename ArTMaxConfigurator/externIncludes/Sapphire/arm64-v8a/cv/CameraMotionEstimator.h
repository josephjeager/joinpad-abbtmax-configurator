/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraMotionEstimator.h
 * Author: marzorati
 *
 * Created on June 14, 2017, 1:05 PM
 */

#ifndef CAMERAMOTIONESTIMATOR_H
#define CAMERAMOTIONESTIMATOR_H

#include <thread>
#include <mutex>


#include "cv/Detector.h"
#include "cv/Matcher.h"
#include "math/Matrix.hpp"
#include "math/Point2D.h"
#include "image/Image.h"
#include "cv/Matcher.h"
#include "math/Homography.h"
#include "math/Shape2D.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"
#include "math/Mesh.h"
#include "cv/Features2DAndDescriptors.hpp"
#include "cv/DetectorFactory.h"
#include "cv/MatcherFactory.h"
#include "cv/Camera.h"


#define NUM_FRAME_ANCHOR    5
#define MIN_NUM_POINTS      200

//#define _DRAW

namespace sapphire
{

class CameraMotionEstimator : public ProcessingNode
{
public:
    CameraMotionEstimator(/*string _configFilename=""*/);

    virtual ~CameraMotionEstimator();


    virtual int InitProc();
    virtual int StartThreadedProc();
    virtual int StopThreadedProc();
    virtual int StartProc();
    virtual int StopProc();

    virtual int SetCurrentFrame(Image& _currentFrame)
    {
        int ret = 0;
        LOGD("CameraMotionEstimator::Setting current frame...");
        mutex_.lock();
        currentFrame_ = _currentFrame;
        isValidFrame_ = true;
        mutex_.unlock();
        LOGD("CameraMotionEstimator::Setting current frame...Done %d", ret);
        return ret;
    }



    virtual int ProcessFrame(Image& _image, Image& _mask, bool _initialGuess = false, int _maxNumMatches = 50, int _branching = 2, int _iterations = 1000);
    virtual int ProcessFrame(Image& _image, bool _generateMask = false, bool _initialGuess = false, int _maxNumMatches = 50, int _branching = 2, int _iterations = 1000);

    virtual int ResetHomography();

    virtual int SetInitialHomography(Matrix & _initHomography);
    
    virtual int GetLastTimestamp(double & _timestamp)
    {
        int ret = 0;
        _timestamp = lastTimestamp_;
        return ret;
    }

    virtual int ProcessKeyFrame(Image& _imageGray, bool & _resetHomographyOut, Image& _mask);

    virtual int SetRefObject(vector<Point2D<float> >& _refObj);

    virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0, double _p1 = 0, double _p2 = 0)
    {
        return camera_.SetCameraParams(_fx, _fy, _cx, _cy, _hFOV, _vFOV, _frameWidth, _frameHeight, _k1, _k2, _k3, _p1, _p2);
    }


    virtual multimap<int, Matrix> GetTransforms(bool& _outIsValid, double &_timestamp);

    virtual multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double &_timestamp);

protected:
    virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    virtual int RunProc();

private:
    int ExtractFeatures(Image& _imageGray, std::vector<cv::Point2f>& _pointsOut,Image & _mask);
    int ExtractHiLevelFeatures(Image& _image, std::vector<cv::Point2f>& _pointsOut,Image& _mask);
    int ComputeHomography(Homography& _homography, vector<cv::Point2f>& _points2d0, vector<cv::Point2f>& _points2d1);
    int ExtractHomographyFromRT(Matrix& _R,Matrix& _t, Camera& _camera,Homography & _homography);
    
public:

    double lastTimestamp_;
    Image currentFrame_;
    Image image_, imageGray_, prevImageGray_;
    Image imageGrayRef0_, imageGrayRef1_;
    Image imageOutMatches_, imageOut_;


    Matrix homography_;
    Matrix prevHomography_;
    Matrix homographyRef_;
    Matrix prevHomographyRef_;
    
    Matrix initHomography_;

    Camera camera_;

    int maxCount_;
    double qualityLevel_;
    int minDistance_;
    double harrisK_;
    int blockSize_;
    bool useHarrisDetector_;
    int subPixWinSize_;
    int pyrMaxLevel_;
    double minEigThreshold_;
    int winSize_;
    bool enableRobustTracking_;
    bool recomputeNewPointsForEachLoop_;


    bool first_;
    int numFrame_;
    bool isValidFrame_, isValidOutputData_;
    int featureMatcherType_, featureDescriptorType_, featureDetectorType_;
    string featureMatcherConfigFilename_, featureDetectorConfigFilename_, featureDescriptorConfigFilename_;
    Detector detector_;
    Matcher matcher_;
    vector<Point2D<float> > refObj_;

    multimap<int, Matrix > oldTransforms_, transforms_;

    Features2DAndDescriptors features2DAndDescriptors0_, features2DAndDescriptors1_;

    std::vector<cv::Point2f> points[2];

};

}

#endif /* CAMERAMOTIONESTIMATOR_H */


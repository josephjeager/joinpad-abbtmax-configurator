/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VISPPoseEstimator.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:40 PM
 */
#ifdef USE_VISP


#ifndef VISPPOSEESTIMATOR_H
#define VISPPOSEESTIMATOR_H

#include <opencv2/opencv.hpp>
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/vision/vpKeyPoint.h>
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/gui/vpDisplayOpenCV.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/io/vpImageIo.h>
#include <visp3/core/vpIoTools.h>
#include <visp3/mbt/vpMbEdgeTracker.h>
#include <visp3/mbt/vpMbEdgeKltTracker.h>
#include <visp3/io/vpVideoReader.h>
#include <visp3/tt/vpTemplateTrackerSSDInverseCompositional.h>
#include <visp3/tt/vpTemplateTrackerWarpHomography.h>
#include <visp3/core/vpTrackingException.h>
#include <visp3/core/vpPixelMeterConversion.h>
#include <visp3/vision/vpPose.h>


#include "cv/PoseEstimator.h"

namespace sapphire {

    class VISPPoseEstimator : public PoseEstimator {
    public:
        VISPPoseEstimator(/*string _configFilename=""*/);
        virtual ~VISPPoseEstimator();



        //        virtual int SetCamera(Camera& _camera);

        virtual int InitPose(std::vector<Point2D<float> >& _imagePoints, std::vector<Point3D<float> >& _worldPoints);

        virtual int ComputePose(Matrix& _homography, Matrix& _rotation, Matrix& _translation);

        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rotation, Matrix& _translation);

        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rtOut);

    protected:
        virtual int ConfigureProc(std::string _sectionName, std::string _yamlString);

    private:

        //        cv::Mat cvToGlConversionMatrix = cv::Mat::zeros(4, 4, CV_64FC1);


        vpCameraParameters cameraParameters_;

        //    vpHomography vispHomography;
        vpHomogeneousMatrix homogeneousMatrix_;
        vpHomogeneousMatrix homogeneousMatrixDementhon_;
        vpHomogeneousMatrix homogeneousMatrixLagrange_;
        //        vpTemplateTrackerWarpHomography warp;
        //        vpTemplateTrackerSSDInverseCompositional *tracker;

        //    vector<vpPoint> posePoints_;
        //    vector<vpImagePoint> framePoints;
        //    vector<vpImagePoint> currentFramePoints;

        //    cv::Mat viewMatrix = cv::Mat(4, 4, CV_64FC1);
        //    cv::Mat cvToGlConversionMatrix = cv::Mat::zeros(4, 4, CV_64FC1);
        //    cv::Mat firstFrameGray;
    };
}

#endif /* VISPPOSEESTIMATOR_H */

#endif
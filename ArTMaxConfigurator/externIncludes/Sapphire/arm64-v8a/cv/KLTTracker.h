/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   KLTTracker.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:01 PM
 */

#ifndef KLTTRACKER_H
#define KLTTRACKER_H

#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <future>         // std::async, std::future
#include <chrono>         // std::chrono::milliseconds

#include "cv/Tracker.h"
#include "math/Patch2D.h"
#include "core/Time.h"

namespace sapphire {

    class KLTTracker : public Tracker {
    public:
        KLTTracker(/*string _configFilename = ""*/);
        virtual ~KLTTracker();



        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame); //, std::vector<Point2D<float> >& _imagePoints);
        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame, Shape2D& _initObject, Homography& _initHomography); //, std::vector<Point2D<float> >& _imagePoints);

    protected:

        virtual int ResetProc();


        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);


        //TODO

        //        virtual int AddObjectToTrack(Mesh& _objMesh) {
        //            return -1;
        //        };
        //
        //        virtual int InitTracker(Patch2D& _objPatch) {
        //            return -1;
        //        };


        virtual Shape2D ComputeShape2D(int _uniqueFaceId);



        virtual int ReInitTrackerProc(Patch2D& _objPatch); //, Shape2D& _initObj, Homography& _initHomography);

        virtual int TrackProc(Image& _image);

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);


    private:
        virtual int CheckDetection(Patch2D& _origObj, Image& _frame, Homography& _computedHomography, double _threshold);

        int CreateMask(Shape2D& _shape2D, cv::Mat &_mask, unsigned char nb);
        void FindHomography();
//        cv::Mat FindHomography(std::vector<cv::Point2f>& _points0,std::vector<cv::Point2f>& _points1,int _method);

        int setFindHomographyParams(std::vector<cv::Point2f>& _points0,std::vector<cv::Point2f>& _points1,int _method);

    private:

        cv::Mat m_gray, m_prevGray;
        Homography homographyPrev_;
        std::vector<cv::Point2f> m_points[2]; //!< Previous [0] and current [1] keypoint location
        std::vector<long> m_points_id; //!< Keypoint id
        int m_maxCount;
        cv::TermCriteria m_termcrit;
        int m_winSize;
        double m_qualityLevel;
        double m_minDistance;
        double m_minEigThreshold;
        double m_harris_k;
        int m_blockSize;
        int m_useHarrisDetector;
        int m_pyrMaxLevel;
        long m_next_points_id;
        bool m_initial_guess;
        std::vector<cv::Point2f> m_points_tmp[2];
        int findHomographyMethod_;
        cv::Mat homography_;
        int maxMillisToFindHomography_;
        int cornerRefinementEnabled_;


        int numPointsToTrackMultiplier_;
        double maxOpticalFlowErrThresh_;

        bool mHomographyComputed=false;
;


        int minNumPointsToTrack_;

        //        vpImage<unsigned char> frameContainer_;
        //
        //        vpTemplateTrackerWarpHomography warp_;
        //        //        vpTemplateTrackerMIInverseCompositional tracker_;
        //        vpTemplateTrackerSSDInverseCompositional tracker_;
        //
        //        vector<vpImagePoint> imagePoints_;



        bool enableCheckDetection_;



    };

}
#endif /* KLTTRACKER_H */


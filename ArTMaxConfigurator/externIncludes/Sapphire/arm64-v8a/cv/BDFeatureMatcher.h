/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BDFeatureMatcher.h
 * Author: marzorati
 *
 * Created on June 7, 2017, 5:09 PM
 */

#ifndef BDFEATUREMATCHER_H
#define BDFEATUREMATCHER_H

#ifdef _TEST
#include <vector>
#include <opencv2/opencv.hpp>

   #include <opencv2/line_descriptor.hpp>

#include "cv/FeatureMatcher.h"
#include "math/Matrix.hpp"


using namespace std;

namespace sapphire {

    class BDFeatureMatcher : public FeatureMatcher {
    public:
        BDFeatureMatcher(); //string _configFilename="");
        virtual ~BDFeatureMatcher();


        virtual int Match(Matrix& _queryDescriptors, Matrix& _trainDescriptors, vector< vector< FMatch > >& _matches);


    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:
        cv::Ptr<cv::line_descriptor::BinaryDescriptorMatcher> matcher_;
        std::string matcherType_;
                int numQueryMatches_;

    };

}
#endif /* BDFEATUREMATCHER_H */

#endif
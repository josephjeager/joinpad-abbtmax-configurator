//
// Created by marzorati on 6/18/18.
//

#ifdef _UNUSED_CODE

#ifdef _ANDROID

#ifndef TFLITEMULTIOBJECTCLASSIFICATOR_H
#define TFLITEMULTIOBJECTCLASSIFICATOR_H

#include "tensorflow/contrib/lite/model.h"
#include "tensorflow/contrib/lite/interpreter.h"
#include "tensorflow/contrib/lite/kernels/register.h"
#include <cstdio>
#include <string>

using namespace std;

using namespace tflite;

#define TFLITE_MINIMAL_CHECK(x) \
  if(!(x)) {                                                    \
    fprintf(stderr, "Error at %s:%d\n",  __FILE__, __LINE__); \
    exit(1); \
  }


class TFLiteMultiObjectClassificator {

public:
    
    TFLiteMultiObjectClassificator(string filename)
    {
        // Load model
        std::unique_ptr<tflite::FlatBufferModel> model
                = tflite::FlatBufferModel::BuildFromFile(filename.c_str());
        TFLITE_MINIMAL_CHECK(model != nullptr);

        // Build the interpreter
        tflite::ops::builtin::BuiltinOpResolver resolver;
        InterpreterBuilder builder(*model.get(), resolver);
        std::unique_ptr<Interpreter> interpreter;
        builder(&interpreter);
        TFLITE_MINIMAL_CHECK(interpreter != nullptr);

        // Allocate tensor buffers.
        TFLITE_MINIMAL_CHECK(interpreter->AllocateTensors() == kTfLiteOk);

        // Fill input buffers
        // TODO(user): Insert code to fill input tensors

        // Run inference
        TFLITE_MINIMAL_CHECK(interpreter->Invoke() == kTfLiteOk);

    }
};


#endif //TFLITEMULTIOBJECTCLASSIFICATOR_H


#endif

#endif
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MarkerPoseEstimator.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 9:26 AM
 */

#ifndef MARKERPOSEESTIMATOR_H
#define MARKERPOSEESTIMATOR_H


#include "cv/Camera.h"
#include "math/Point2D.h"
#include "math/Point3D.h"
#include "math/Homography.h"
#include "math/Matrix.hpp"
#include "core/ConfigurableNode.hpp"

//#define LOG_TAG "MarkerPoseEstimator"

namespace sapphire
{

class MarkerPoseEstimator : public ConfigurableNode
{
public:

    MarkerPoseEstimator(/*string _configFilename = ""*/)
    {
        isValidOutputData_ = false;
isInitMarkerPoseEstimator_=false;
    };

    virtual ~MarkerPoseEstimator()
    {
    };

    //        virtual int SetCamera(Camera& _camera) = 0;

//    virtual int InitPose(std::vector<Point2D<float> >& _imagePoints, std::vector<Point3D<float> >& _worldPoints) = 0;

    virtual int ComputePose(Shape2D& _corners2D, Matrix& _rotationOut, Matrix& _translationOut)
    {
        int ret = -1;
        if (isInitMarkerPoseEstimator_)
            ret = ComputePoseProc(_corners2D, _rotationOut, _translationOut);
        else
            LOGD("MarkerPoseEstimator::ComputePose Not Initialized");
        return ret;
    }

        virtual int GetWorldPoints3D(std::vector<Point3D<float> >& _worldPoints)
        {
            _worldPoints=worldPoints_;
            return 0;
        }
        virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0)
        {
            return camera_.SetCameraParams(_fx,_fy,_cx,_cy,_hFOV,_vFOV,_frameWidth,_frameHeight,_k1 , _k2 ,_k3,_p1,_p2);
        }
        virtual int SetCameraDevice(Camera& _camera)
        {
            camera_=_camera;
            return 0;
        }


protected:
    //         virtual int Init()=0;
    virtual int ComputePoseProc(Shape2D& _corners2D, Matrix& _rotationOut, Matrix& _translationOut) = 0;

protected:

    //        bool isReady_;
    bool isInitMarkerPoseEstimator_;
    bool isValidOutputData_;
    //        string configFilename_;
    //        cv::FileStorage configFile_;

    Camera camera_;
    std::vector<Point2D<float> > imagePoints_;
    std::vector<Point3D<float> > worldPoints_;

};
}
#endif /* MARKERPOSEESTIMATOR_H */


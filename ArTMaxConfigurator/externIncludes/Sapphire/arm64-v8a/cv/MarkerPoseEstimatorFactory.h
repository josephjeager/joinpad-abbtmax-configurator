/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MarkerPoseEstimatorFactory.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 11:06 PM
 */

#ifndef MARKERPOSEESTIMATORFACTORY_H
#define MARKERPOSEESTIMATORFACTORY_H


#include "ArucoMarkerPoseEstimator.h"

#define ARUCOMARKERPOSEESTIMATOR 1



namespace sapphire {

    class MarkerPoseEstimatorFactory {
    public:

        static MarkerPoseEstimator* CreateMarkerPoseEstimator(int _markerPoseEstimatorType) {
            switch (_markerPoseEstimatorType) {
                case ARUCOMARKERPOSEESTIMATOR:
                    return new ArucoMarkerPoseEstimator;
                    break;
            }
            throw "MarkerPoseEstimatorFactory::invalid markerPoseEstimatorType.";
        }


    };

}

#endif /* MARKERPOSEESTIMATORFACTORY_H */


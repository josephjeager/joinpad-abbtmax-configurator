/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MatcherFactory.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 11:06 PM
 */

#ifndef MATCHERFACTORY_H
#define MATCHERFACTORY_H



#include "OCVBFFeatureMatcher.h"
#include "OCVFlannFeatureMatcher.h"
#include "BDFeatureMatcher.h"


#define OCVFLANNFEATUREMATCHER  1
#define OCVBFFEATUREMATCHER  2
#define BDFEATUREMATCHER  3


namespace sapphire {

    class MatcherFactory {
    public:

        static FeatureMatcher* CreateFeatureMatcher(int _featureMatcherType) {
            switch (_featureMatcherType) {
                case OCVFLANNFEATUREMATCHER:
                    return new OCVFlannFeatureMatcher;
                    break;
                case OCVBFFEATUREMATCHER:
                    return new OCVBFFeatureMatcher;
                    break;
//                case BDFEATUREMATCHER:
//                    return new BDFeatureMatcher;
//                    break;
            }
            throw "MatcherFactory::invalid featureMatchertype.";
        }
    };

}

#endif /* MATCHERFACTORY_H */


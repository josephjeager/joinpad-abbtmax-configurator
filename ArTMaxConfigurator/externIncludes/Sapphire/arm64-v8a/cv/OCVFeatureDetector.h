/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OCVFeatureDetector.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 11:20 AM
 */

#ifndef OCVFEATUREDETECTOR_H
#define OCVFEATUREDETECTOR_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/core/core.hpp>


#include "cv/Feature2D.h"
#include "cv/FeatureDetector.h"
#include "image/Image.h"


using namespace std;

namespace sapphire {


class OCVFeatureDetector :public  FeatureDetector{
    public:
        OCVFeatureDetector(/*string _configFilename=""*/);
        virtual ~OCVFeatureDetector();

    virtual int Detect(Image& _img, Image& _mask,vector<Feature2D>& _outFeatures2D);
    virtual int Detect(Image& _img,vector<Feature2D>& _outFeatures2D);

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:

        cv::Ptr<cv::FeatureDetector> detector_;
        string detectorType_;

    };

}

#endif /* OCVFEATUREDETECTOR_H */


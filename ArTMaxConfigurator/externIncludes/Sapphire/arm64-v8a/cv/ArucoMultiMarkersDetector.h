/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArucoMultiMarkersDetector.h
 * Author: marzorati
 *
 * Created on November 15, 2017, 6:17 PM
 */

#ifndef ARUCOMULTIMARKERSDETECTOR_H
#define ARUCOMULTIMARKERSDETECTOR_H


#include <mutex>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "../3rdparty/aruco2/aruco.hpp"

#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "math/Mesh.h"
#include "cv/Camera.h"

#include "cv/MarkerDetector.h"

//#define LOG_TAG "ArucoMarkerDetector"

namespace sapphire
{

class ArucoMultiMarkersDetector : public MarkerDetector
{
public:
    ArucoMultiMarkersDetector();
    virtual ~ArucoMultiMarkersDetector();
    
    int CreateMarker(cv::Ptr<aruco2::Dictionary> _dictionary, int _markerId, int _markerSize);

protected:

//    virtual int ResetProc();

    virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    virtual int DetectProc(Image& _image, Image& _mask, Shape2D& _outCorners2D,int & _id);

    virtual int DetectProc(Image& _image, Shape2D& _outCorners2D,int & _id);

    virtual int DetectProc(Image& _image, Image& _mask, vector<Shape2D>& _outCorners2D,vector<int> & _ids);
    
    virtual int DetectProc(Image& _image, vector<Shape2D>& _outCorners2D,vector<int> & _ids);
    
    bool readDetectorParameters(string filename, cv::Ptr<aruco2::DetectorParameters> &params);

//    virtual int AddMarkerToDetectProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);
//
//    virtual int ReInitDetectorProc(Patch2D& _objPatch);



private:

    cv::Ptr<aruco2::Dictionary> dictionary_;
    cv::Ptr<aruco2::DetectorParameters> detectorParams_ ;
//    float markerLength_;
    

};

}
#endif /* ARUCOMULTIMARKERSDETECTOR_H */


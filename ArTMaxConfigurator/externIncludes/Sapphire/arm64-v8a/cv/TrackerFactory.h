/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TrackerFactory.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 11:06 PM
 */

#ifndef TRACKERFACTORY_H
#define TRACKERFACTORY_H

#ifdef USE_VISP

#include "VISPTracker.h"
#include "VISPTracker3D.h"

#endif

#include "KLTTracker.h"
#ifndef _ANDROID
#include "KCFTracker.h"
#endif
#include "KCFTracker2.h"
#include "CMTTracker.h"
#include "CMTBrainTracker.h"
#include "OpticalFlowTracker.h"

#define VISPTRACKER2D       1
#define VISPTRACKER3D       2
#define KLTTRACKER          3
#ifndef _ANDROID
#define KCFTRACKER          4
#endif
#define KCFTRACKER2         5
#define CMTTRACKER          6
#define CMTBRAINTRACKER     7
#define OPTICALFLOWTRACKER  8


namespace sapphire {

    class TrackerFactory {
    public:

        static Tracker* CreateTracker(int _trackerType) {
            switch (_trackerType) {
#ifdef USE_VISP
                case VISPTRACKER2D:
                    return new VISPTracker;
                    break;
                case VISPTRACKER3D:
                    return new VISPTracker3D;
                    break;
#endif
                case KLTTRACKER:
                    return new KLTTracker;
                    break;
#ifndef _ANDROID
                case KCFTRACKER:
                    return new KCFTracker;
                    break;
#endif
                case KCFTRACKER2:
                    return new KCFTracker2;
                    break;

                case CMTTRACKER:
                    return new CMTTracker;
                    break;

                case CMTBRAINTRACKER:
                    return new CMTBrainTracker;
                    break;

                case OPTICALFLOWTRACKER:
                    return new OpticalFlowTracker;
                    break;

            }
            throw "TrackerFactory::invalid tracker type.";
        }
    };

}

#endif /* TRACKERFACTORY_H */


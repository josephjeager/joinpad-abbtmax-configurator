/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureDescriptor.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 9:57 AM
 */

#ifndef FEATUREDESCRIPTOR_H
#define FEATUREDESCRIPTOR_H

#include "image/Image.h"
#include "cv/Feature2D.h"
#include "math/Matrix.hpp"
#include "core/ConfigurableNode.hpp"

namespace sapphire {

    class FeatureDescriptor:public ConfigurableNode {
    public:

        FeatureDescriptor()//string _configFilename = "")
        {
            //            isReady_ = false;

            //            configFilename_ = _configFilename;

        };

        //        virtual int Configure(string _configFilename) {
        //            int ret = 0;
        //            LOGD("FeatureDescriptor::Configure");
        //            LOGD("FeatureDescriptor::Configure: ConfigFile: %s %d", _configFilename.c_str(), _configFilename.empty());
        //            if (!_configFilename.empty()) {
        //                LOGD("FeatureDescriptor::Configure: Open");
        //                cv::String str=_configFilename;
        //
        //                configFile_.open(str, cv::FileStorage::READ);
        //                LOGD("FeatureDescriptor::Configure: isOpen: %d", configFile_.isOpened());
        //                if (configFile_.isOpened()) {
        //                    isReady_ = true;
        //                    ret = Init();
        //                    LOGD("FeatureDescriptor::Ready");
        //                } else {
        //                    LOGD("Error: Config file doesn't exist");
        //                    ret = -1;
        //                }
        //
        //            } else
        //                ret = -1;
        //            return ret;
        //        }

        virtual ~FeatureDescriptor() {
        };

        virtual int Compute(Image& _image, vector<Feature2D>& _features2D, Matrix& _outDescriptors) = 0;

    protected:

        //        virtual Cint Init() = 0;


    protected:

        //        bool isReady_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;
    };
}

#endif /* FEATUREDESCRIPTOR_H */


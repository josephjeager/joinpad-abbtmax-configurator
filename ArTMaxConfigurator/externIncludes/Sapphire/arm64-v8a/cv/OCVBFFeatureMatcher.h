/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OCVBFFeatureMatcher.h
 * Author: marzorati
 *
 * Created on June 7, 2017, 5:09 PM
 */

#ifndef OCVBFFEATUREMATCHER_H
#define OCVBFFEATUREMATCHER_H


#include <vector>
#include <opencv2/opencv.hpp>


#include "cv/FeatureMatcher.h"
#include "math/Matrix.hpp"


using namespace std;

namespace sapphire {

    class OCVBFFeatureMatcher : public FeatureMatcher {
    public:
        OCVBFFeatureMatcher(); //string _configFilename="");
        virtual ~OCVBFFeatureMatcher();


        virtual int Match(Matrix& _queryDescriptors, Matrix& _trainDescriptors, vector< vector< FMatch > >& _matches);


    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:
        cv::Ptr<cv::DescriptorMatcher> matcher_;
        std::string matcherType_;
    };

}
#endif /* OCVBFFEATUREMATCHER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MultiObjectClassification.h
 * Author: marzorati
 *
 * Created on June 21, 2017, 3:48 PM
 */

#ifndef MULTIOBJECTCLASSIFICATION_H
#define MULTIOBJECTCLASSIFICATION_H

#define _USE_MATH_DEFINES

#include <vector>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"


#include "math/Homography.h"
#include "core/Time.h"
#include "core/Thread.h"
#include "core/Info.h"
#include "utils/DrawingTools.h"
#include "cv/OCVMultiObjectClassificator.h"

#include "MultiObjectClassificationConfig.h"

#define LOG_TAG "MultiObjectClassification"

//#define _DRAW

//#ifdef _DEBUG
#ifdef _ANDROID
//#include <android/log.h>
//#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
//#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
//#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
#endif

#define FROM_FILE 0
#define FROM_STRING 1


using namespace sapphire;


class MultiObjectClassification {
public:
    MultiObjectClassification(string _configString,int _loadType);

    virtual ~MultiObjectClassification();
    
    int Init();
    int Configure();
    int Start();
    int Stop();

    std::string BuildDefaultConfigString();

    int ProcessFrame(Image& _frame);

    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV=0,double _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

    int SetImage(Image& _image);

    int GetDetectedBoxes(multimap<int,Shape2D >& _detectedBoxesOut);
    int GetDetectedObjects(multimap<int,Patch2D >& _detectedObjsOut);

    int GetCamera(Matrix& _cameraMatrix);


private:


    bool drawOutputImage_;
    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;


    OCVMultiObjectClassificator multiObjectClassificator_;

    Camera camera_;
    Image imgScene_;//, imgScene_tmp_,imgSceneOut_;

    int numFrame_;

    bool isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;

};

#endif /* MULTIOBJECTCLASSIFICATION_H */


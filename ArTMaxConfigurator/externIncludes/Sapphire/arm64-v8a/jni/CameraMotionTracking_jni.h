//
// Created by marzorati on 6/21/17.
//

#ifndef CAMERAMOTIONTRACKER_JNI_H
#define CAMERAMOTIONTRACKER_JNI_H



#include <jni.h>

#include "modules/CameraMotionTracking.h"
#include "modules/CameraMotionTrackingConfig.h"


#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeCreate(JNIEnv * jenv, jclass,jint option,jstring configParams);

JNIEXPORT void JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeDestroy(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeInit(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeSetCameraParams(JNIEnv * jenv, jclass, jlong thiz,jdouble fx,jdouble fy,jdouble cx,jdouble cy,jint width,jint height);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeConfigure(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeStart(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeStop(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeProcessFrame(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeGetHomography(JNIEnv * jenv, jclass, jlong thiz,jlong rHomographyOut);
//JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeGetRTMatrixes(JNIEnv * jenv, jclass, jlong thiz,jlong rRTMatrixesOut);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeGetMarker(JNIEnv * jenv, jclass, jlong thiz,jlong rMarkerOut,jlong rRTPmtt_MOut);//,jlong rRTPmkt_MOut);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeGetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImageOut);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_cameramotiontracking_CameraMotionTracking_nativeSetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);


#ifdef __cplusplus
}
#endif



#endif //CAMERAMOTIONTRACKER_JNI_H

//
// Created by marzorati on 6/21/17.
//

#ifndef OBJ2DTRACKING_JNI_H
#define OBJ2DTRACKING_JNI_H

#ifdef _ANDROID


#include <jni.h>

#include "modules/Obj2DTracking.h"
#include "modules/Obj2DTrackingConfig.h"


#ifdef __cplusplus
extern "C" {
#endif

//JNIEXPORT jlong JNICALL Java_net_joinpad_brainpad_Obj2DTracking_Obj2DTracking_nativeCreate(JNIEnv * jenv, jclass,jint option);
JNIEXPORT jlong JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeCreate(JNIEnv * jenv, jclass,jint option,jstring configParams);

JNIEXPORT void JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeDestroy(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeInit(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeConfigure(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeStart(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeStop(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeSetCameraParams(JNIEnv * jenv, jclass, jlong thiz,jdouble fx,jdouble fy,jdouble cx,jdouble cy,jint width,jint height);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeProcessFrame(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);

//JNIEXPORT jint JNICALL Java_net_joinpad_abb_wrapper_ABB_nativeGetDetectedObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rDetectedObjsOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeGetHomographies(JNIEnv * jenv, jclass, jlong thiz,jlong rHomographiesOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeGetReferenceObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rRefObjsOut);
//JNIEXPORT jint JNICALL Java_net_joinpad_abb_wrapper_ABB_nativeGetDetectedObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rDetectedObjsOut);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeGetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImageOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DTracking_nativeSetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);


#ifdef __cplusplus
}
#endif


#endif

#endif //OBJ2DTRACKING_JNI_H

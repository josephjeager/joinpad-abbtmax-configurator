/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Point2D.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 10:43 AM
 */

#ifndef POINT2D_H
#define POINT2D_H

#include "core/Validable.h"


namespace sapphire {

    template <class DataType>
    class Point2D{
    public:
        Point2D(DataType _x, DataType _y);
        Point2D();

        virtual ~Point2D() {
        };

        //Matrix GetHomogeneousMatrix();


    public:

        DataType x_;
        DataType y_;

    };

    //    template<class DataType>
    //    Matrix Point2D<DataType>::GetHomogeneousMatrix()
    //        {
    //            Matrix point(3,1);
    //            point.at<double>(0,0)=(double)x_;
    //            point.at<double>(1,0)=(double)y_;
    //            point.at<double>(2,0)=(double)1.0;
    //            return point;
    //        }

    template<class DataType>
    Point2D<DataType>::Point2D(DataType _x, DataType _y) {
        x_ = _x;
        y_ = _y;
    }

    template <class DataType>
    Point2D<DataType>::Point2D() {
    }

}
#endif /* POINT2D_H */


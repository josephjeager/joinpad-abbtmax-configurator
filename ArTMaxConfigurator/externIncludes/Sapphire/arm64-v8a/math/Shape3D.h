/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Shape3D.h
 * Author: marzorati
 *
 * Created on July 3, 2017, 11:33 AM
 */

#ifndef SHAPE3D_H
#define SHAPE3D_H

//#include "image/Image.h"
#include "math/Point3D.h"
//#include "cv/Feature2D.h"
#include "core/Validable.h"
#include "math/Matrix.hpp"

namespace sapphire {

    class Shape3D : public Validable {
    public:
        Shape3D(/*int _objId=-1,*/std::vector< Point3D<float> > _shape = std::vector<Point3D<float> >());

        virtual ~Shape3D();

        //        virtual int SetImage(Image& _image);
        //        virtual int SetFeatures2D(std::vector<Feature2D>& _features);

        virtual int SetVertexes3D(std::vector< Point3D<float> >& _vertexes3D) {
            int ret = 0;
            shape3D_ = _vertexes3D;
            ret = ComputeDims();
            if (ret == 0)
                ret = ComputeObjectCenter();
            return ret;
        }

        virtual int AddVertex3D(Point3D<float> _vertex3D) {
            int ret = 0;
            shape3D_.push_back(_vertex3D);
            return ret;

        }

        //        virtual int SetShape3D(std::vector<Point3D<float> > & _shape3D);
        //        virtual int SetSize(double _imageObjCenterX,double _imageObjCenterY,double _dimX,double _dimY);

        //        virtual int SetObjId(int _objId);

        virtual std::vector<Point3D<float> > GetShape() {
            return shape3D_;
        }

        int GetObjectCenter(float& _centerXOut, float& _centerYOut, float& _centerZOut) {
            _centerXOut = imageObjCenterX_;
            _centerYOut = imageObjCenterY_;
            _centerZOut = imageObjCenterZ_;
            return 0;
        }

        int GetDims(double& _dimXOut, double& _dimYOut, double& _dimZOut) {
            int ret = 0;
            _dimXOut = dimX_;
            _dimYOut = dimY_;
            _dimZOut = dimZ_;
            return ret;
        }

    private:
        virtual int ComputeDims();
        virtual int ComputeObjectCenter();

    public:

        double imageObjCenterX_, imageObjCenterY_, imageObjCenterZ_;
    private:

        //        double imageObjCenterX_,imageObjCenterY_,dimX_,dimY_;
        //        Image image_;
        //        std::vector<Feature2D> features_;
        std::vector<Point3D<float> > shape3D_;
        double dimX_, dimY_, dimZ_;

        //        int objId_;
    };
}
#endif /* SHAPE3D_H */


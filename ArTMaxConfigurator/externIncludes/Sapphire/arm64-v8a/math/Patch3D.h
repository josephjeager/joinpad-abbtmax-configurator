/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Patch3D.h
 * Author: marzorati
 *
 * Created on July 3, 2017, 11:33 AM
 */

#ifndef PATCH3D_H
#define PATCH3D_H

#include <chrono>

#include "image/Image.h"
#include "math/Shape2D.h"
#include "math/Shape3D.h"
//#include "math/GeomFace.h"

namespace sapphire
{

    class Patch3D
    {
    public:

        Patch3D(int _id=-1,std::vector<Point2D<float> >  _shape2D=std::vector<Point2D<float> >(),std::vector<Point3D<float> >  _shape3D=std::vector<Point3D<float> >());//,double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        Patch3D(int _id,Image& _image,std::vector<Point2D<float> >  _shape2D=std::vector<Point2D<float> >(),std::vector<Point3D<float> >  _shape3D=std::vector<Point3D<float> >());//,double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        virtual ~Patch3D();

        virtual int SetShape2D(vector<Point2D<float> >& _shape2D);
        virtual int SetShape3D(vector<Point3D<float> >& _shape3D);
        virtual int SetImage(Image& _image);
        virtual int SetId(int _id)
        {
            id_=_id;
            return 0;
        }

        virtual int GetId()
        {
            return id_;
        }

    public:

        Image image_;
        int id_;
        Shape2D shape2D_;
        Shape3D shape3D_;

    };
}
#endif /* PATCH3D_H */


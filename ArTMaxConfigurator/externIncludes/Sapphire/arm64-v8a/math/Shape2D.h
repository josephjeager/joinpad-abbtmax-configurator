/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Shape2D.h
 * Author: marzorati
 *
 * Created on July 3, 2017, 11:33 AM
 */

#ifndef SHAPE2D_H
#define SHAPE2D_H

#include "image/Image.h"
#include "math/Point2D.h"
#include "cv/Feature2D.h"
#include "core/Validable.h"

namespace sapphire {

    class Shape2D : public Validable {
    public:
        //        Shape2D(/*int _objId, */double _imageObjCenterX = 0, double _imageObjCenterY = 0); //,double _dimX=0,double _dimY=0);
        Shape2D(/*int _objId, */std::vector<Point2D<float> > _shape = std::vector<Point2D<float> >());//, double _imageObjCenterX = 0, double _imageObjCenterY = 0); //,double _dimX=0,double _dimY=0);

        virtual ~Shape2D();

        //        virtual int SetImage(Image& _image);
        //        virtual int SetFeatures2D(std::vector<Feature2D>& _features);

        virtual int SetVertexes2D(std::vector<Point2D<float> > & _vertexes2D) {
            int ret = 0;
            shape_ = _vertexes2D;
            ret = ComputeDims();
            if (ret==0)
                ret=ComputeObjectCenter();
            return ret;
        }

        virtual int AddVertex2D(Point2D<float>& _vertex2D)
        {
            int ret=0;
            shape_.push_back(_vertex2D);
            ret = ComputeDims();
            if (ret == 0)
                ret = ComputeObjectCenter();
            return ret;

        }





        //        virtual int SetObjCenter(double _imageObjCenterX, double _imageObjCenterY); //,double _dimX,double _dimY);

        //        virtual int SetObjId(int _objId);

        std::vector<Point2D<float> >& GetShape() {
            return shape_;
        }

        int GetObjectCenter(float& _centerXOut, float& _centerYOut) {
            _centerXOut = imageObjCenterX_;
            _centerYOut = imageObjCenterY_;
            return 0;
        }

        int GetDims(double& _dimXOut, double& _dimYOut) {
            int ret = 0;
            _dimXOut = dimX_;
            _dimYOut = dimY_;

            return ret;
        }

        int GetBoundingBox(vector<Point2D<float> >& _boundingBox) {
            int ret = 0;
            _boundingBox = boundingBox_;
            return ret;
        }

    public:

        //        int objId_;

        double imageObjCenterX_, imageObjCenterY_;

    private:
        virtual int ComputeDims();
        virtual int ComputeObjectCenter();

    private:

        std::vector<Point2D<float> > shape_;
        double dimX_, dimY_;
        std::vector<Point2D<float> > boundingBox_;
        //        Image image_;
        //        std::vector<Feature2D> features_;

    };
}
#endif /* SHAPE2D_H */


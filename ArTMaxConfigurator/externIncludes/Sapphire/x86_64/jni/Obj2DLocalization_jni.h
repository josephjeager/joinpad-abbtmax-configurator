//
// Created by marzorati on 6/21/17.
//

#ifndef OBJ2DLOCALIZATION_JNI_H
#define OBJ2DLOCALIZATION_JNI_H

#ifdef _ANDROID

#include <jni.h>

#include "modules/Obj2DLocalization.h"
#include "modules/Obj2DLocalizationConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeCreate(JNIEnv * jenv, jclass,jint option,jstring configParams);

JNIEXPORT void JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeDestroy(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeInit(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeSetCameraParams(JNIEnv * jenv, jclass, jlong thiz,jdouble fx,jdouble fy,jdouble cx,jdouble cy,jint width,jint height);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeConfigure(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeStart(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeStop(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeProcessFrame(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeGetHomographies(JNIEnv * jenv, jclass, jlong thiz,jlong rHomographiesOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeGetRTMatrixes(JNIEnv * jenv, jclass, jlong thiz,jlong rRTMatrixesOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeGetReferenceObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rRefObjsOut);

JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeGetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImageOut);
JNIEXPORT jint JNICALL Java_net_joinpad_brainpad_Obj2DLocalization_nativeSetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);


#ifdef __cplusplus
}
#endif

#endif

#endif //OBJ2DLOCALIZATION_JNI_H

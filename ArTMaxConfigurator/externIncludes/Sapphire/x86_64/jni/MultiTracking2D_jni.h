//
// Created by marzorati on 6/21/17.
//

#ifndef MULTITRACKING2D_JNI_H
#define MULTITRACKING2D_JNI_H

#ifdef _ANDROID
#include <jni.h>
#include "modules/MultiTracking2D.h"
#include "modules/DefaultMultiTracking2DConfig.h"


#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeCreate(JNIEnv * jenv, jclass,jint option,jstring configParams);

JNIEXPORT void JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeDestroy(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeInit(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeConfigure(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeStart(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeStop(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeSetCameraParams(JNIEnv * jenv, jclass, jlong thiz,jdouble fx,jdouble fy,jdouble cx,jdouble cy,jint width,jint height);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeProcessFrame(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeAddNewObjectToTrack(JNIEnv * jenv, jclass, jlong thiz,jlong rImage,jint id,jint left,jint top,jint right,jint bottom);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeRemoveTrackedObject(JNIEnv * jenv, jclass, jlong thiz,jint id);


JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeSetMaxNumTrackerLives(JNIEnv * jenv, jclass, jlong thiz,jint id);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeGetNumTrackerLives(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeGetHomographies(JNIEnv * jenv, jclass, jlong thiz,jlong rHomographiesOut);
//JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeGetReferenceObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rRefObjsOut);
//
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeGetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImageOut);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multitracking2d_MultiTracking2D_nativeSetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);


#ifdef __cplusplus
}
#endif



#endif //MULTITRACKING2D_JNI_H
#endif

//
// Created by marzorati on 6/21/17.
//

#ifndef MULTIOBJECTCLASSIFICATION_JNI_H
#define MULTIOBJECTCLASSIFICATION_JNI_H

#ifdef _ANDROID
#include <jni.h>
#include "modules/MultiObjectClassification.h"
#include "modules/MultiObjectClassificationConfig.h"
#include <map>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeCreate(JNIEnv * jenv, jclass,jint option,jstring configParams);

JNIEXPORT void JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeDestroy(JNIEnv * jenv, jclass, jlong thiz);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeInit(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeConfigure(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeStart(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeStop(JNIEnv * jenv, jclass, jlong thiz);
JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeSetCameraParams(JNIEnv * jenv, jclass, jlong thiz,jdouble fx,jdouble fy,jdouble cx,jdouble cy,jint width,jint height);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeProcessFrame(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);

JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeGetDetectedObjects(JNIEnv * jenv, jclass, jlong thiz,jlong rDetectedObjsOut);

JNIEXPORT jstring JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeGetObjectLabel(JNIEnv * jenv, jclass,jlong thiz, jint classID);

//JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeGetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImageOut);
//JNIEXPORT jint JNICALL Java_net_joinpad_sapphire_multiobjectclassification_MultiObjectClassification_nativeSetImage(JNIEnv * jenv, jclass, jlong thiz,jlong rImage);


#ifdef __cplusplus
}
#endif



#endif //MULTIOBJECTCLASSIFICATION_JNI_H
#endif

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Point3D.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 10:43 AM
 */

#ifndef POINT3D_H
#define POINT3D_H

#include "core/Validable.h"

namespace sapphire {

    template <class DataType>
    class Point3D {
    public:
        Point3D(DataType _x, DataType _y, DataType _z);
        Point3D();

        virtual ~Point3D() {
        };

        //        virtual Matrix GetHomogeneousMatrix()
        //        {
        //            Matrix point(4,1);
        //            point.at<double>(0,0)=(double)x_;
        //            point.at<double>(1,0)=(double)y_;
        //            point.at<double>(2,0)=(double)z_;
        //            point.at<double>(3,0)=(double)1.0;
        //            return point;
        //        }
    public:

        DataType x_;
        DataType y_;
        DataType z_;

    };

    template<class DataType>
    Point3D<DataType>::Point3D(DataType _x, DataType _y,DataType _z) {
        x_ = _x;
        y_ = _y;
        z_=_z;
    }

    template <class DataType>
    Point3D<DataType>::Point3D() {
    }

}
#endif /* POINT3D_H */


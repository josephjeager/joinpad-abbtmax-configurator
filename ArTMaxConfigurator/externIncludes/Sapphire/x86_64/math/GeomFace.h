/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GeomFace.h
 * Author: marzorati
 *
 * Created on August 2, 2017, 5:37 PM
 */

#ifndef GEOMFACE_H
#define GEOMFACE_H


#include "image/Image.h"
#include "math/Shape2D.h"
//#include "math/Shape3D.h"
#include "math/Patch3D.h"

namespace sapphire {

    class GeomFace {
    public:
        GeomFace(int _uniqueFaceId = -1/*, std::vector<int> _vertexIndexes = std::vector<int>()*/); //,std::vector<Point3D<float> > _vertexes3D = std::vector<Point3D<float> >());
        virtual ~GeomFace();

        //        virtual int SetAbsoluteTexCoords(std::vector<Point2D<float> >& _texCoords) {
        //            int ret = 0;
        //            absTexCoords_.SetVertexes2D(_texCoords);
        //            vector< Point2D<float> > relTexCoords;
        //            float minX = 1e10, minY = 1e10;
        //            float maxX = -1e10, maxY = -1e10;
        //            for (int i = 0; i < _texCoords.size(); ++i) {
        //                if (minX > _texCoords[i].x_)
        //                    minX = _texCoords[i].x_;
        //                if (minY > _texCoords[i].y_)
        //                    minY = _texCoords[i].y_;
        //                if (maxX < _texCoords[i].x_)
        //                    maxX = _texCoords[i].x_;
        //                if (maxY < _texCoords[i].y_)
        //                    maxY = _texCoords[i].y_;
        //            }
        //            for (int i = 0; i < _texCoords.size(); ++i) {
        //                Point2D<float> point;
        //                point.x_=_texCoords[i].x_-minX;
        //                point.y_=_texCoords[i].y_-minY;                
        //                relTexCoords.push_back(point);
        //            }
        //            SetRelativeTexCoords(relTexCoords);
        //            return ret;
        //        }

        virtual int SetTexCoords(std::vector<Point2D<float> >& _texCoords) {
            int ret = 0;
            face_.SetShape2D(_texCoords);
            //            relTexCoords_ = _relTexCoords;
            return ret;
        }

        virtual int SetUniqueFaceId(int _uniqueFaceId) {
            int ret = 0;
            face_.SetId(_uniqueFaceId);
            //            faceId_ = _faceId;
            return ret;
        }

        //        virtual int SetObjId(int _objId) {
        //            int ret = 0;
        //            objId_ = _objId;
        //            return ret;
        //        }

        //        virtual int SetVertexIndexes(std::vector<int> & _vertexIndexes) {
        //            int ret = 0;
        //            vertexIndexes_ = _vertexIndexes;
        //            return ret;
        //        }

        virtual int SetTexture(Image & _texture) {
            int ret = 0;
            face_.image_ = _texture;
            //            texture_ = _texture;
            return ret;
        }

        virtual int SetVertex3D(std::vector<Point3D<float> >& _vertexes3D) {
            int ret = 0;
            face_.shape3D_ = _vertexes3D;
            //            vertexes3D_ = _vertexes3D;
            return ret;
        }

        virtual vector<Point2D<float> > GetRelativeTexCoords() {
            return face_.shape2D_.GetShape(); //relTexCoords_;
        }

        //        virtual vector<Point2D<float> > GetAbsoluteTexCoords() {
        //            return absTexCoords_.GetShape();//texCoords_;
        //        }

        virtual Image GetTexture() {
            return face_.image_; //texture_;
        }

        //        virtual vector<int> GetVertexIndexes() {
        //            return vertexIndexes_;
        //        }

        virtual int GetUniqueFaceId() {
            return face_.GetId();
            //            return faceId_;
        }

        //        virtual int GetObjId() {
        //            return objId_;
        //        }

        virtual std::vector<Point3D<float> > GetVertex3D() {
            return face_.shape3D_.GetShape(); //vertexes3D_;
        }

        virtual Shape3D GetShape3D() {
            return face_.shape3D_;
        }

        virtual Shape2D GetShape2D() {
            return face_.shape2D_;
        }


    private:

        //        int faceId_;
        //        int objId_;

        //        Patch2D face_;
        Patch3D face_;

        //        Shape2D absTexCoords_;

        //        Image texture_;
        //        std::vector<Point2D<float> > texCoords_;
        //        std::vector<Point2D<float> > relTexCoords_;

        //        std::vector<Point3D<float> > vertexes3D_;
        //        std::vector<int> vertexIndexes_;

    };

}

#endif /* GEOMFACE_H */


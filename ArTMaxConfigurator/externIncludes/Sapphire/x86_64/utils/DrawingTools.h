/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DrawingTools.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 11:30 AM
 */

#ifndef DRAWINGTOOLS_H
#define DRAWINGTOOLS_H

#include <opencv2/opencv.hpp>

#include <vector>
#include <stdio.h>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "math/Matrix.hpp"
#include "cv/Camera.h"
#include "math/Shape2D.h"




using namespace std;

namespace sapphire {

    namespace drawingTools {


        int DisplayShapes2D(Mesh& _meshToProject, Matrix& _transform, Image& _outImg, Camera _camera = Camera(), int _uniqueFaceId = -1);
        int DisplayShape2D(Shape2D& _shape2D,Image& _outImg,int _colorR,int _colorG,int _colorB,int _id);
        int DisplayMultiShapes2D(vector<Shape2D>& _shapes2D,Image& _outImg,vector<int> _ids);
        int DisplayAxes(Matrix& _transform, Image& _outImg, Camera _camera = Camera(), double _axesLength = 1.0);

    };


}



#endif /* DRAWINGTOOLS_H */


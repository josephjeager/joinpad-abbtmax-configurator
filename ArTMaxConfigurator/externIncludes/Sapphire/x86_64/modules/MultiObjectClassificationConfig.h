//
// Created by marzorati on 1/19/18.
//

#ifndef MULTIOBJECTCLASSIFICATIONCONFIG_H
#define MULTIOBJECTCLASSIFICATIONCONFIG_H

#define DEFAULT_CONFIG_STRING  "%YAML:1.0\n"\
                                "---\n"\
                                "multiobjectclassification:\n"\
                                "   multiObjectClassificatorConfigFilename: \"ocvmultiobjectclassificator\"\n"\
                                "   cameraFilename: \"camera640x480\"\n"\
                                "camera640x480:\n"\
                                "   fx: 800\n"\
                                "   fy: 800\n"\
                                "   cx: 320\n"\
                                "   cy: 240\n"\
                                "   k1: 0\n"\
                                "   k2: 0\n"\
                                "   k3: 0\n"\
                                "   hFOV: 50\n"\
                                "   vFOV: 50\n"\
                                "   frameWidth: 640\n"\
                                "   frameHeight: 480\n"\
                                "ocvmultiobjectclassificator:\n"\
                                "   configFromFile: 1\n"\
                                "   objectLabelsFilename: \"/storage/emulated/0/projects/SmartEyeX/inferenceGraph/object_detection_classes_coco.txt\"\n"\
                                "   inferenceGraphFilename:\"/storage/emulated/0/projects/SmartEyeX/inferenceGraph/frozen_inference_graph.pb\"\n"\
                                "   inferenceGraphConfigFilename:\"/storage/emulated/0/projects/SmartEyeX/inferenceGraph/ssd_mobilenet_v1_coco.pbtxt\"\n"\
                                "   thresholdScore: 0.5\n"\
                                "   thresholdIOU: 0.8\n"\
                                "   imageWidth: 300\n"\
                                "   imageHeight: 300\n"\
                                "   scale: 0.007843137\n"\
                                "   meanR: 127.5\n"\
                                "   meanG: 127.5\n"\
                                "   meanB: 127.5\n"\
                                "   swapRB: 1\n"\
                                "   crop: 0\n"\
                                "   backend: 0\n"\
                                "   target: 0"
#endif //MULTIOBJECTCLASSIFICATIONCONFIG_H

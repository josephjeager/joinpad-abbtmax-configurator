//
// Created by marzorati on 1/19/18.
//

#ifndef MULTITRACKING2D_DEFAULTCONFIG_H
#define MULTITRACKING2D_DEFAULTCONFIG_H

#define DEFAULT_CONFIG_STRING  "%YAML:1.0\n"\
                                "---\n"\
                                "multitracking2D:\n"\
                                "   multiObjectTrackerConfigFilename: \"multitracker\"\n"\
                                "multitracker:\n"\
                                "   trackerType: 5\n"\
                                "   trackerConfigFilename: \"kfctracker\"\n"\
                                "kfctracker :\n"\
                                "   hog: 1\n"\
                                "   multiscale: 0\n"\
                                "   fixedWindow: 1\n"\
                                "   lab: 1\n"\
                                "   detectThresh: 0.2\n"\
                                "   updatingThresh: 0.4\n"\
                                "   targetDimsMultiplier: 2\n"\
                                "   maxNumOnlyTrackedProcessedFrame: 10000\n"\
                                "   cameraFilename: \"/storage/emulated/0/projects/SmartAssistanceX/data/Camera640x480.yaml\"\n"\
                                "   enableCheckDetection: 0"


#endif //MULTITRACKING2D_DEFAULTCONFIG_H

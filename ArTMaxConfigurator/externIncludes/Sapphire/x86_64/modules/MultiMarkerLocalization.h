/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MultiMarkerLocalization.h
 * Author: marzorati
 *
 * Created on June 21, 2017, 3:48 PM
 */

#ifndef MULTIMARKERLOCALIZATION_H
#define MULTIMARKERLOCALIZATION_H

#define _USE_MATH_DEFINES

#include <vector>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"


#include "cv/MarkerRecognizer.h"
#include "math/Homography.h"
#include "core/Time.h"
#include "core/Thread.h"
#include "core/Info.h"
#include "utils/DrawingTools.h"

#include "MultiMarkerLocalizationConfig.h"

#define LOG_TAG "MultiMarkerLocalization"

//#define _DRAW

//#ifdef _DEBUG
#ifdef _ANDROID
//#include <android/log.h>
//#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
//#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
//#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
#endif

#define FROM_FILE 0
#define FROM_STRING 1


#define MAX_MARKER_LIVES    20

using namespace sapphire;


class Marker
{
public:

    Marker():rtC_M_(4,4,CV_64F)
    {}

    int detectedMarkerID_=-1;
    int markerLives_=-1;
    double timestamp_=-1;
    int numDetections_=0;

    Matrix rtC_M_;

    double dimX_=0;
    double dimY_=0;
};


class MultiMarkerLocalization {
public:
    MultiMarkerLocalization(string _configString,int _loadType);
//    Obj2DLocalizationModule(const char* _pConfigParamsJSON);

    virtual ~MultiMarkerLocalization();
    
    int Init();
    int Configure();
    int Start();
    int Stop();


    std::string BuildDefaultConfigString();

    int ProcessFrame(Image& _frame);

//    int GetDevicePose(Matrix& _rtPmt0_PmttOut,Matrix& _xPmt0_PmttOut,double &_timestampOut);
//    int GetMarker(Marker& _marker,Matrix& _rtPmtt_M,Matrix& _rtPmkt_M,double &_timestamp);

//    int ResetMarkerDetection();

    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV=0,double _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

//    int GetImage(Image& _imageOut);

    int SetImage(Image& _image);


//    int GetMarkerPose(Matrix& rtPmtt_M,Matrix& _rtPmkt_M,double &_timestamp);


    int GetDetectedObjects(multimap<int,Shape2D >& _detectedObjsOut);

    int GetHomographies(multimap<int,Matrix >& _homographiesOut);
    int GetRTMatrixes(multimap<int, Matrix>& _rtMatrixesOut);

//    int GetTransforms(multimap<int,Matrix>& _transformsOut);
    multimap<int,Marker> GetDetectedMarkers()
    {
        return detectedMarkers_;
    }

//    int GetDetectedMarkers(multimap<int,Shape2D >& _detectedObjsOut);

    int GetCamera(Matrix& _cameraMatrix);




private:
    int DetectMarkers(Image & _image,multimap<int,Marker>& _markerOut);

    int UpdateMarkers(multimap<int,Marker>& _markers);

private:


    bool drawOutputImage_;
    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;


    MarkerRecognizer markerRecognizer_;

    multimap<int,Marker> detectedMarkers_;

    Camera camera_;
    Image imgScene_;//, imgScene_tmp_,imgSceneOut_;

    int numFrame_;

    bool isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;

};

#endif /* MULTIMARKERLOCALIZATION_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LSDFeatureDetector.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 11:20 AM
 */

#ifndef LSDFEATUREDETECTOR_H
#define LSDFEATUREDETECTOR_H

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/line_descriptor.hpp>

#include "cv/Feature2D.h"
#include "cv/FeatureDetector.h"
#include "image/Image.h"


#ifdef _TEST
using namespace std;

namespace sapphire {


class LSDFeatureDetector :public  FeatureDetector{
    public:
        LSDFeatureDetector(/*string _configFilename=""*/);
        virtual ~LSDFeatureDetector();

    virtual int Detect(Image& _img, Image& _mask,vector<Feature2D>& _outFeatures2D);
    virtual int Detect(Image& _img,vector<Feature2D>& _outFeatures2D);

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:

        cv::Ptr<cv::line_descriptor::LSDDetector> detector_;
        string detectorType_;
        
        double scale_;
        int numOctaves_;
        
    };

}

#endif /* OCVFEATUREDETECTOR_H */

#endif
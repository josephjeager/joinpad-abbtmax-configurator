/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MultiObjectClassificator.h
 * Author: marzorati
 *
 * Created on June 14, 2018, 1:05 PM
 */

#ifdef _ENABLE_TENSORFLOW

#ifndef MULTIOBJECTCLASSIFICATOR_H
#define MULTIOBJECTCLASSIFICATOR_H

#include <thread>
#include <mutex>
#include <vector>
#include <fstream>
#include <utility>
#include <iostream>

#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/cc/ops/const_op.h"
#include "tensorflow/cc/ops/image_ops.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"


#include <math.h>
#include <fstream>
#include <utility>
#include <vector>
#include <iostream>
#include <regex>

#include "tensorflow/cc/ops/const_op.h"
#include "tensorflow/cc/ops/image_ops.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/stringpiece.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/env.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"

#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <time.h>

#include "math/Matrix.hpp"
#include "math/Point2D.h"
#include "image/Image.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"

using tensorflow::Flag;
using tensorflow::Tensor;
using tensorflow::Status;
using tensorflow::string;
using tensorflow::int32;

namespace sapphire {

    class MultiObjectClassificator : public ProcessingNode {
    public:
        MultiObjectClassificator(/*string _configFilename=""*/);

        virtual ~MultiObjectClassificator();

        virtual int InitProc();
        virtual int StartThreadedProc();
        virtual int StopThreadedProc();
        virtual int StartProc();
        virtual int StopProc();

        virtual int SetCurrentFrame(Image& _currentFrame) {
            int ret = 0;
            LOGD("MultiObjectClassificator::Setting current frame...");
            mutex_.lock();
            currentFrame_ = _currentFrame;
            isValidFrame_ = true;
            mutex_.unlock();
            LOGD("MultiObjectClassificator::Setting current frame...Done %d", ret);
            return ret;
        }

        virtual int ProcessFrame(Image& _image);
        
        virtual int GetLastTimestamp(double & _timestamp) {
            int ret = 0;
            _timestamp = lastTimestamp_;
            return ret;
        }

//        virtual multimap<int, Matrix> GetTransforms(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Patch2D > GetImageObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Shape2D> GetShapeObjects(bool& _outIsValid, double &_timestamp);

//        virtual multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Patch2D > GetCurrentImageObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Shape2D> GetCurrentShapeObjects(bool& _outIsValid, double &_timestamp);

    protected:
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

        virtual int RunProc();
        
    private:
        Status ReadLabelsMapFile(const string &_fileName, map<int, string> &_labelsMap);
        Status ReadTensorFromMat(Image &_image, Tensor &_outTensor);
        Status LoadGraph(const string &_graph_file_name,unique_ptr<tensorflow::Session> *_session);
        vector<size_t> FilterBoxes(tensorflow::TTypes<float>::Flat &_scores,tensorflow::TTypes<float, 3>::Tensor &_boxes,double _thresholdIOU, double _thresholdScore); 
        double IOU(cv::Rect2f _box1, cv::Rect2f _box2);
    
    public:

        double lastTimestamp_;
        Image currentFrame_;

        bool isValidFrame_, isValidOutputData_; 
        
        string objectLabelFilename_,inferenceGraphFilename_;

        string inputLayer_;
        vector<string> outputLayer_;
        std::unique_ptr<tensorflow::Session> session_;
        
        multimap<int, Patch2D > oldImgObjs_, imgObjs_;
        multimap<int, Shape2D > oldShapeObjs_, shapeObjs_;
//        multimap<int, Matrix > oldTransforms_, transforms_;

        Tensor tensor_;
        std::vector<Tensor> outputs_;
        double thresholdScore_;
        double thresholdIOU_;
        tensorflow::TensorShape shape_;
        std::map<int, std::string> labelsMap_;
    
    };

}

#endif /* MULTIOBJECTCLASSIFICATOR_H */

#endif
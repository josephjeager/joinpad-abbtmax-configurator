/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tracker.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 9:26 AM
 */

#ifndef TRACKER_H_
#define TRACKER_H_

#include <mutex>
#include <thread>

#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "math/Mesh.h"
#include "cv/Camera.h"

namespace sapphire {

    class Tracker : public ConfigurableNode {
    public:

        Tracker(/*string _configFilename = ""*/)// : imgRefObj_(-1), imgObj_(-1)
        {

            //            isReady_ = false;
            numProcessedFrames_ = 0;
            maxNumOnlyTrackedProcessedFrame_ = 0;
            isValidOutputData_ = false;
            isInitTracker_ = false;
            isANewTracker_=true;
            initTransform_=(cv::Mat)initTransform_.eye(3,3,CV_64F);
            transform_=(cv::Mat)transform_.eye(3,3,CV_64F);
            isTracked_=-1;
            //            configFilename_ = _configFilename;
        }

        //        virtual int Configure(string _configFilename) {
        //            int ret = 0;
        //            configFile_.open(_configFilename, cv::FileStorage::READ);
        //            if (configFile_.isOpened()) {
        //                configFilename_ = _configFilename;
        //                isReady_ = true;
        //                ret = Init();
        //            } else {
        //                std::cout << "Error: Config file doesn't exist" << endl;
        //                ret = -1;
        //            }
        //
        //            return ret;
        //        }

        virtual ~Tracker() {
        };

        virtual int Reset() {
            int ret = -1;
            isInitTracker_ = false;
            isANewTracker_=true;
            ret = ResetProc();
            return ret;
        };

        virtual int AddObjectToTrack(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform) {
            int ret = -1;
            isInitTracker_ = false;
            SetInitTransform(_initTransform);
            ret = AddObjectToTrackProc(_objMesh, _firstObjPatch, _initTransform);
            if (ret == 0)
                isInitTracker_ = true;
            return ret;
        }

        //        virtual int InitTracker() {
        //            int ret = -1;
        //            if (InitTrackerProc() == 0) {
        //                isReady_ = true;
        //                ret = 0;
        //            } else {
        //                isReady_ = false;
        //                ret = -1;
        //            }
        //
        //            return ret;
        //
        //        };

        virtual int ReInitTracker(Patch2D& _firstObjPatch) {
            int ret = -1;
            isInitTracker_ = false;
            isANewTracker_=true;
            ret = ReInitTrackerProc(_firstObjPatch);
            return ret;


        }; // Shape2D& _initObj, Homography& _initHomography) = 0;


        virtual int SetImage(Image& _image)
        {
            image_=_image;
            return 0;
        }

        int TrackNoImage() {
            int ret = -1;
            isTracked_=-1;
            if (isInitTracker_)
            {
                isANewTracker_ = false;
                isTracked_ = TrackProc(image_);
            }
            else
                LOGD("Tracker::Track Not Initialized");
            return isTracked_;
        }

        virtual int Track(Image& _image) {
            int ret = -1;
            isTracked_=-1;
            if (isInitTracker_)
            {
                isANewTracker_ = false;
                isTracked_ = TrackProc(_image);
            }
            else
                LOGD("Tracker::Track Not Initialized");
            return isTracked_;
        }

        //        virtual Point2D<float> GetImagePos(bool& _outIsValid){
        //            mutex_.lock();
        //            _outIsValid=isValidOutputData_;
        //            mutex_.unlock();
        //            return imagePos_;
        //        }

        virtual int IsTracked()
        {
            return isTracked_;

        }
        virtual int GetNumProcessedFrame() {
            return numProcessedFrames_;
        }

        virtual bool GetIsANewTracker()
        {
            return isANewTracker_;
        }

        virtual int SetIsANewTracker(bool isANewTracker)
        {
            isANewTracker_=isANewTracker;
            return 0;
        }

        virtual int SetNumProcessedFrame(int _numProcessedFrame) {
            numProcessedFrames_ = _numProcessedFrame;
            return 0;
        }

        virtual int SetInitTransform(Matrix& _initTransform) {
            initTransform_ = _initTransform;
            //            imgRefObj_ = objToTrack_.GetFaces()[0].GetShape2D();
            //            isInitTracker_ = true;

            return 0;
        }

        virtual Matrix& GetTransform(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return transform_;
        }

        virtual Matrix& GetInitTransform(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return initTransform_;
        }

        virtual Mesh& GetTrackedMesh() {
            return objToTrack_;
        }

        virtual Shape2D ComputeShape2D(int _uniqueFaceId) = 0;


        //        virtual std::vector< Point2D<float> > & GetBoundingBox(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return boundingBox_;
        //        }


        //        virtual int ComputeShapes2D(vector<Shape2D>& _outShapes2D)=0;
        //        virtual int ComputeShape2D(int _numFace,Shape2D& _outShape2D)=0;


        //        virtual vector<Shape2D>& GetShapesObject(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return imgObj_;
        //        }

        //        virtual Shape2D& GetFirstShapeObject(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            if (imgObj_.size() > 0)
        //                return imgObj_[0];
        //            else {
        //                Shape2D shape;
        //                return shape;
        //            }
        //        }

        virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0)
        {
            return camera_.SetCameraParams(_fx,_fy,_cx,_cy,_hFOV,_vFOV,_frameWidth,_frameHeight,_k1 , _k2 ,_k3,_p1,_p2);
        }

        virtual int SetCameraDevice(Camera& _camera)
        {
            camera_=_camera;
            return 0;
        }


    protected:
        virtual int ResetProc() = 0;


        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame) = 0; //Image& _image, std::vector<Point2D<float> >& _imagePoints) = 0;
        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame, Shape2D& _initObject, Homography& _initHomography) = 0; //Image& _image, std::vector<Point2D<float> >& _imagePoints) = 0;


        //        virtual int AddObjectToTrack(Mesh& _objMesh) = 0;




        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform) = 0;



        //        virtual int InitTrackerProc() = 0;

        //        virtual int InitTracker(Homography& _initHomography) = 0; //Image& _image, std::vector<Point2D<float> >& _imagePoints) = 0;
        //
        //        virtual int InitTracker(Patch2D& _objPatch) = 0;

        virtual int ReInitTrackerProc(Patch2D& _objPatch) = 0;

        //        virtual int Init() = 0;

        virtual int TrackProc(Image& _image) = 0;


    protected:

        Image image_;

        bool isInitTracker_;
        bool isANewTracker_;
        //        bool isReady_;
        std::mutex mutex_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;

        //        Point2D<float> imagePos_;

        Patch2D firstObjPatch_;

        Mesh objToTrack_;
        Camera camera_;


        bool isValidOutputData_;

        int numProcessedFrames_;
        int maxNumOnlyTrackedProcessedFrame_;
        int maxProjectionError_;

        int isTracked_;

        //        vector<Shape2D> imgObj_;


        Matrix initTransform_, transform_;

        //OLD - TO REMOVE
        //        Shape2D imgRefObj_;


        //        bool isReady_;
        //        std::vector<Point2D<float> > boundingBox_;


    };
}

#endif /* TRACKER_H_ */


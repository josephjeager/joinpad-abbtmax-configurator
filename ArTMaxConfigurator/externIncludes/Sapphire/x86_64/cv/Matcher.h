/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Matcher.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 9:56 AM
 */

#ifndef MATCHER_H_
#define MATCHER_H_

#include "FeatureMatcher.h"

namespace sapphire {

    class Matcher {
    public:
        Matcher();
        virtual ~Matcher();

        virtual int SetFeatureMatcher(FeatureMatcher* _featureMatcher);
        virtual FeatureMatcher* GetFeatureMatcher();
        virtual int Match(Matrix& _queryDescriptors, Matrix& _trainDescriptors);
        virtual int FilterMatches(std::vector<Feature2D>& _trainPoints, std::vector<Feature2D>& _queryKeyPoints, float _maxDistanceRatio = 0.8f, float _minDistanceMultiplier = 3, bool _enableOnlyOneCorrispondenceFilter = false);

        virtual std::vector< std::vector< FMatch > > GetMatches() {
            return matches_;
        }

        virtual std::vector<FMatch> GetMatchesFiltered() {
            return matchesFiltered_;
        }

    private:

        FeatureMatcher* pFeatureMatcher_;
        vector< vector< FMatch> > matches_;
        vector< FMatch> matchesFiltered_;
    };
}

#endif /* MATCHER_H_ */


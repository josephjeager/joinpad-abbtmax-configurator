/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MultiObjectTracker.h
 * Author: marzorati
 *
 * Created on July 3, 2017, 11:28 AM
 */

#ifndef MULTIOBJECTTRACKER_H
#define MULTIOBJECTTRACKER_H

#include <thread>
#include <mutex>


#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Shape2D.h"
#include "cv/Tracker.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"
#include "cv/TrackerFactory.h"
#include "math/Mesh.h"

#define MAX_NUM_TRACKER_LIVES   10
#define _ENABLEMULTITHREADTRACKING

namespace sapphire {

    class MultiObjectTracker : public ProcessingNode {
    public:

        MultiObjectTracker(); //string _configFilename="");

        virtual ~MultiObjectTracker();

        //        virtual int Configure(string _configFilename);

        virtual int InitProc();

        virtual int StartThreadedProc();
        virtual int StopThreadedProc();
        virtual int StartProc();
        virtual int StopProc();

        virtual int SetCurrentFrame(Image& _currentFrame) {
            int ret = 0;
            mutex_.lock();
            LOGD("MultiObjectTracker::Setting current frame...");
            currentFrame_ = _currentFrame;
            isValidFrame_ = true;
            mutex_.unlock();
            LOGD("MultiObjectTracker::Setting current frame...Done %d", ret);

            return ret;
        }

        virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0)
        {
            return camera_.SetCameraParams(_fx,_fy,_cx,_cy,_hFOV,_vFOV,_frameWidth,_frameHeight,_k1 , _k2 ,_k3,_p1,_p2);
        }
        virtual int SetCameraDevice(Camera& _camera)
        {
            camera_=_camera;
            return 0;
        }

        virtual int GetLastTimestamp(double & _timestamp) {
            int ret = 0;
            _timestamp = lastTimestamp_;
            return ret;
        }

        //OLD - REMOVE - START
        //        virtual int AddNewObjectToTrack(Mesh& _objMesh, Patch2D& _firstObjPatch,Homography& _initHomography);
        //OLD - REMOVE - END
        virtual int AddNewObjectToTrack(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix& _initTransform); //,Homography& _initHomography);

        //OLD - REMOVE - START
        //        virtual int AddNewObjectToTrack(int _uniqueId, Tracker2D* _pTracker);
        //OLD - REMOVE - END

        virtual int SetMaxNumTrackerLives(int _maxNumTrackerLives=MAX_NUM_TRACKER_LIVES)
        {
            maxNumTrackerLives_=_maxNumTrackerLives;
            return 0;
        }

        virtual int GetNumTrackerLives(int _trackerId)
        {
            if (objTrackersLife_.find(_trackerId)!=objTrackersLife_.end())
            {
                return objTrackersLife_.find(_trackerId)->second;
            }
            else if (_trackerId==-1)
                return maxNumTrackerLives_;
            else
                return -1;
        }

        virtual int RemoveObject(int _objId);

        virtual int Reset();

        virtual int ProcessFrame(Image& _image);

        //        multimap<int, std::vector<Point2D<float> > > GetBoundingBoxes(bool& _outIsValid) {
        //            mutex_.lock();
        //
        //            _outIsValid = true; //isValidOutputData_;
        //            mutex_.unlock();
        //            return oldBoundingBoxes_;
        //        }

        //        multimap<int, Homography > GetHomographies(bool& _outIsValid, double &_timestamp) {
        //            mutex_.lock();
        //            _outIsValid = true; //isValidOutputData_;
        //            _timestamp = lastTimestamp_;
        //            mutex_.unlock();
        //            return oldHomographies_;
        //        }

        multimap<int, Matrix > GetTransforms(bool& _outIsValid, double &_timestamp) {
            mutex_.lock();
            _outIsValid = true; //isValidOutputData_;
            _timestamp = lastTimestamp_;
            mutex_.unlock();
            return oldTransforms_;
        }

        multimap<int, Shape2D > GetDetectedObjects(bool& _outIsValid, double &_timestamp) {
            mutex_.lock();
            _outIsValid = true; //isValidOutputData_;
            _timestamp = lastTimestamp_;

            mutex_.unlock();
            //            return oldImgObjs_;
            return oldShapeObjs_;
        }

        //        multimap<int, Shape2D > GetShapeObjects(bool& _outIsValid, double &_timestamp) {
        //            mutex_.lock();
        //            _outIsValid = true; //isValidOutputData_;
        //            _timestamp = lastTimestamp_;
        //
        //            mutex_.unlock();
        //            //            return oldImgObjs_;
        //            return oldShapeObjs_;
        //        }

        //        multimap<int, std::vector< Patch2D > > GetMeshObjects(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = true; //isValidOutputData_;
        //            mutex_.unlock();
        //            return oldMeshObjs_;
        //        }

        //        multimap<int, std::vector<Point2D<float> > > GetCurrentBoundingBoxes(bool& _outIsValid) {
        //            mutex_.lock();
        //
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return boundingBoxes_;
        //        }

        //        multimap<int, Homography> GetCurrentHomographies(bool& _outIsValid, double & _timestamp) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            _timestamp = lastTimestamp_;
        //
        //            mutex_.unlock();
        //            return homographies_;
        //        }

        multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double & _timestamp) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            _timestamp = lastTimestamp_;

            mutex_.unlock();
            return transforms_;
        }



        //        multimap<int, Shape2D > GetCurrentShapeObjects(bool& _outIsValid, double & _timestamp) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            _timestamp = lastTimestamp_;
        //
        //            mutex_.unlock();
        //            //            return imgObjs_;
        //            return shapeObjs_;
        //        }

        //        multimap<int, std::vector< Patch2D > > GetCurrentMeshObjects(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return meshObjs_;
        //        }

        //OLD - REMOVE - START
        //        virtual std::multimap<int, Tracker2D*>& GetObjectTrackers() {
        //OLD - REMOVE - END

        virtual std::multimap<int, Tracker*>& GetObjectTrackers() {

            return objTrackers_;
        }


    protected:
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

        virtual int RunProc();

    protected:

        //        bool isReady_;

        Camera camera_;

        std::string yamlString_;
        Image currentFrame_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;

        //        std::mutex mutex_;

        multimap<int, Matrix > transforms_, oldTransforms_;
        multimap<int, Shape2D > shapeObjs_, oldShapeObjs_;

        int maxNumTrackerLives_;

        bool isValidFrame_, isValidOutputData_;
        //         bool isRunning_;

        //OLD - REMOVE - START
        //        std::multimap<int, Tracker2D*> objTrackers_;
        //OLD - REMOVE - END
        std::multimap<int, Tracker*> objTrackers_;
        std::multimap<int,int> objTrackersLife_;

        int numProcessedFrames_;

        //        std::thread thr_;

    private:

        double lastTimestamp_;
        //        virtual int IsANewObject(Object2D& _obj);
        //        
        //        virtual int DataAssociation(Object2D& _obj, int &_outObjIndex);

    };

}

#endif /* MULTIOBJECTTRACKER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   P3PPoseEstimator.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:40 PM
 */

#ifndef P3PPOSEESTIMATOR_H
#define P3PPOSEESTIMATOR_H

#include <opencv2/opencv.hpp>

#include <opencv2/calib3d.hpp>

//#include <visp3/gui/vpDisplayGDI.h>
//#include <visp3/vision/vpKeyPoint.h>
//#include <visp3/gui/vpDisplayGDI.h>
//#include <visp3/gui/vpDisplayOpenCV.h>
//#include <visp3/gui/vpDisplayX.h>
//#include <visp3/io/vpImageIo.h>
//#include <visp3/core/vpIoTools.h>
//#include <visp3/mbt/vpMbEdgeTracker.h>
//#include <visp3/mbt/vpMbEdgeKltTracker.h>
//#include <visp3/io/vpVideoReader.h>
//#include <visp3/tt/vpTemplateTrackerSSDInverseCompositional.h>
//#include <visp3/tt/vpTemplateTrackerWarpHomography.h>
//#include <visp3/core/vpTrackingException.h>
//#include <visp3/core/vpPixelMeterConversion.h>
//#include <visp3/vision/vpPose.h>


#include "cv/PoseEstimator.h"


namespace sapphire {

    class P3PPoseEstimator : public PoseEstimator {
    public:
        P3PPoseEstimator(/*string _configFilename=""*/);
        virtual ~P3PPoseEstimator();



        //        virtual int SetCamera(Camera& _camera);

        virtual int InitPose(std::vector<Point2D<float> >& _imagePoints, std::vector<Point3D<float> >& _worldPoints,int _objId=-1);

        virtual int ComputePose(Matrix& _homography, Matrix& _rotationOut, Matrix& _translationOut,bool _useExtrinsics,int _objId=-1);
        virtual int ComputePose(Matrix& _homography, Matrix& _rtOut,bool _useExtrinsics,int _objId=-1);

        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rotationOut, Matrix& _translationOut,bool _useExtrinsics,int _objId=-1);

        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rtOut,bool _useExtrinsics,int _objId=-1);


    protected:
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    private:

        int poseEstimatorMethod_;
        
        int objId_;

        //        cv::Mat cvToGlConversionMatrix = cv::Mat::zeros(4, 4, CV_64FC1);



        //        vpCameraParameters cameraParameters_;
        //        vpPose pose_;
        //        //    vpHomography vispHomography;
        //        vpHomogeneousMatrix homogeneousMatrix_;
        //        vpHomogeneousMatrix homogeneousMatrixDementhon_;
        //        vpHomogeneousMatrix homogeneousMatrixLagrange_;
        //        vpTemplateTrackerWarpHomography warp;
        //        vpTemplateTrackerSSDInverseCompositional *tracker;

        //    vector<vpPoint> posePoints_;
        //    vector<vpImagePoint> framePoints;
        //    vector<vpImagePoint> currentFramePoints;

        //    cv::Mat viewMatrix = cv::Mat(4, 4, CV_64FC1);
        //    cv::Mat cvToGlConversionMatrix = cv::Mat::zeros(4, 4, CV_64FC1);
        //    cv::Mat firstFrameGray;
    };
}

#endif /* P3PPOSEESTIMATOR_H */


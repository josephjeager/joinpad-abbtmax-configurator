/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   KCFTracker2.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:01 PM
 */

#ifndef KCFTRACKER2_H
#define KCFTRACKER2_H

#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "3rdparty/KCFTracker/kcftracker.hpp"

#include "cv/Tracker.h"
#include "math/Patch2D.h"

namespace sapphire {

    class KCFTracker2 : public Tracker {
    public:
        KCFTracker2(/*string _configFilename = ""*/);
        virtual ~KCFTracker2();



        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame); //, std::vector<Point2D<float> >& _imagePoints);
        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame, Shape2D& _initObject, Homography& _initHomography); //, std::vector<Point2D<float> >& _imagePoints);

        virtual Shape2D ComputeShape2D(int _uniqueFaceId);

    protected:

        virtual int ResetProc();


        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);






        virtual int ReInitTrackerProc(Patch2D& _objPatch); //, Shape2D& _initObj, Homography& _initHomography);

        virtual int TrackProc(Image& _image);

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    private:
        virtual int CheckDetection(Patch2D& _origObj, Image& _frame, Homography& _computedHomography, double _threshold);

        int CreateMask(Shape2D& _shape2D, cv::Mat &_mask, unsigned char nb);


    private:

        cv::Ptr<KCFTracker> pTracker_;

        bool hog_, lab_, multiscale_, fixedWindow_;

        cv::Rect2d roi[2];

        double mTargetDimsMultiplier;

        Homography homographyPrev_;

        bool enableCheckDetection_;

    };

}
#endif /* KCFTRACKER2_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Match.hpp
 * Author: marzorati
 *
 * Created on June 7, 2017, 5:03 PM
 */

#ifndef FMATCH_HPP
#define FMATCH_HPP

namespace sapphire {

    class FMatch {
    public:

        FMatch(int _idx1 = -1, int _idx2 = -1 , float _distance=-1) 
        {
            idx1_ = _idx1;
            idx2_ = _idx2;
            distance_=_distance;

        }
        virtual ~FMatch(){};

        int& GetIdx1() {
            return idx1_;
        };

        int& GetIdx2() {
            return idx2_;
        };

        float& GetDistance() {
            return distance_;
        };

        int SetIdx1(int _idx1)
        {
            idx1_=_idx1;
            return 0;
        }

        int SetIdx2(int _idx2)
        {
            idx2_=_idx2;
            return 0;
        }

        int SetDistance(float _distance)
        {
            distance_=_distance;
            return 0;
        }

    private:

        int idx1_;
        int idx2_;
        float distance_;
    };
}


#endif /* FMATCH_HPP */


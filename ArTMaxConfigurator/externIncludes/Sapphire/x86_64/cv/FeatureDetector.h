/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureDetector.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 9:57 AM
 */

#ifndef FEATUREDETECTOR_H
#define FEATUREDETECTOR_H

#include "image/Image.h"
#include "cv/Feature2D.h"
#include "core/ConfigurableNode.hpp"

using namespace std;

namespace sapphire {

    class FeatureDetector: public ConfigurableNode {
    public:

        FeatureDetector()//string _configFilename = "") 
        {
            //            isReady_ = false;

            //            configFilename_ = _configFilename;

        };

        //        virtual int Configure(string _configFilename) {
        //            int ret = 0;
        //            LOGD("FeatureDetector::Configure");
        //            LOGD("FeatureDetector::Configure: ConfigFile: %s %d", _configFilename.c_str(), _configFilename.empty());
        //            if (!_configFilename.empty()) {
        //                LOGD("FeatureDetector::Configure: Open");
        //                cv::String str=_configFilename;
        //
        //                configFile_.open(str, cv::FileStorage::READ);
        //                LOGD("FeatureDetector::Configure: isOpen: %d", configFile_.isOpened());
        //                if (configFile_.isOpened()) {
        //                    isReady_ = true;
        //                    ret = Init();
        //                    LOGD("FeatureDetector::Ready");
        //                } else {
        //                    LOGD("Error: Config file doesn't exist");
        //                    ret = -1;
        //                }
        //
        //            } else
        //                ret = -1;
        //            return ret;
        //        }

        virtual ~FeatureDetector() {
        };

        virtual int Detect(Image& _img, Image& _mask, vector<Feature2D>& _outFeatures2D) = 0;
        virtual int Detect(Image& _img, vector<Feature2D>& _outFeatures2D) = 0;

    protected:

        //        virtual int Init() = 0;


    protected:

        //        bool isReady_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;
    };
}

#endif /* FEATUREDETECTOR_H */


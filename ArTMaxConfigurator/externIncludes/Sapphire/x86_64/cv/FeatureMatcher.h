/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FeatureMatcher.h
 * Author: marzorati
 *
 * Created on June 7, 2017, 5:01 PM
 */

#ifndef FEATUREMATCHER_H
#define FEATUREMATCHER_H

#include "math/Matrix.hpp"
#include "cv/FMatch.h"
#include "core/ConfigurableNode.hpp"

using namespace std;


namespace sapphire {

    class FeatureMatcher:public ConfigurableNode
    {
    public:

        FeatureMatcher()//string _configFilename = "")
        {
            //            isReady_ = false;

            //            configFilename_ = _configFilename;

        };

        //        virtual int Configure(string _configFilename) {
        //            int ret = 0;
        //            LOGD("FeatureMatcher::Configure");
        //            LOGD("FeatureMatcher::Configure: ConfigFile: %s %d", _configFilename.c_str(), _configFilename.empty());
        //            if (!_configFilename.empty()) {
        //                LOGD("FeatureMatcher::Configure: Open");
        //                cv::String str=_configFilename;
        //
        //                configFile_.open(str, cv::FileStorage::READ);
        //                LOGD("FeatureMatcher::Configure: isOpen: %d", configFile_.isOpened());
        //                if (configFile_.isOpened()) {
        //                    isReady_ = true;
        //                    ret = Init();
        //                    LOGD("FeatureMatcher::Ready");
        //                } else {
        //                    LOGD("Error: Config file doesn't exist");
        //                    ret = -1;
        //                }
        //
        //            } else
        //                ret = -1;
        //            return ret;
        //        }

        virtual ~FeatureMatcher() {
        };

        virtual int Match(Matrix& _queryDescriptors, Matrix& _trainDescriptors, vector< vector<FMatch> >& _matches) = 0;
        //        virtual int FilterMatches(vector< vector<FMatch> > & _matches, std::vector<Feature2D>& _trainPoints, std::vector<Feature2D>& _queryKeyPoints, vector<FMatch>& _outMatchesFiltered);


    protected:

        //        virtual int Init() = 0;


    protected:

        //        bool isReady_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;
    };

}

#endif


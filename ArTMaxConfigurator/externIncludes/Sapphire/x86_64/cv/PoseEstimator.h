/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PoseEstimator.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 9:26 AM
 */

#ifndef POSEESTIMATOR_H
#define POSEESTIMATOR_H


#include "cv/Camera.h"
#include "math/Point2D.h"
#include "math/Point3D.h"
#include "math/Homography.h"
#include "math/Matrix.hpp"
#include "core/ConfigurableNode.hpp"

namespace sapphire {

    class PoseEstimator : public ConfigurableNode {
    public:

        PoseEstimator(/*string _configFilename = ""*/)//:lastRT_(4,4,CV_64F),lastRTisValid_(false)
        {
            //            isReady_ = false;

            isValidOutputData_ = false;

            //            configFilename_ = _configFilename;
        };

        //        virtual int Configure(string _configFilename) {
        //            int ret = 0;
        //            configFile_.open(_configFilename, cv::FileStorage::READ);
        //            if (configFile_.isOpened()) {
        //                configFilename_ = _configFilename;
        //                isReady_ = true;
        //                ret=Init();
        //            } else {
        //                std::cout << "Error: Config file doesn't exist" << endl;
        //                ret = -1;
        //            }
        //
        //            return ret;
        //        }

        virtual ~PoseEstimator() {
        };

        //        virtual int SetCamera(Camera& _camera) = 0;

        virtual int InitPose(std::vector<Point2D<float> >& _imagePoints, std::vector<Point3D<float> >& _worldPoints,int _objId=-1) = 0;
        
        virtual int ComputePose(Matrix& _homography, Matrix& _rtOut,bool _useExtrinsics,int _objId=-1) = 0;
        virtual int ComputePose(Matrix& _homography, Matrix& _rotationOut, Matrix& _translationOut,bool _useExtrinsics,int _objId=-1) = 0;
        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rotationOut, Matrix& _translationOut,bool _useExtrinsics,int _objId=-1) = 0;
        virtual int ComputePose(vector<Point2D<float> >& _shape2D, Matrix& _rtOut,bool _useExtrinsics,int _objId=-1) = 0;

        virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0)
        {
            return camera_.SetCameraParams(_fx,_fy,_cx,_cy,_hFOV,_vFOV,_frameWidth,_frameHeight,_k1 , _k2 ,_k3,_p1,_p2);
        }
        virtual int SetCameraDevice(Camera& _camera)
        {
            camera_=_camera;
            return 0;
        }
    protected:

        //         virtual int Init()=0;

    protected:

        //        bool isReady_;
        bool isValidOutputData_;
        //        string configFilename_;
        //        cv::FileStorage configFile_;

//        Matrix lastRT_;
//        bool lastRTisValid_;

        Camera camera_;
        map<int, std::vector<Point2D<float> > >imagePoints_;
        map<int, std::vector<Point3D<float> > >worldPoints_;

    };
}
#endif /* POSEESTIMATOR_H */


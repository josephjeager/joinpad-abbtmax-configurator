/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OCVFlannFeatureMatcher.h
 * Author: marzorati
 *
 * Created on June 7, 2017, 5:09 PM
 */

#ifndef OCVFLANNFEATUREMATCHER_H
#define OCVFLANNFEATUREMATCHER_H


#include <vector>
#include <opencv2/opencv.hpp>


#include "cv/FeatureMatcher.h"
#include "math/Matrix.hpp"


using namespace std;

namespace sapphire {

    
class OCVFlannFeatureMatcher : public FeatureMatcher{
    public:
        OCVFlannFeatureMatcher(/*string _configFilename=""*/);
        virtual ~OCVFlannFeatureMatcher();

        virtual int Match(Matrix& _queryDescriptors, Matrix& _trainDescriptors, vector< vector< FMatch > >& _matches);

        //    vector<FMatch> FilterMatches(vector< vector<FMatch> > & _matches, std::vector<Feature2D>& _trainPoints, std::vector<Feature2D>& _queryKeyPoints);

        //        virtual int Setup();

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    private:
        cv::Ptr<cv::DescriptorMatcher> matcher_;
        std::string matcherType_;

        int numQueryMatches_;
    };

}
#endif /* OCVFLANNFEATUREMATCHER_H */


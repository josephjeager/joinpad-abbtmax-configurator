/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OCVMultiObjectClassificator.h
 * Author: marzorati
 *
 * Created on June 14, 2018, 1:05 PM
 */


#ifndef OCVMULTIOBJECTCLASSIFICATOR_H
#define OCVMULTIOBJECTCLASSIFICATOR_H


#include <math.h>
#include <fstream>
#include <utility>
#include <vector>
#include <iostream>
#include <regex>
#include <time.h>

#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/dnn.hpp>


#include "math/Matrix.hpp"
#include "math/Point2D.h"
#include "image/Image.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"

using namespace cv::dnn;

namespace sapphire {

    class OCVMultiObjectClassificator : public ProcessingNode {
    public:
        OCVMultiObjectClassificator(/*string _configFilename=""*/);

        virtual ~OCVMultiObjectClassificator();

        virtual int InitProc();
        virtual int StartThreadedProc();
        virtual int StopThreadedProc();
        virtual int StartProc();
        virtual int StopProc();

        int SetInferenceGraphBuffer(char* _pInferenceGraphBuffer,int _inferenceGraphBufferLength,char* _pInferenceGraphConfigBuffer,int _inferenceGraphConfigBufferLength,char* _pObjectLabelsBuffer,int _objectLabelsBufferLength);

//        int SetInferenceGraphBuffer(string _inferenceGraphBuffer,string _inferenceGraphConfigBuffer,string _objectLabelsBuffer);

        virtual int SetCurrentFrame(Image& _currentFrame) {
            int ret = 0;
            LOGD("MultiObjectClassificator::Setting current frame...");
            mutex_.lock();
            currentFrame_ = _currentFrame;
            isValidFrame_ = true;
            mutex_.unlock();
            LOGD("MultiObjectClassificator::Setting current frame...Done %d", ret);
            return ret;
        }

        virtual int ProcessFrame(Image& _image);
        
        virtual int GetLastTimestamp(double & _timestamp) {
            int ret = 0;
            _timestamp = lastTimestamp_;
            return ret;
        }

//        virtual multimap<int, Matrix> GetTransforms(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Patch2D > GetImageObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Shape2D> GetShapeObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, float> GetObjectConfidences(bool& _outIsValid, double &_timestamp);

        //        virtual multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Patch2D > GetCurrentImageObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, Shape2D> GetCurrentShapeObjects(bool& _outIsValid, double &_timestamp);
        virtual multimap<int, float> GetCurrentObjectConfidences(bool& _outIsValid, double &_timestamp);

        virtual string GetObjectLabel(int _classID);

    protected:
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

        virtual int RunProc();
        
    private:
        void PostProcess(cv::Mat& frame, const std::vector<cv::Mat>& outs, Net& net, vector<int>& _classIdsOut,vector<string>& _labelsOut, vector<double>& _confidencesOut, vector<Point2D<float> >& _boxCornersOut);

        std::vector<cv::String> GetOutputsNames(const Net& net);
        
//        void DrawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame);
        
    public:

        double lastTimestamp_;
        Image currentFrame_;

        bool isValidFrame_, isValidOutputData_; 
        
        string objectLabelFilename_,inferenceGraphFilename_,inferenceGraphConfigFilename_;
        
        bool configFromFile_;
        char* pInferenceGraphBuffer_;
        char* pInferenceGraphConfigBuffer_;
        char* pObjectLabelsBuffer_;
        int inferenceGraphBufferLength_;
        int inferenceGraphConfigBufferLength_;
        int objectLabelsBufferLength_;


        multimap<int, Patch2D > oldImgObjs_, imgObjs_;
        multimap<int, Shape2D > oldShapeObjs_, shapeObjs_;
        multimap<int,float> oldImgConfidences_,imgConfidences_;
//        multimap<int, Matrix > oldTransforms_, transforms_;

        int imageWidth_,imageHeight_;
        double thresholdScore_;
        double scale_;
        bool swapRB_;
        bool crop_;
        int backend_;
        int target_;
        double meanR_,meanG_,meanB_;
        
        std::vector<std::string> classes_;
        cv::Mat  blob_;
        Net net_;

    
    };

}

#endif /* OCVMULTIOBJECTCLASSIFICATOR_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ObjectDetector.h
 * Author: marzorati
 *
 * Created on June 14, 2017, 1:05 PM
 */

#ifndef OBJECTDETECTOR_H
#define OBJECTDETECTOR_H

#include <thread>
#include <mutex>


#include "cv/Detector.h"
#include "cv/Matcher.h"
#include "math/Matrix.hpp"
#include "math/Point2D.h"
#include "image/Image.h"
#include "cv/Matcher.h"
#include "math/Homography.h"
#include "math/Shape2D.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"
#include "math/Mesh.h"
#include "cv/Features2DAndDescriptors.hpp"
#include "cv/DetectorFactory.h"
#include "cv/MatcherFactory.h"

namespace sapphire {

    //    struct Features2DAndDescriptors {
    //    public:
    //
    //        int SetFeatures2DAndDescriptors(vector<Feature2D>& _features2D, Matrix _descriptors) {
    //
    //            features2D_ = _features2D;
    //            descriptors_ = _descriptors;
    //
    //            return 0;
    //        }
    //    public:
    //        vector<Feature2D> features2D_;
    //        Matrix descriptors_;
    //    };

    class ObjectDetector : public ProcessingNode {
    public:
        ObjectDetector(/*string _configFilename=""*/);

        virtual ~ObjectDetector();

        //        virtual int Configure(string _configFilename);

        virtual int AddReferenceObject(/*Patch2D &objImage_,*/Mesh& _objMesh_);

        virtual int ClearAllReferenceObjects();


        virtual int InitProc();
        virtual int Update();
        virtual int StartThreadedProc();
        virtual int StopThreadedProc();
        virtual int StartProc();
        virtual int StopProc();

        virtual int SetCurrentFrame(Image& _currentFrame) {
            int ret = 0;
            LOGD("ObjectDetector::Setting current frame...");
            mutex_.lock();
            currentFrame_ = _currentFrame;
            isValidFrame_ = true;
            mutex_.unlock();
            LOGD("ObjectDetector::Setting current frame...Done %d", ret);
            return ret;
        }


        virtual int EvaluateReferenceObjectQuality(Image &_texture,int _numHBuckets,int _numVBuckets,vector<Feature2D> & _keypointsOut,vector<int>& _numValuesOut,vector<Point2D<float> >& _meanValuesOut,vector<Point2D<float> > & _varValuesOut);
        virtual int ComputeStatistics(vector<Feature2D> & _keyPoints,Image& _image,int _numHBuckets,int _numVBuckets,vector<int>& _numValuesOut,vector<Point2D<float> > & _meanOut,vector<Point2D<float> >& _varianceOut);

        virtual int ProcessFrame(Image& _image, Image& _mask, int _maxNumMatches = 50, int _branching = 2, int _iterations = 1000);
        virtual int ProcessFrame(Image& _image, bool _generateMask = false, int _maxNumMatches = 50, int _branching = 2, int _iterations = 1000);

        virtual int GetLastTimestamp(double & _timestamp) {
            int ret = 0;
            _timestamp = lastTimestamp_;
            return ret;
        }

        //        virtual int SetDetector(Detector* _pDetector);
        //        virtual int SetDetector(Detector* _pObjDetector, Detector* _pSceneDetector);
        //        virtual int SetMatcher(Matcher* _pMatcher);

        virtual multimap<int, Matrix> GetTransforms(bool& _outIsValid, double &_timestamp);
        //        virtual multimap<int, std::vector<Point2D<float> > > GetBoundingBoxes(bool& _outIsValid);
        virtual multimap<int, Patch2D > GetImageObjects(bool& _outIsValid, double &_timestamp);
        //        virtual multimap<int, std::vector< Patch2D > > GetMeshObjects(bool& _outIsValid);
        virtual multimap<int, Shape2D> GetShapeObjects(bool& _outIsValid, double &_timestamp);

        virtual multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double &_timestamp);
        //        virtual multimap<int, std::vector<Point2D<float> > > GetCurrentBoundingBoxes(bool& _outIsValid);
        virtual multimap<int, Patch2D > GetCurrentImageObjects(bool& _outIsValid, double &_timestamp);
        //        virtual multimap<int, std::vector< Patch2D > > GetCurrentMeshObjects(bool& _outIsValid);
        virtual multimap<int, Shape2D> GetCurrentShapeObjects(bool& _outIsValid, double &_timestamp);

    protected:
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

        virtual int RunProc();

    private:
        virtual int CheckDetection(Patch2D & _origObj, Image& _frame, Homography& _computedHomography, double _threshold);


    public:

        double lastTimestamp_;
        Image currentFrame_;

        bool isValidFrame_, isValidOutputData_; //,isReady_;
        //        bool isRunning_;
        int minValidMatches_;

        double checkDetectionThresh_;

        int numMinTrainKeyPoints_;

        bool enableCheckDetection_;
        bool enableMultiInstance_;

        //        string configFilename_;
        //        cv::FileStorage configFile_;

        int featureMatcherType_, featureDescriptorType_, featureDetectorType_;
        string featureMatcherConfigFilename_, featureDetectorConfigFilename_, featureDescriptorConfigFilename_;
        Detector trainDetector_;
        Detector queryDetector_;
        Matcher matcher_;


        //        std::vector< std::vector < std::vector<Feature2D> > > trainKeyPointsVec_;
        //        std::vector< std::vector < Matrix > > descriptorsObjectVec_;

        std::map<int, Features2DAndDescriptors> trainKeyPointsMap_;


        //        vector<Patch2D> imgRefObjs_;
        std::vector<Mesh> meshRefObjs_;

        multimap<int, Patch2D > oldImgObjs_, imgObjs_;
        //        multimap<int, std::vector<Patch2D> > oldMeshObjs_, meshObjs_;
        multimap<int, Shape2D > oldShapeObjs_, shapeObjs_;


        //        vector< vector<GeomFace> > facesRefObjs_;

        multimap<int, Matrix > oldTransforms_, transforms_;

        //        multimap<int, std::vector< Point2D<float> > > oldBoundingBoxes_, boundingBoxes_;

        //        std::mutex mutex_;
        //        std::thread thr_;

    };

}

#endif /* OBJECTDETECTOR_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MarkerRecognizer.h
 * Author: marzorati
 *
 * Created on November 15, 2017, 5:44 PM
 */

#ifndef MARKERRECOGNIZER_H
#define MARKERRECOGNIZER_H


#include <mutex>

#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "math/Mesh.h"
#include "cv/Camera.h"

#include "cv/MarkerDetectorFactory.h"
#include "cv/MarkerPoseEstimatorFactory.h"

namespace sapphire
{

class MarkerRecognizer: public ProcessingNode
{
public:
    MarkerRecognizer();
    virtual ~MarkerRecognizer();


    virtual int InitProc();
    virtual int StartThreadedProc();
    virtual int StopThreadedProc();
    virtual int StartProc();
    virtual int StopProc();

    virtual int SetCurrentFrame(Image& _currentFrame) {
        int ret = 0;
        LOGD("MarkerRecognizer::Setting current frame...");
        mutex_.lock();
        currentFrame_ = _currentFrame;
        isValidFrame_ = true;
        mutex_.unlock();
        LOGD("MarkerRecognizer::Setting current frame...Done %d", ret);
        return ret;
    }


    virtual int SetBackgroundMode(bool _enableBackgroundMode)
    {
        enableBackgroundMode_=_enableBackgroundMode;
        return 0;
    }
    virtual int SetRate(int _rate)
    {
        rate_=_rate;
        return 0;
    }

    virtual int ProcessFrame(Image& _image, Image& _mask);
    virtual int ProcessFrame(Image& _image);


    virtual multimap<int, Matrix> GetTransforms(bool& _outIsValid, double &_timestamp);
    virtual multimap<int, Shape2D> GetShapeObjects(bool& _outIsValid, double &_timestamp);
    virtual multimap<int, Matrix> GetHomographies(bool& _outIsValid, double &_timestamp);

    virtual multimap<int, Matrix> GetCurrentTransforms(bool& _outIsValid, double &_timestamp);
    virtual multimap<int, Shape2D> GetCurrentShapeObjects(bool& _outIsValid, double &_timestamp);
    virtual multimap<int, Matrix> GetCurrentHomographies(bool& _outIsValid, double &_timestamp);
    

    virtual int GetMarkerPoseEstimator(MarkerPoseEstimator*& _pMarkerPoseEstimator)
    {
        _pMarkerPoseEstimator=pMarkerPoseEstimator_;
        return 0;
    }

    virtual int GetMarkerDetector(MarkerDetector*& _pMarkerDetector)
    {
        _pMarkerDetector=pMarkerDetector_;
        return 0;
    }

    virtual int SetCameraDevice(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0)
    {
        return camera_.SetCameraParams(_fx,_fy,_cx,_cy,_hFOV,_vFOV,_frameWidth,_frameHeight,_k1 , _k2 ,_k3,_p1,_p2);
    }
    virtual int SetCameraDevice(Camera& _camera)
    {
        camera_=_camera;
        return 0;
    }

private:

protected:
    virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    virtual int RunProc();

public:

    double lastTimestamp_;
    Image currentFrame_;

    Camera camera_;

    int rate_;

    bool enableBackgroundMode_;

    bool isValidFrame_, isValidOutputData_;


    MarkerDetector* pMarkerDetector_;
    MarkerPoseEstimator* pMarkerPoseEstimator_;

    int markerDetectorType_,markerPoseEstimatorType_;
    string markerDetectorConfigFilename_,markerPoseEstimatorConfigFilename_;
    

    multimap<int, Shape2D > oldShapeObjs_, shapeObjs_;

    multimap<int, Matrix > oldHomographies_, homographies_;

    multimap<int, Matrix > oldTransforms_, transforms_;
};

}

#endif /* MARKERRECOGNIZER_H */


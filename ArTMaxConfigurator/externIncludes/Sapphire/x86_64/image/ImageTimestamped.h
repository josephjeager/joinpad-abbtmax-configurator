/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImageTimestamped.h
 * Author: marzorati
 *
 * Created on July 11, 2017, 11:49 AM
 */

#ifndef IMAGETIMESTAMPED_H
#define IMAGETIMESTAMPED_H


#include "core/Timestamp.h"
#include "image/Image.h"

namespace sapphire {

    class ImageTimestamped : public Image, public Timestamp {
    public:
        ImageTimestamped();
        virtual ~ImageTimestamped();
    private:

    };
}

#endif /* IMAGETIMESTAMPED_H */


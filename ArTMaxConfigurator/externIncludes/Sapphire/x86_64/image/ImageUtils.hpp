/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImageUtils.hpp
 * Author: marzorati
 *
 * Created on September 12, 2017, 1:11 PM
 */

#ifndef IMAGEUTILS_HPP
#define IMAGEUTILS_HPP

#include <opencv2/imgproc/imgproc.hpp>


#include "math/Matrix.hpp"
#include "math/Homography.h"
#include "math/Patch2D.h"
#include "image/Image.h"

namespace sapphire {

    class ImageUtils {

        static int CompareTemplateImages(Patch2D& _img1, Image& _img2, Homography& _img1toImg2Transform, double _threshold) {
            int ret = -1;


            Image transformedImage = _img2;
            Image subtractedImage = _img2;
            cv::warpPerspective(_img1.image_.GetImageData(), transformedImage.GetImageData(), _img1toImg2Transform.GetH().GetMatrixData(), cv::Size(transformedImage.GetImageData().GetCols(), transformedImage.GetImageData().GetRows()));

            //    cv::imshow("TEST Homo transformedImage", transformedImage.GetImageData());

            //remove mean brightness from the current frame and the transformed original object image
            cv::Scalar meanCurrentFrame = cv::mean(_img2.GetImageData(), transformedImage.GetImageData());
            cv::Scalar meanTransformedFrame = cv::mean(transformedImage.GetImageData(), transformedImage.GetImageData());

            cv::subtract(_img2.GetImageData(), meanCurrentFrame, subtractedImage.GetImageData(), transformedImage.GetImageData());
            cv::subtract(transformedImage.GetImageData(), meanTransformedFrame, transformedImage.GetImageData(), transformedImage.GetImageData());

            //    cv::imshow("TEST Homo sub frame-mean", subtractedImage.GetImageData());
            //    cv::imshow("TEST Homo sub transformed-mean", transformedImage.GetImageData());

            //subtract the current (frame - meanFrame) image and the (transformed - meanTransformed) image
            cv::subtract(subtractedImage.GetImageData(), transformedImage.GetImageData(), subtractedImage.GetImageData(), transformedImage.GetImageData());

            //    cv::imshow("TEST Homo sub frame-transformed", subtractedImage.GetImageData());


            //    //erode
            //    int erosion_size=5;
            //    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
            //                                        cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
            //                                        cv::Point(erosion_size, erosion_size));
            //
            //    /// Apply the erosion operation
            //    cv::erode(subtractedImage.GetImageData(), subtractedImage.GetImageData(),element);

            //compute the residual as mean value of the subtracted pixels
            cv::Scalar sum = cv::mean(subtractedImage.GetImageData(), transformedImage.GetImageData());

            //    sum = cv::mean(subtractedImage.GetImageData(), transformedImage.GetImageData());

            if (sum.val[0] < _threshold) {
                ret = 0;
                LOGD("ImageUtils::CompareTemplateImages OK!");
            } else {
                ret = -1;
                LOGD("ImageUtils::CompareTemplateImages Failed!");
            }
            //        //DRAW
            //        stringstream str;
            //        str << sum.val[0];
            //    
            //        std::vector<Point2D<float> > obj_corners = _origObj.shape2D_.GetShape();
            //        std::vector<Point2D<float> > scene_corners(4);
            //        
            //        Image viewImage;
            //        cv::cvtColor(subtractedImage.GetImageData(),viewImage.GetImageData(),CV_GRAY2RGB);
            //    
            //        _computedHomography.Project(obj_corners, scene_corners);
            //        std::vector<cv::Point2f > sceneCornersOCV;
            //        utils::ConvertToOCV(scene_corners, sceneCornersOCV);
            //    
            //        line(viewImage.GetImageData(), sceneCornersOCV[0], sceneCornersOCV[1], cv::Scalar(0, 255, 0), 4);
            //        line(viewImage.GetImageData(), sceneCornersOCV[1], sceneCornersOCV[2], cv::Scalar(0, 255, 0), 4);
            //        line(viewImage.GetImageData(), sceneCornersOCV[2], sceneCornersOCV[3], cv::Scalar(0, 255, 0), 4);
            //        line(viewImage.GetImageData(), sceneCornersOCV[3], sceneCornersOCV[0], cv::Scalar(0, 255, 0), 4);
            //    
            //    
            //        cv::putText(viewImage.GetImageData(), str.str(), cv::Point(100, 100), cv::FONT_HERSHEY_PLAIN, 2.0, cv::Scalar(255, 0, 0), 3);
            //    
            //        cv::imshow("TEST Homo Frame", _frame.GetImageData());
            //        cv::imshow("TEST Homo", viewImage.GetImageData());
            //        cv::waitKey(10);
            //        //END DRAW

            return ret;
        }
    };
};

#endif /* IMAGEUTILS_HPP */


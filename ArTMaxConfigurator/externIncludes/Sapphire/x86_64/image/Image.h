/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Image.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 11:11 AM
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <chrono>

#include "math/Matrix.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace sapphire {

    class Image {
    public:

        Image(Matrix& _image) {
            image_ = _image;
            std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());

            SetTimestamp(ms.count());
        }

        //        Image(Matrix _image) {
        //            image_ = _image;
        //            std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
        //
        //            SetTimestamp(ms.count());
        //        }
        //
        //        
        //        Image(const Image& _image) {
        //            image_ = _image.image_;
        //            std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
        //
        //            SetTimestamp(ms.count());
        //        }
        //
        //        Image& operator=(const Image& _image) {
        //            image_ = _image.image_;
        //            std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
        //
        //            SetTimestamp(ms.count());
        //        }


        Image();

        virtual ~Image();

        virtual Matrix& GetImageData() {
            return image_;
        }

        virtual int GetHeight() {
            return image_.GetRows();
        }

        virtual int GetWidth() {
            return image_.GetCols();
        }

        virtual int SetImageData(Matrix& _image) {
            std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());

            SetTimestamp(ms.count());
            image_ = _image;
            return 0;
        }

        virtual int SetTimestamp(long _timestamp) {
            int ret = 0;
            timestamp_ = _timestamp;
            return ret;

        }

        virtual Matrix GetAlphaChannel() {
            Matrix alphaChannel(image_.rows, image_.cols, CV_8UC1);
            alphaChannel.GetMatrixData() = cv::Scalar(255);
            if (image_.channels() > 3) {
                for (int row = 0; row < image_.rows; ++row)
                    for (int col = 0; col < image_.cols; ++col) {
                        cv::Vec4b& v = image_.at<cv::Vec4b>(row, col);
                        alphaChannel.at<unsigned char>(row, col) = v[3];

                    }

            }
            return alphaChannel;
        }

        int Resize(int _width, int _height, double _scalingW, double _scalingH, int _interpolationType) {
            if (_scalingW == 0 || _scalingH == 0) {
                cv::resize(image_.GetMatrixData(), image_.GetMatrixData(), cv::Size(_width, _height), 0, 0, _interpolationType);
            } else {
                cv::resize(image_.GetMatrixData(), image_.GetMatrixData(), cv::Size(), _scalingW, _scalingH, _interpolationType);
            }
            return 0;
        }

        int Resize(int _width, int _height, double _scalingW, double _scalingH, int _interpolationType, Image& _outImage) {
            if (_scalingW == 0 || _scalingH == 0) {
                cv::resize(image_.GetMatrixData(), _outImage.image_.GetMatrixData(), cv::Size(_width, _height), 0, 0, _interpolationType);
            } else {
                cv::resize(image_.GetMatrixData(), _outImage.image_.GetMatrixData(), cv::Size(), _scalingW, _scalingH, _interpolationType);
            }
            return 0;
        }

        int Rotate(int _angle) {
            if (_angle == 270 || _angle == -90) {
                // Rotate clockwise 270 degrees
                cv::transpose(image_.GetMatrixData(), image_.GetMatrixData());
                cv::flip(image_.GetMatrixData(), image_.GetMatrixData(), 0);
            } else if (_angle == 180 || _angle == -180) {
                // Rotate clockwise 180 degrees
                cv::flip(image_.GetMatrixData(), image_.GetMatrixData(), -1);
            } else if (_angle == 90 || _angle == -270) {
                // Rotate clockwise 90 degrees
                cv::transpose(image_.GetMatrixData(), image_.GetMatrixData());
                cv::flip(image_.GetMatrixData(), image_.GetMatrixData(), 1);
            } else if (_angle == 360 || _angle == 0 || _angle == -360) {

            } else
                return -1;

            return 0;
        }



    private:

        Matrix image_;

    public:

        long timestamp_;

    };
}

#endif /* IMAGE_H */


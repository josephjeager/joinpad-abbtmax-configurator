#include <modules/CameraMotionTracking.h>
#include "utils/Utils.h"
#ifdef _ANDROID

#include <android/log.h>

#endif

#ifdef __cplusplus
extern "C" {
#endif

CameraMotionTracking* nativeCameraMotionTracking_Create();

void nativeCameraMotionTracking_Destroy(CameraMotionTracking* thiz);

int nativeCameraMotionTracking_Init(CameraMotionTracking* thiz);
int nativeCameraMotionTracking_SetCameraParams(CameraMotionTracking* thiz, int _width, int _height, float _hFOV, float _vFOV);
int nativeCameraMotionTracking_Configure(CameraMotionTracking* thiz);
int nativeCameraMotionTracking_Start(CameraMotionTracking* thiz);
int nativeCameraMotionTracking_Stop(CameraMotionTracking* thiz);

int nativeCameraMotionTracking_ProcessFrame(CameraMotionTracking* thiz, cv::Mat* rImage);

int nativeCameraMotionTracking_GetHomography(CameraMotionTracking* thiz,cv::Mat* rHomographyOut);
int nativeCameraMotionTracking_GetMarker(CameraMotionTracking* thiz,cv::Mat* rMarkerOut,cv::Mat* rRTPmtt_MOut);//,jlong rRTPmkt_MOut);

#ifdef __cplusplus
}
#endif

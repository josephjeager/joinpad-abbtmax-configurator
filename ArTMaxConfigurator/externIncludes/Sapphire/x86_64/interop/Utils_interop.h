#include "utils/Utils.h"
#include <math/Matrix.hpp>


using namespace sapphire;

#ifdef _ANDROID

#include <android/log.h>

#endif


#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endif

#ifdef __cplusplus
extern "C" {
#endif

DllExport int utilsConvertMat2Tex(long _pImage, long _pTexture2D, bool _textureWithAlpha);
DllExport int utilsConvertTex2Mat(long _pTexture2D, long _pImage, bool _textureWithAlpha);

#ifdef __cplusplus
}
#endif

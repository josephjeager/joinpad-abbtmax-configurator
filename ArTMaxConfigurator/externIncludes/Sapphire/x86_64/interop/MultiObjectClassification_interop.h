#include <modules/MultiObjectClassification.h>
#include "utils/Utils.h"
#ifdef _ANDROID

#include <android/log.h>

#endif

#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endif

#ifdef __cplusplus
extern "C" {
#endif

DllExport MultiObjectClassification * nativeMultiObjectClassification_Create(/*MultiMarkerLocalization **outPtr*/);

DllExport void nativeMultiObjectClassification_Destroy(MultiObjectClassification * thiz);

DllExport int nativeMultiObjectClassification_Init(MultiObjectClassification * thiz);
//DllExport int nativeEvaluateReferenceObjectQuality(long thiz, long queryFrame,int numHBuckets,int numVBuckets,long rNumValuesMatrix,long rMeanValuesMatrix,long rVarValuesMatrix);
//DllExport int nativeAddNewObjectToDetect(long thiz, int objectId, long queryFrame, long worldPlane);
DllExport int nativeMultiObjectClassification_SetCameraParams(MultiObjectClassification * thiz, int _width, int _height, float _hFOV, float _vFOV);
DllExport int nativeMultiObjectClassification_Configure(MultiObjectClassification * thiz);
DllExport int nativeMultiObjectClassification_Start(MultiObjectClassification * thiz);
DllExport int nativeMultiObjectClassification_Stop(MultiObjectClassification * thiz);

//DllExport int nativeMultiObjectClassification_SetInferenceGraphBuffer(MultiObjectClassification * thiz, const char* rInferenceGraphBuffer,const char* rInferenceGraphConfigBuffer);
DllExport int nativeMultiObjectClassification_ProcessFrame(MultiObjectClassification * thiz, cv::Mat* rImage);

DllExport int nativeMultiObjectClassification_GetDetectedObjects(MultiObjectClassification * thiz, cv::Mat* rDetectedObjectsOut);
//DllExport int nativeGetReferenceObjects(long thiz, long rRefObjsOut);

DllExport int nativeMultiObjectClassification_GetImage(MultiObjectClassification * thiz, cv::Mat* rImageOut);
DllExport int nativeMultiObjectClassification_SetImage(MultiObjectClassification * thiz, cv::Mat* rImage);

#ifdef __cplusplus
}
#endif

#include <modules/MultiMarkerLocalization.h>
#include "utils/Utils.h"
#ifdef _ANDROID

#include <android/log.h>

#endif

#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endif

#ifdef __cplusplus
extern "C" {
#endif

DllExport MultiMarkerLocalization * nativeMultiMarkerLocalization_Create(/*MultiMarkerLocalization **outPtr*/);

DllExport void nativeMultiMarkerLocalization_Destroy(MultiMarkerLocalization * thiz);

DllExport int nativeMultiMarkerLocalization_Init(MultiMarkerLocalization * thiz);
//DllExport int nativeEvaluateReferenceObjectQuality(long thiz, long queryFrame,int numHBuckets,int numVBuckets,long rNumValuesMatrix,long rMeanValuesMatrix,long rVarValuesMatrix);
//DllExport int nativeAddNewObjectToDetect(long thiz, int objectId, long queryFrame, long worldPlane);
DllExport int nativeMultiMarkerLocalization_SetCameraParams(MultiMarkerLocalization * thiz, int _width, int _height, float _hFOV, float _vFOV);
DllExport int nativeMultiMarkerLocalization_Configure(MultiMarkerLocalization * thiz);
DllExport int nativeMultiMarkerLocalization_Start(MultiMarkerLocalization * thiz);
DllExport int nativeMultiMarkerLocalization_Stop(MultiMarkerLocalization * thiz);

DllExport int nativeMultiMarkerLocalization_ProcessFrame(MultiMarkerLocalization * thiz, cv::Mat* rImage);

DllExport int nativeMultiMarkerLocalization_GetRTMatrixes(MultiMarkerLocalization * thiz, cv::Mat* rRTMatrixesOut);
//DllExport int nativeGetReferenceObjects(long thiz, long rRefObjsOut);

DllExport int nativeMultiMarkerLocalization_GetImage(MultiMarkerLocalization * thiz, cv::Mat* rImageOut);
DllExport int nativeMultiMarkerLocalization_SetImage(MultiMarkerLocalization * thiz, cv::Mat* rImage);

#ifdef __cplusplus
}
#endif

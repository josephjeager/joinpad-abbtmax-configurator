#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "utils/Utils.h"

using namespace sapphire;

#ifdef _ANDROID

#include <android/log.h>

#endif


#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endif

#ifdef __cplusplus
extern "C" {
#endif


/* C# new Mat(5, 4, CvType.CV_64FC1);
C++ private static extern IntPtr core_Mat_n_1Mat__III (int rows, int cols, int type);
*/
DllExport cv::Mat* core_Mat_n_1Mat__III (int _rows, int _cols, int _type);

/* C# Mat.release()
C++ private static extern void core_Mat_n_1release (IntPtr nativeObj);
 */
DllExport int core_Mat_n_1release (cv::Mat* _pMat);

/* C# OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
C++ private static extern void core_Core_flip_10 (IntPtr src_nativeObj, IntPtr dst_nativeObj, int flipCode);
*/
DllExport int core_Core_flip_10(cv::Mat* _pSrcNativeObj, cv::Mat* _pDstNativeObj, int _flipCode);

/* C# Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
C++ private static extern void imgproc_Imgproc_cvtColor_11 (IntPtr src_nativeObj, IntPtr dst_nativeObj, int code);
*/
DllExport int imgproc_Imgproc_cvtColor_11 (cv::Mat* _pSrcNativeObj, cv::Mat* _pDstNativeObj, int _code);

/* C# Utils.copyToMat(ptr, matrix);
C++ private static extern void OpenCVForUnity_ByteArrayToMatData (IntPtr byteArray, IntPtr Mat);
*/
DllExport void OpenCVForUnity_ByteArrayToMatData (uchar* _pSrcByteArray, cv::Mat* _pDstMat);

/* C# mat.isContinuous ()
C++ private static extern bool core_Mat_n_1isContinuous (IntPtr nativeObj); (NON indispensabile)
*/
DllExport bool core_Mat_n_1isContinuous (cv::Mat* _pNativeObj);

/* C# Mat.get (int row, int col)
C++ private static extern int core_Mat_nGet (IntPtr self, int row, int col, int count, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] double[] vals);
*/
//DllExport int opencvCoreMatGet(long _pNativeObj, int _row, int _col);

/* C# Mat.rows()
C++ private static extern int core_Mat_n_1rows (IntPtr nativeObj);
 */
DllExport int core_Mat_n_1rows (cv::Mat* _pNativeObj);

/* C# Mat.cols()
C++ private static extern int core_Mat_n_1cols (IntPtr nativeObj);
*/
DllExport int core_Mat_n_1cols (cv::Mat* _pNativeObj);

/* C# Mat.dataAddr ()
C++ private static extern long core_Mat_n_1dataAddr (IntPtr nativeObj);
*/
DllExport uchar* core_Mat_n_1dataAddr (cv::Mat* _pNativeObj);

/* C# Mat.total()
C++ private static extern long core_Mat_n_1total (IntPtr nativeObj);
*/
DllExport long core_Mat_n_1total (cv::Mat* _pNativeObj);

/* C# Mat.elemSize()
C++ private static extern long core_Mat_n_1elemSize (IntPtr nativeObj);
*/
DllExport long core_Mat_n_1elemSize (cv::Mat* _pNativeObj);

DllExport int core_Mat_n_1type(cv::Mat* _pNativeObj);

DllExport int core_Mat_nPutD (cv::Mat* _pNativeObj, int _row, int _col, int _count, double* _pData);
DllExport int core_Mat_nPutB (cv::Mat* _pNativeObj, int _row, int _col, int _count, uchar* _pData);
DllExport bool core_Mat_n_1isSubmatrix (cv::Mat* _pNativeObj);
DllExport int core_Mat_n_1channels (cv::Mat* _pNativeObj);
DllExport int core_Mat_nGet (cv::Mat* _pNativeObj, int _row, int _col, int _count, double* _vals);
#ifdef __cplusplus
}
#endif

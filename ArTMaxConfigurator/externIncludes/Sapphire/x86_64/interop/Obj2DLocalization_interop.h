#include <modules/Obj2DLocalization.h>
#include "utils/Utils.h"
#ifdef _ANDROID

#include <android/log.h>

#endif

#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endif


#ifdef __cplusplus
extern "C" {
#endif

DllExport Obj2DLocalization* nativeObj2DLocalization_Create(/*Obj2DLocalization **outPtr*/);

DllExport void nativeObj2DLocalization_Destroy(Obj2DLocalization* thiz);

DllExport int nativeObj2DLocalization_Init(Obj2DLocalization* thiz);
DllExport int nativeObj2DLocalization_EvaluateReferenceObjectQuality(Obj2DLocalization* thiz, Mat * queryFrame,int numHBuckets,int numVBuckets,Mat * rNumValuesMatrix,Mat * rMeanValuesMatrix,Mat * rVarValuesMatrix);
DllExport int nativeObj2DLocalization_AddNewObjectToDetect(Obj2DLocalization* thiz, int objectId, Mat * queryFrame, Mat * worldPlane);
DllExport int nativeObj2DLocalization_SetCameraParams(Obj2DLocalization* thiz, int _width, int _height, float _hFOV, float _vFOV);
DllExport int nativeObj2DLocalization_Configure(Obj2DLocalization* thiz);
DllExport int nativeObj2DLocalization_Start(Obj2DLocalization* thiz);
DllExport int nativeObj2DLocalization_Stop(Obj2DLocalization* thiz);

DllExport int nativeObj2DLocalization_ProcessFrame(Obj2DLocalization* thiz, Mat * rImage);

//int nativeGetHomographies(long thiz, long rHomographiesOut);
DllExport int nativeObj2DLocalization_GetRTMatrixes(Obj2DLocalization* thiz, Mat * rRTMatrixesOut);
DllExport int nativeObj2DLocalization_GetReferenceObjects(Obj2DLocalization* thiz, Mat * rRefObjsOut);

DllExport int nativeObj2DLocalization_GetImage(Obj2DLocalization* thiz, Mat * rImageOut);
DllExport int nativeObj2DLocalization_SetImage(Obj2DLocalization* thiz, Mat * rImage);


//Matrix ConvertRHSToLHS(Matrix & _rhs);

#ifdef __cplusplus
}
#endif

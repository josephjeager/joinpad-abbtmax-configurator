/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EventSubscriberNode.h
 * Author: marzorati
 *
 * Created on July 7, 2017, 11:14 AM
 */

#ifndef EVENTSUBSCRIBERNODE_H
#define EVENTSUBSCRIBERNODE_H

#include "core/EventType.h"

class EventSubscriberNode
{
public:
    // The prefix "cbi" is to prevent naming clashes.
    virtual int Notify(EventType _eventType){return 0;};

protected:

};

#endif /* EVENTSUBSCRIBERNODE_H */


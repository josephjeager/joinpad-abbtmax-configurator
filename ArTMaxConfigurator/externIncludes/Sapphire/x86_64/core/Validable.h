/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Validable.h
 * Author: marzorati
 *
 * Created on July 6, 2017, 9:18 AM
 */

#ifndef VALIDABLE_H
#define VALIDABLE_H



namespace sapphire {

    class Validable {
    public:
        Validable(bool _isValid=false)
        {
            isValid_=_isValid;
        }

        virtual ~Validable(){}

        
        virtual bool IsValid()
        {
            return isValid_;
        }

        virtual int SetValid(bool _isValid)
        {
            int ret=0;
            isValid_=_isValid;
            return ret;
        }

    protected:

        bool isValid_;


    };
}


#endif /* VALIDABLE_H */


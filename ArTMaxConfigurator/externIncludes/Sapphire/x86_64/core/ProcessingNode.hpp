/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ProcessingNode.hpp
 * Author: marzorati
 *
 * Created on July 26, 2017, 5:27 PM
 */

#ifndef PROCESSINGNODE_HPP
#define PROCESSINGNODE_HPP


#include <vector>
#include <iostream>
#include <iomanip>
#include <thread>
#include <mutex>

#include "ConfigurableNode.hpp"
#include "EventPublisherNode.h"
#include "EventSubscriberNode.h"

//#define LOG_TAG "ProcessingNode"

//#define NODE_NAME "ProcessingNode"

namespace sapphire {

    class ProcessingNode : public ConfigurableNode, EventPublisherNode, EventSubscriberNode {
    public:

        ProcessingNode() {

            isRunning_ = false;
            isThreaded_ = false;
            isRunOnce_ = false;
            isInit_ = false;

        }

        virtual ~ProcessingNode() {
        };

        int Init() {
            //            LOGD("%s::Init()->Start", NODE_NAME);
            int ret = -1;
            if (isConfigurated_) {
                ret = InitProc();
                if (ret == 0)
                    isInit_ = true;
                else
                    isInit_ = false;
            }
            return ret;
        };

        int Run()
        {
            int ret=-1;
            if (isConfigurated_ && isInit_)
            {
                ret = RunProc();
            }
            return ret;
        }

        int Start() {
            int ret = 0;
            isRunning_ = true;
            isThreaded_ = false;
            isRunOnce_ = true;
            ret = StartProc();
            return ret;
        };

        int Stop() {
            int ret = 0;
            isRunning_ = false;
            isThreaded_ = false;
            isRunOnce_ = false;
            ret = StopProc();
            return ret;
        };

        int StartThreaded() {
            int ret = 0;

            LOGD("ProcessingNode::StartThreaded");
            isRunning_ = true;
            isThreaded_ = true;
            isRunOnce_ = false;

            ret = StartThreadedProc();

            if (ret == 0) {

                thr_ = std::thread(&ProcessingNode::Run, this);
            }
            LOGD("ProcessingNode::StartThreaded Done:%d", ret);
            return ret;
        };

        int StopThreaded() {
            int ret = 0;

            LOGD("ProcessingNode::StopThreaded");
            ret = StopThreadedProc();
            if (ret == 0) {
                isRunning_ = false;
                isThreaded_ = false;
                isRunOnce_ = false;
                thr_.join();
            }
            LOGD("ProcessingNode::StopThreaded Done:%d", ret);
            return ret;
        };

        virtual int InitProc() = 0;

        virtual int StartProc() = 0;
        virtual int StopProc() = 0;

        virtual int StartThreadedProc() = 0;
        virtual int StopThreadedProc() = 0;

        protected:

        virtual int RunProc() = 0;


    protected:

        bool isInit_;
        bool isRunning_;
        bool isThreaded_;
        bool isRunOnce_;
        std::mutex mutex_;
        std::thread thr_;



    };

}


#endif /* PROCESSINGNODE_HPP */


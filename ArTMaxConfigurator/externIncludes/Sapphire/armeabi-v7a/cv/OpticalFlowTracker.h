/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OpticalFlowTracker.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:01 PM
 */

#ifndef OPTICALFLOWTRACKER_H
#define OPTICALFLOWTRACKER_H

#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "cv/Tracker.h"
#include "math/Patch2D.h"

namespace sapphire {

    class OpticalFlowTracker : public Tracker {
    public:
        OpticalFlowTracker(/*string _configFilename = ""*/);
        virtual ~OpticalFlowTracker();



        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame); //, std::vector<Point2D<float> >& _imagePoints);
        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame, Shape2D& _initObject, Homography& _initHomography); //, std::vector<Point2D<float> >& _imagePoints);

        virtual Shape2D ComputeShape2D(int _uniqueFaceId);

    protected:

        virtual int ResetProc();


        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);


        virtual int ReInitTrackerProc(Patch2D& _objPatch); //, Shape2D& _initObj, Homography& _initHomography);

        virtual int TrackProc(Image& _image);

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    private:
        virtual int CheckDetection(Patch2D& _origObj, Image& _frame, Homography& _computedHomography, double _threshold);

        int CreateMask(Shape2D& _shape2D, cv::Mat &_mask, unsigned char nb);


    private:

        double 	pyrScale_=0.4;
        int 	levels_=1;
        int 	winsize_=12;
        int 	iterations_=2;
        int 	polyN_=8;
        double 	polySigma_=1.2;
        int 	flags_=0;

        cv::Mat prevgray_;
        Matrix flow_;

        bool enableCheckDetection_;

    };

}
#endif /* OPTICALFLOWTRACKER_H */


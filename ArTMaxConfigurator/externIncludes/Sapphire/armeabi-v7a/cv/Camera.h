/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Camera.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 10:37 AM
 */

#ifndef CAMERA_H
#define CAMERA_H


#include <opencv2/opencv.hpp>
#include "utils/Utils.h"
#include "math/Mesh.h"


#define FROM_FILE       0
#define FROM_STRING     1

namespace sapphire {

    class Camera: public Validable {
    public:

        Camera();
        Camera(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0);

        //int Configure(std::string& _sectionName, std::string& _yamlString);

        int SetCameraParams(double _fx, double _fy, double _cx, double _cy, double _hFOV = 0, double _vFOV = 0, int _frameWidth = 0, int _frameHeight = 0, double _k1 = 0, double _k2 = 0, double _k3 = 0,double _p1=0,double _p2=0);

        virtual int SetFOV(double _hFOV, double _vFOV) {
            hFOV_ = _hFOV;
            vFOV_ = _vFOV;
            isValid_=true;
            return 0;
        }
        
        virtual Matrix GetDistCoeffs()
        {
            
            Matrix distCoeffs(5,1);
            //k_1, k_2, p_1, p_2, k_3
            
            distCoeffs.at<double>(0,0)=k1_;
            distCoeffs.at<double>(1,0)=k2_;
            distCoeffs.at<double>(2,0)=p1_;
            distCoeffs.at<double>(3,0)=p2_;
            distCoeffs.at<double>(4,0)=k3_;
            
            return distCoeffs;
        }
        

        virtual int SetFrameSize(double _frameWidth, double _frameHeight) {
            frameWidth_ = _frameWidth;
            frameHeight_ = _frameHeight;
            return 0;
        }

        virtual Matrix GetCameraMatrix() {
            int ret = -1;
            Matrix cameraMatrix(3, 4);

            cameraMatrix.at<double>(0, 0) = fx_;
            cameraMatrix.at<double>(0, 1) = 0;
            cameraMatrix.at<double>(0, 2) = cx_;
            cameraMatrix.at<double>(0, 3) = 0;
            cameraMatrix.at<double>(1, 0) = 0;
            cameraMatrix.at<double>(1, 1) = fy_;
            cameraMatrix.at<double>(1, 2) = cy_;
            cameraMatrix.at<double>(1, 3) = 0;
            cameraMatrix.at<double>(2, 0) = 0;
            cameraMatrix.at<double>(2, 1) = 0;
            cameraMatrix.at<double>(2, 2) = 1;
            cameraMatrix.at<double>(2, 3) = 0;
            ret = 0;


            return cameraMatrix;
        }

        virtual Matrix GetCameraMatrix3x3() {
            int ret = -1;
            Matrix cameraMatrix(3, 3);

            cameraMatrix.at<double>(0, 0) = fx_;
            cameraMatrix.at<double>(0, 1) = 0;
            cameraMatrix.at<double>(0, 2) = cx_;
            cameraMatrix.at<double>(1, 0) = 0;
            cameraMatrix.at<double>(1, 1) = fy_;
            cameraMatrix.at<double>(1, 2) = cy_;
            cameraMatrix.at<double>(2, 0) = 0;
            cameraMatrix.at<double>(2, 1) = 0;
            cameraMatrix.at<double>(2, 2) = 1;
            ret = 0;


            return cameraMatrix;
        }

        virtual int Project(Mesh& _obj, std::vector< Shape2D >& _projectObjOut);
        virtual int Project(Shape3D& _shape3D, Shape2D & _projectObjOut);

        virtual int SetRTC_W(Matrix& _rtC_W) {
            int ret = -1;
            if (_rtC_W.GetCols() == 4 && _rtC_W.GetRows() == 4) {
                rtC_W_ = _rtC_W;
                ret = 0;
            } else
                ret = -1;
            return 0;
        }


        virtual ~Camera();


    public:

        double fx_, fy_, cx_, cy_, k1_, k2_, k3_,p1_,p2_;
        double hFOV_, vFOV_;
        int frameWidth_, frameHeight_;

        Matrix rtC_W_;

        bool isConfigurated_;

        std::string configFilename_;
        cv::FileNode fileNode_;
        cv::FileStorage configFile_;
        bool loadType_;
    };

}
#endif /* CAMERA_H */


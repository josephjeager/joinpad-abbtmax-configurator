/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Detector.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 9:53 AM
 */

#ifndef DETECTOR_H_
#define DETECTOR_H_

#include "cv/FeatureDetector.h"
#include "cv/FeatureDescriptor.h"
#include "math/Matrix.hpp"
#include "cv/Feature2D.h"
#include "image/Image.h"

//#define LOG_TAG "Detector"

namespace sapphire {

    class Detector {
    public:
        Detector();
        Detector(const Detector& orig);
        virtual ~Detector();

        virtual int Process(Image& _image, Image& _mask);
        virtual int Process(Image& _image);

        virtual vector<Feature2D>& GetKeyPoints() {
            return keyPoints_;
        };

        virtual Matrix& GetDescriptors() {
            return descriptors_;
        };
        virtual int SetFeatureDetector(FeatureDetector* _pFeatureDetector);
        virtual int SetFeatureDescriptor(FeatureDescriptor* _pFeatureDescriptor);

        virtual FeatureDetector* GetFeatureDetector();
        virtual FeatureDescriptor* GetFeatureDescriptor();

    private:


        FeatureDetector* pFeatureDetector_;
        FeatureDescriptor* pFeatureDescriptor_;
        vector<Feature2D> keyPoints_;
        Matrix descriptors_;

    };

}
#endif /* DETECTOR_H_ */


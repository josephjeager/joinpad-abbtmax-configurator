/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LBDFeatureDescriptor.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 4:24 PM
 */

#ifndef LBDFEATUREDESCRIPTOR_H
#define LBDFEATUREDESCRIPTOR_H

#ifdef _TEST

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/line_descriptor.hpp>

#include "cv/Feature2D.h"
#include "cv/FeatureDescriptor.h"
#include "image/Image.h"
#include "math/Matrix.hpp"


using namespace std;

namespace sapphire {



    class LBDFeatureDescriptor : public FeatureDescriptor {
    public:
        LBDFeatureDescriptor(/*string _configFilename=""*/);
        virtual ~LBDFeatureDescriptor();


        virtual int Compute(Image& _image, vector<Feature2D>& _features2D, Matrix& _outDescriptors);

        //    virtual int Setup();

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:

        cv::Ptr<cv::line_descriptor::BinaryDescriptor> descriptor_;
        string descriptorType_;
    };
}
#endif /* LBDFEATUREDESCRIPTOR_H */

#endif
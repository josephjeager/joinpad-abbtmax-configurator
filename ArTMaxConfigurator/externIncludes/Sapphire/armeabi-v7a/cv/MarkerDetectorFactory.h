/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MarkerDetectorFactory.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 11:06 PM
 */

#ifndef MARKERDETECTORFACTORY_H
#define MARKERDETECTORFACTORY_H


#include "ArucoMarkerDetector.h"
#include "ArucoMultiMarkersDetector.h"

#define ARUCOMARKERDETECTOR      1
#define ARUCOMULTIMARKERSDETECTOR      2


namespace sapphire {

    class MarkerDetectorFactory {
    public:

        static MarkerDetector* CreateMarkerDetector(int _markerDetectorType) {
            switch (_markerDetectorType) {
                case ARUCOMARKERDETECTOR:
                    return new ArucoMarkerDetector;
                    break;
                case ARUCOMULTIMARKERSDETECTOR:
                    return new ArucoMultiMarkersDetector;
                    break;
            }
            throw "MarkerDetectorFactory::invalid markerDetectorType.";
        }

    };

}

#endif /* MARKERDETECTORFACTORY_H */


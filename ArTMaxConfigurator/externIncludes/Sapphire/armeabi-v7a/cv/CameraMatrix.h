/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraMatrix.h
 * Author: marzorati
 *
 * Created on September 9, 2017, 1:08 PM
 */

#ifndef CAMERAMATRIX_H
#define CAMERAMATRIX_H

#include "math/Matrix.hpp"
#include "math/Mesh.h"
#include "cv/Camera.h"


namespace sapphire {

    class CameraMatrix {
    public:
        CameraMatrix();
        CameraMatrix(Matrix & _cameraMatrix);
        CameraMatrix(Matrix & _cameraMatrix, Matrix & _rtC_W);
        CameraMatrix(Camera& _camera, Matrix& _rtC_W);

        virtual ~CameraMatrix();

        virtual int Project(Mesh& _obj, std::vector< Shape2D >& _projectObjOut);

        virtual int SetCamera(Camera& _camera) {
            int ret = -1;
            cameraMatrix_ = _camera.GetCameraMatrix();
            return ret;
        }

        virtual int SetCameraMatrix(Matrix& _cameraMatrix) {
            int ret = -1;
            if (_cameraMatrix.GetCols() == 4 && _cameraMatrix.GetRows() == 3) {
                cameraMatrix_ = _cameraMatrix;

                ret = 0;
            } else
                ret = -1;
            return ret;
        }

        virtual int SetRTC_W(Matrix& _rtC_W) {
            int ret = -1;
            if (_rtC_W.GetCols() == 4 && _rtC_W.GetRows() == 4) {
                rtC_W_ = _rtC_W;
                ret = 0;
            } else
                ret = -1;
            return 0;
        }
    private:

        Matrix cameraMatrix_;
        Matrix rtC_W_;
    };
}

#endif /* CAMERAMATRIX_H */


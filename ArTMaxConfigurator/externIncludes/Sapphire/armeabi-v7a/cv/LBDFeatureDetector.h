/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LBDFeatureDetector.h
 * Author: marzorati
 *
 * Created on June 6, 2017, 11:20 AM
 */

#ifndef LBDFEATUREDETECTOR_H
#define LBDFEATUREDETECTOR_H
#ifdef _TEST
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/features2d.hpp>

#include "cv/Feature2D.h"
#include "cv/FeatureDetector.h"
#include "image/Image.h"


using namespace std;

namespace sapphire {


class LBDFeatureDetector :public  FeatureDetector{
    public:
        LBDFeatureDetector(/*string _configFilename=""*/);
        virtual ~LBDFeatureDetector();

    virtual int Detect(Image& _img, Image& _mask,vector<Feature2D>& _outFeatures2D);
    virtual int Detect(Image& _img,vector<Feature2D>& _outFeatures2D);

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
    private:

        cv::Ptr<cv::line_descriptor::BinaryDescriptor> detector_;
        string detectorType_;

    };

}

#endif /* LBDFEATUREDETECTOR_H */

#endif
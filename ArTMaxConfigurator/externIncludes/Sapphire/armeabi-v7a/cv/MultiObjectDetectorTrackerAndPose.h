/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MultiObjectDetectorTrackerAndPose.h
 * Author: marzorati
 *
 * Created on July 4, 2017, 10:02 AM
 */

#ifndef MULTIOBJECTDETECTORTRACKERANDPOSE_H
#define MULTIOBJECTDETECTORTRACKERANDPOSE_H

//#include <condition_variable>
#include <thread>
#include <chrono>

#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Shape2D.h"
//#include "cv/VISPTracker.h"
#include "MultiObjectTracker.h"
#include "ObjectDetector.h"
#include "math/Patch2D.h"
#include "core/EventPublisherNode.h"
#include "core/ConfigurableNode.hpp"
#include "cv/Camera.h"
#include "cv/TrackerFactory.h"
#include "cv/PoseEstimator.h"
#include "utils/DrawingTools.h"

#define MAX_OBJECT_DETECTOR_DELAY   0
#define NODE_NAME "MultiObjectDetectorTrackerAndPose"

#define _ENABLEGIMBALLLOCKCHECK


namespace sapphire {

    class MultiObjectDetectorTrackerAndPose : public ProcessingNode {
    public:
        MultiObjectDetectorTrackerAndPose(); //string _configFilename="");
        virtual ~MultiObjectDetectorTrackerAndPose();

        //        virtual int Configure(string _configFilename);

        virtual int EvaluateObjectReferenceQuality(Image &_image,int _numHBuckets,int _numVBuckets,vector<Feature2D> & _keypointsOut,vector<int>& _numValuesOut,vector<Point2D<float> >& _meanValuesOut,vector<Point2D<float> > & _varValuesOut);
        virtual int AddReferenceObjects(std::vector<Patch2D> & _objs, map<int, Mesh> & _meshObjs);

        virtual int SetObjectDetector(ObjectDetector * _pObjDetector) {
            int ret = 0;
            pObjDetector_ = _pObjDetector;
            return ret;
        }

        virtual int SetMultiTracker(MultiObjectTracker* _pMultiTracker) {
            int ret = 0;
            pMultiTracker_ = _pMultiTracker;
            return ret;
        }

        virtual int SetPoseEstimator(PoseEstimator* _pPoseEstimator) {
            int ret = 0;
            pPoseEstimator_ = _pPoseEstimator;
            return ret;
        }




        virtual int InitProc();


        virtual int StartThreadedProc();
        virtual int StopThreadedProc();

        virtual int StartProc();
        virtual int StopProc();


        //        template <class T> int ConnectCallBackFunction(T *_obj, void (T::*_func)(void));

        virtual int SetCurrentFrame(Image& _currentFrame) {
            int ret = 0;
            mutex_.lock();
            currentFrame_ = _currentFrame;
            isValidFrame_ = true;
            mutex_.unlock();
            return ret;
        }

        virtual Image& GetCurrentFrame() {
            return currentFrame_;
        }

        virtual int ProcessFrame(Image& _image);

        //        virtual multimap<int, std::vector<Point2D<float> > > GetBoundingBoxes(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return boundingBoxes_;
        //        }

        virtual std::multimap<int, Matrix >& GetHomographies(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return homographies_;
        }

        virtual std::multimap<int, Matrix >& GetTransforms(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return transforms_;
        }

        virtual std::multimap<int, Matrix >& GetCurrentTransforms(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return transforms_;
        }

        virtual std::multimap<int, Patch2D>& GetImageObjects(bool& _outIsValid) {
            mutex_.lock();
            _outIsValid = isValidOutputData_;
            mutex_.unlock();
            return imgObjs_;
        }

        virtual int RemoveObject(int _uniqueId);

        //        virtual std::multimap<int, Shape2D >& GetShapeObjects(bool& _outIsValid) {
        //            mutex_.lock();
        //            _outIsValid = isValidOutputData_;
        //            mutex_.unlock();
        //            return shapeObjs_;
        //        }

    protected:

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
        virtual int RunProc();
    public:

        //        static std::mutex mutex_;
        //        static std::condition_variable cv_;

    private:



        virtual int IsANewObject(Shape2D& _obj);

        virtual int DataAssociation(int _uniqueId, Shape2D& _obj, int &_outObjIndex);

    private:

        //        Camera camera_;

        double lastTimestampOD_;
        double lastTimestampMT_;
        double lastTimestamp_;
        std::thread thr_;


    protected:

        Image currentFrame_;

        bool enableReInitTracking_;

        bool isValidOutputData_;

        MultiObjectTracker* pMultiTracker_;

        ObjectDetector* pObjDetector_;

        PoseEstimator* pPoseEstimator_;

        std::vector<Patch2D> refObjs_;

        map<int, Mesh> meshRefObjs_;

        std::mutex mutex_;


        bool isValidFrame_;
        multimap<int, Matrix> homographies_;

        multimap<int, Matrix> transforms_;

        multimap<int, Patch2D > imgObjs_;

        //        multimap<int, Shape2D > shapeObjs_;

        multimap<int, Matrix > transformsMT_;

        multimap<int, Patch2D > imgObjsMT_;

        //        multimap<int, Shape2D > shapeObjsMT_;

        std::multimap<int, Matrix > homographiesOD_;

        multimap<int, Patch2D > imgObjsOD_;

        multimap<int, Shape2D> shapeObjsOD_;

        multimap<int, Matrix> RTC_WsPE_;

        multimap<int, Matrix> oldRTC_WsPE_;

    };

}
#endif /* MULTIOBJECTDETECTORTRACKERANDPOSE_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CMTTracker.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:01 PM
 */

#ifndef CMTTRACKER_H
#define CMTTRACKER_H

#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <getopt.h>

#include "cv/Tracker.h"
#include "math/Patch2D.h"

#include "3rdparty/CMTTracker/CMT.h"

//#define LOG_TAG "CMTTracker"

namespace sapphire {

    class CMTTracker : public sapphire::Tracker {
    public:
        CMTTracker(/*string _configFilename = ""*/);
        virtual ~CMTTracker();

    protected:

        virtual int ResetProc();
        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);
        virtual Shape2D ComputeShape2D(int _uniqueFaceId);

        virtual int ReInitTrackerProc(Patch2D& _objPatch); //, Shape2D& _initObj, Homography& _initHomography);
        virtual int TrackProc(Image& _image);
        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);

    private:

        virtual int CheckDetection(Patch2D& _origObj, Image& _frame, Homography& _computedHomography, double _threshold);
        int CreateMask(Shape2D& _shape2D, cv::Mat &_mask, unsigned char nb);

    private:

        cmt::CMT tracker_;

        cv::Point2f roiPrev_[4];
        cv::Point2f roiCurr_[4];

        Homography homographyPrev_;

        bool enableCheckDetection_;

    };

}
#endif /* CMTTRACKER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MarkerDetector.h
 * Author: marzorati
 *
 * Created on November 15, 2017, 5:44 PM
 */

#ifndef MARKERDETECTOR_H
#define MARKERDETECTOR_H


#include <thread>
#include <mutex>


#include "math/Matrix.hpp"
#include "math/Point2D.h"
#include "image/Image.h"
#include "math/Shape2D.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "core/ProcessingNode.hpp"
#include "math/Mesh.h"

//#define LOG_TAG "MarkerDetector"

namespace sapphire
{

class MarkerDetector : public ConfigurableNode
{
public:
    MarkerDetector(){
        isInitDetector_=false;
        isValidOutputData_ = false;
    }
    virtual ~MarkerDetector(){};

    virtual int Detect(Image& _image, Image& _mask,Shape2D& _outCorners2D,int & _id)
    {
        int ret = -1;
        if (isInitDetector_)
            ret = DetectProc(_image, _mask,_outCorners2D,_id);
        else
            LOGD("MarkerDetector::Detect Not Initialized");
        return ret;
    }

    virtual int Detect(Image& _image,Shape2D& _outCorners2D,int & _id)
    {
        int ret = -1;
        if (isInitDetector_)
            ret = DetectProc(_image,_outCorners2D,_id);
        else
            LOGD("MarkerDetector::Detect Not Initialized");
        return ret;
    }

    virtual int Detect(Image& _image, Image& _mask,vector<Shape2D>& _outCorners2D,vector<int> & _ids)
    {
        int ret = -1;
        if (isInitDetector_)
            ret = DetectProc(_image, _mask,_outCorners2D,_ids);
        else
            LOGD("MarkerDetector::Detect Not Initialized");
        return ret;
    }

    virtual int Detect(Image& _image,vector<Shape2D>& _outCorners2D,vector<int> & _ids)
    {
        int ret = -1;
        if (isInitDetector_)
            ret = DetectProc(_image,_outCorners2D,_ids);
        else
            LOGD("MarkerDetector::Detect Not Initialized");
        return ret;
    }
    
    virtual int GetNumProcessedFrame()
    {
        return numProcessedFrames_;
    }

    virtual int SetNumProcessedFrame(int _numProcessedFrame)
    {
        numProcessedFrames_ = _numProcessedFrame;
        return 0;
    }

    //    virtual int Reset()
    //    {
    //        int ret = -1;
    //        isInitDetector_ = false;
    //        ret = ResetProc();
    //        return ret;
    //    };
    //
    //    virtual int AddMarkerToDetect(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform)
    //    {
    //        int ret = -1;
    //        isInitDetector_ = false;
    //        SetInitTransform(_initTransform);
    //        ret = AddMarkerToDetectProc(_objMesh, _firstObjPatch, _initTransform);
    //        if (ret == 0)
    //            isInitDetector_ = true;
    //        return ret;
    //    }
    //
    //    virtual int ReInitDetector(Patch2D& _firstObjPatch)
    //    {
    //        int ret = -1;
    //        isInitDetector_ = false;
    //        ret = ReInitDetectorProc(_firstObjPatch);
    //        return ret;
    //
    //
    //    };
    //    virtual int SetInitTransform(Matrix& _initTransform)
    //    {
    //        initTransform_ = _initTransform;
    //
    //        return 0;
    //    }
    //
    //    virtual Matrix& GetTransform(bool& _outIsValid)
    //    {
    //        mutex_.lock();
    //        _outIsValid = isValidOutputData_;
    //        mutex_.unlock();
    //        return transform_;
    //    }
    //
    //    virtual Matrix& GetInitTransform(bool& _outIsValid)
    //    {
    //        mutex_.lock();
    //        _outIsValid = isValidOutputData_;
    //        mutex_.unlock();
    //        return initTransform_;
    //    }
    //
    //    virtual Mesh& GetMarkerMesh()
    //    {
    //        return markerToDetect_;
    //    }
    //
    //    virtual Shape2D ComputeShape2D(int _uniqueFaceId) = 0;



protected:
    //    virtual int ResetProc() = 0;


    //    virtual int AddMarkerToDetectProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform) = 0;

    //    virtual int ReInitDetectorProc(Patch2D& _objPatch) = 0;

    virtual int DetectProc(Image& _image, Image& _mask,Shape2D& _outCorners2D,int & _id) = 0;
    virtual int DetectProc(Image& _image, Shape2D& _outCorners2D,int & _id) = 0;

    virtual int DetectProc(Image& _image, Image& _mask,vector<Shape2D>& _outCorners2D,vector<int> & _ids)
    {
        int ret=-1;
        ret=DetectProc(_image,_mask,_outCorners2D[0],_ids[0]);
        return ret;
    }
    virtual int DetectProc(Image& _image, vector<Shape2D>& _outCorners2D,vector<int> & _ids)
    {
        int ret=-1;
        ret=DetectProc(_image,_outCorners2D[0],_ids[0]);
        return ret;
    };


protected:

    bool isInitDetector_;

    std::mutex mutex_;

    //    Patch2D firstObjPatch_;

    //    Mesh markerToDetect_;
    //    Camera camera_;


    bool isValidOutputData_;

    int numProcessedFrames_;
    int maxNumOnlyDetectedProcessedFrame_;
    int maxProjectionError_;

    Matrix initTransform_, transform_;

};

}

#endif /* MARKERDETECTOR_H */


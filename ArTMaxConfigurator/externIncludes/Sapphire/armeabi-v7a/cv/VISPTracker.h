/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VISPTracker.h
 * Author: marzorati
 *
 * Created on June 16, 2017, 12:01 PM
 */


#ifdef USE_VISP

#ifndef VISPTRACKER_H
#define VISPTRACKER_H

#include <opencv2/opencv.hpp>
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/vision/vpKeyPoint.h>
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/gui/vpDisplayOpenCV.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/io/vpImageIo.h>
#include <visp3/core/vpIoTools.h>
#include <visp3/mbt/vpMbEdgeTracker.h>
#include <visp3/mbt/vpMbEdgeKltTracker.h>
#include <visp3/io/vpVideoReader.h>
#include <visp3/tt/vpTemplateTrackerSSDInverseCompositional.h>
#include <visp3/tt/vpTemplateTrackerWarpHomography.h>
#include <visp3/tt_mi/vpTemplateTrackerMIInverseCompositional.h>
#include <visp3/core/vpTrackingException.h>
#include <visp3/core/vpPixelMeterConversion.h>
#include <visp3/vision/vpPose.h>

#include "cv/Tracker.h"
#include "math/Patch2D.h"

namespace sapphire {

    class VISPTracker : public Tracker {
    public:
        VISPTracker(/*string _configFilename = ""*/);
        virtual ~VISPTracker();



        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame); //, std::vector<Point2D<float> >& _imagePoints);
        //        virtual int AddObjectToTrack(Shape2D& _object, Image& _frame, Shape2D& _initObject, Homography& _initHomography); //, std::vector<Point2D<float> >& _imagePoints);

    protected:

        virtual int ResetProc();

        virtual int AddObjectToTrackProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix& _initTransform);


        //TODO

        //        virtual int AddObjectToTrack(Mesh& _objMesh) {
        //            return -1;
        //        };
        //
        //        virtual int InitTracker(Patch2D& _objPatch) {
        //            return -1;
        //        };





        virtual int ReInitTrackerProc(Patch2D& _objPatch); //, Shape2D& _initObj, Homography& _initHomography);

        virtual int TrackProc(Image& _image);

        virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);
        virtual Shape2D ComputeShape2D(int _uniqueFaceId);



    private:
        virtual int CheckDetection(Patch2D& _origObj, Image& _frame, Homography& _computedHomography, double _threshold);


    private:

        vpImage<unsigned char> frameContainer_;

        vpTemplateTrackerWarpHomography warp_;
        //        vpTemplateTrackerMIInverseCompositional tracker_;
        vpTemplateTrackerSSDInverseCompositional tracker_;

        vector<vpImagePoint> imagePoints_;


        bool enableCheckDetection_;



    };

}
#endif /* VISPTRACKER_H */

#endif


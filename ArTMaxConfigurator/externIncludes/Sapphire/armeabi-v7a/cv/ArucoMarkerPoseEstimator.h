/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArucoMarkerPoseEstimator.h
 * Author: marzorati
 *
 * Created on November 15, 2017, 6:17 PM
 */

#ifndef ARUCOMARKERPOSEESTIMATOR_H
#define ARUCOMARKERPOSEESTIMATOR_H


#include <mutex>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "../3rdparty/aruco2/aruco.hpp"

#include "image/Image.h"
#include "math/Point2D.h"
#include "math/Homography.h"
#include "math/Patch2D.h"
#include "core/ConfigurableNode.hpp"
#include "math/Mesh.h"
#include "cv/Camera.h"

#include "cv/MarkerPoseEstimator.h"

namespace sapphire
{

class ArucoMarkerPoseEstimator : public MarkerPoseEstimator
{
public:
    ArucoMarkerPoseEstimator();
    virtual ~ArucoMarkerPoseEstimator();

//    virtual int InitPose(std::vector<Point2D<float> >& _imagePoints, std::vector<Point3D<float> >& _worldPoints) = 0;


protected:

//    virtual int ResetProc();
    virtual int ComputePoseProc(Shape2D& _corners2D,Matrix& _rotationOut, Matrix& _translationOut);

    virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString);


    
//    virtual int AddMarkerToDetectProc(Mesh& _objMesh, Patch2D& _firstObjPatch, Matrix & _initTransform);
//
//    virtual int ReInitDetectorProc(Patch2D& _objPatch);



private:

//    Ptr<cv::aruco::Dictionary> dictionary_;
//    Ptr<cv::aruco::DetectorParameters> detectorParams_ ;
    float markerLength_;
    

};

}
#endif /* ARUCOMARKERPOSEESTIMATOR_H */


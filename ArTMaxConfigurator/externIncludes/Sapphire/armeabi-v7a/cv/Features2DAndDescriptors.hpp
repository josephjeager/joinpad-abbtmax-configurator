/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Features2DAndDescriptors.hpp
 * Author: marzorati
 *
 * Created on September 7, 2017, 4:33 PM
 */



#ifndef FEATURES2DANDDESCRIPTORS_HPP
#define FEATURES2DANDDESCRIPTORS_HPP


#include <thread>
#include <mutex>


#include "cv/Detector.h"
#include "cv/Matcher.h"
#include "math/Matrix.hpp"
#include "cv/Feature2D.h"

namespace sapphire
{

    class Features2DAndDescriptors {
    public:

        Features2DAndDescriptors()
        {

        }

        virtual ~Features2DAndDescriptors()
        {
//            LOGD("Features2DAndDescriptors::Destroy");
//            LOGD("Features2DAndDescriptors::destroy descriptors_: %d %d",descriptors_.rows,descriptors_.cols);

        }
        int SetFeatures2DAndDescriptors(vector<Feature2D>& _features2D, Matrix& _descriptors) {

            features2D_ = _features2D;
            descriptors_ = _descriptors.clone();
//            LOGD("Features2DAndDescriptors::descriptors_: %d %d",descriptors_.rows,descriptors_.cols);
//            LOGD("Features2DAndDescriptors::features2D_: %d",features2D_.size());

            return 0;
        }
    public:
        vector<Feature2D> features2D_;
        Matrix descriptors_;
    };
}


#endif /* FEATURES2DANDDESCRIPTORS_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Feature2d.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 9:52 AM
 */

#ifndef FEATURE2D_H
#define FEATURE2D_H

#include "math/Point2D.h"
#include "math/Matrix.hpp"

namespace sapphire
{

class Feature2D
{
public:

    Feature2D(float _x, float _y, float _size, float _angle = -1, float _response = 0, int _octave = 0,int _classID=-1,float _ePointInOctaveX=-1,float _ePointInOctaveY=-1,float _endPointX=-1,float _endPointY=-1,float _lineLength=-1,int _numOfPixels=-1,float  _sPointInOctaveX=-1,float _sPointInOctaveY=-1,float _startPointX=-1,float _startPointY=-1)    {
        point2D_.x_ = _x;
        point2D_.y_ = _y;
        angle_ = _angle;
        size_ = _size;
        response_ = _response;
        octave_ = _octave;
        classID_=_classID;
         ePointInOctaveX_=_ePointInOctaveX;
            ePointInOctaveY_=_ePointInOctaveY;
            endPointX_=_endPointX;
            endPointY_=_endPointY;
            lineLength_=_lineLength;
            numOfPixels_=_numOfPixels;
            sPointInOctaveX_=_sPointInOctaveX;
            sPointInOctaveY_=_sPointInOctaveY;
            startPointX_=_startPointX;
            startPointY_=_startPointY;
    }

    virtual ~Feature2D()
    {
//        LOGD("Feature2D::Destroy");
//        LOGD("Feature2D::descriptor_: %d %d",descriptor_.rows,descriptor_.cols);

    };

    Matrix& GetDescriptor()
    {
        return descriptor_;
    };

    Point2D<float>& GetPoint2D()
    {
        return point2D_;
    };

    int SetPoint2D(Point2D<float>& _point2D)
    {
        point2D_ = _point2D;
        return 0;
    }

    int SetDescriptor(Matrix& _descriptor)
    {
        descriptor_ = _descriptor.clone();
        return 0;
    }

public:
    float angle_, response_, octave_, size_;
    int classID_;
    float ePointInOctaveX_,ePointInOctaveY_,endPointX_,endPointY_, lineLength_;
    int numOfPixels_;
    float sPointInOctaveX_;
    float sPointInOctaveY_;
    float startPointX_;
    float startPointY_;
private:


    Point2D<float> point2D_;


    Matrix descriptor_;

};
}

#endif /* FEATURE2D_H */


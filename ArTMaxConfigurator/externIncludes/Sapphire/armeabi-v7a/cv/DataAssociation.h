/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataAssociation.h
 * Author: marzorati
 *
 * Created on September 12, 2017, 1:20 PM
 */

#ifndef DATAASSOCIATION_H
#define DATAASSOCIATION_H

#include "math/Matrix.hpp"
#include "math/Shape2D.h"

namespace sapphire {

    class DataAssociation {
    public:
        DataAssociation();
        virtual ~DataAssociation();

        static int Compute(Shape2D& _refShapeObj, vector<Shape2D>& _shapeObjsToAssociate,int &_outShapeIndex);

    private:

    };
}
#endif /* DATAASSOCIATION_H */


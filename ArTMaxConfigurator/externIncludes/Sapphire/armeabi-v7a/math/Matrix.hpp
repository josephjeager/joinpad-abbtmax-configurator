/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Matrix.h
 * Author: marzorati
 *
 * Created on May 29, 2017, 10:43 AM
 */

#ifndef MATRIX_H
#define MATRIX_H


#include <opencv2/opencv.hpp>

#include "utils/Utils.h"
#include "core/Validable.h"

namespace sapphire {

    class Matrix : public cv::Mat, public Validable {
    public:

        Matrix(cv::Mat _matrixOCV) {
            _matrixOCV.copyTo(*this);
        }

        Matrix(cv::Mat& _matrixOCV) {
            _matrixOCV.copyTo(*this);
        }
        //        
        //        Matrix(const cv::Mat& _matrixOCV) {
        //            _matrixOCV.copyTo(*this);
        //        }

        
        Matrix(const Matrix& _matrixOCV) {
            _matrixOCV.copyTo(*this);
        }


        Matrix& operator=(cv::Mat& _matrixOCV) {
            _matrixOCV.copyTo(*this);
            return *this;
        }

        Matrix& operator=(const Matrix& _matrixOCV) {
            _matrixOCV.copyTo(*this);
            return *this;
        }

        Matrix(int _num_rows = 1, int _num_cols = 1, int _data_type = CV_64F) {
            //            matrix_.create(_num_rows, _num_cols, _data_type);
            create(_num_rows, _num_cols, _data_type);
        }

        //        Matrix(const Matrix& _matrix) {
        //            
        //         }

        virtual ~Matrix() {
        };

        int GetCols() {
            return cols;
        }

        int GetRows() {
            return rows;
        }

        cv::Mat& GetMatrixData() {
            return *this;
        }

        //        int SetMatrixData(cv::Mat& _matrixOCV)
        //        {
        //            *this=_matrixOCV;
        //            return 0;
        //        }

    public:

        //        cv::Mat matrix_;
    };

}

#endif /* MATRIX_H */


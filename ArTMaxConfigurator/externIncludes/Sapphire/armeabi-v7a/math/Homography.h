/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Homography.h
 * Author: marzorati
 *
 * Created on June 8, 2017, 4:03 PM
 */

#ifndef HOMOGRAPHY_H
#define HOMOGRAPHY_H

#include <opencv2/opencv.hpp>
#include <vector>

#include "math/Point2D.h"
#include "math/Matrix.hpp"
#include "utils/Utils.h"
#include "core/Validable.h"

using namespace std;

namespace sapphire {

    class Homography: public Validable {
    public:
        Homography();

        virtual ~Homography();


        virtual bool IsGood();
        virtual int Estimate(vector<Point2D<float> >& _srcPoints, vector<Point2D<float> >& _dstPoints, int _method = 0, double _ransacReprojThreshold = 3, int _maxIters = 2000, double _confidence = 0.995);

        virtual int Project(vector<Point2D<float> >& _pointsToProject, vector<Point2D<float> >& _projectedPoints);


        virtual int SetIdentity()
        {
            H_=(cv::Mat)H_.eye(3,3,CV_64F);
            return 0;
        }

        virtual Matrix& GetH() {
            return H_;
        }

        virtual int SetH(Matrix& _H) {
            int ret =0;
            if (_H.GetRows() == 3 && _H.GetCols() == 3)
                H_ = _H;
            else
                ret = -1;
            return ret;
        }

    private:

        Matrix H_;
    };

}
#endif /* HOMOGRAPHY_H */


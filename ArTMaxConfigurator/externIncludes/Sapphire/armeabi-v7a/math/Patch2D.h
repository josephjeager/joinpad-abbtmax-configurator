/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Patch.h
 * Author: marzorati
 *
 * Created on July 3, 2017, 11:33 AM
 */

#ifndef PATCH2D_H
#define PATCH2D_H

#include <chrono>

#include "image/Image.h"
#include "math/Shape2D.h"
//#include "math/Shape3D.h"
//#include "math/GeomFace.h"

namespace sapphire
{

    class Patch2D//:public Shape2D 
    {
    public:

        //        Patch2D(int id);//,GeomFace& _face3D);
        //        Patch2D(int _objId,GeomFace& _face3D,std::vector<Feature2D>& _features);
        //        Patch2D(int id,Image& _image);//,GeomFace& _face3D);
        //        Patch2D(int _objId,Image& _image,GeomFace& _face3D,std::vector<Feature2D>& _features);


        //        Patch2D(int _objId,double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        Patch2D(int _id=-1,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >());//,double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        //        Patch2D(int _objId,std::vector<Feature2D>& _features,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >(),double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        Patch2D(int _id,Image& _image,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >());//,double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        //        Patch2D(int _objId,Image& _image,std::vector<Feature2D>& _features,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >(),double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        //        Patch2D(int _objId,GeomFace& _face3D,Image& _image,std::vector<Feature2D>& _features,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >(),double _imageObjCenterX=0,double _imageObjCenterY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        //        Patch(int _objId,Image& _image,std::vector<Point2D<float> >  _shape=std::vector<Point2D<float> >(),double _imageObjCenterX=0,double _imageObjCenterY=0,double _dimX=0,double _dimY=0,vector<Point3D<float> > _shape3D=std::vector<Point3D<float> >());
        virtual ~Patch2D();

        //        virtual int SetFace(GeomFace& _face3D);
        //        virtual int SetObjId(int _objId);
        virtual int SetShape2D(vector<Point2D<float> >& _shape2D);
        virtual int SetImage(Image& _image);

        virtual int SetId(int _id)
        {
            id_=_id;
            return 0;
        }

        virtual int GetId()
        {
            return id_;
        }
        //        virtual int SetFeatures2D(std::vector<Feature2D>& _features);
        //        virtual int SetShape3D(vector<Point3D<float> >& _shape3D);

        //        virtual int SetTimestamp(long _timestamp);

    public:

        Image image_;
        //        Shape3D shape3D_;
        int id_;
        Shape2D shape2D_;
        //        GeomFace face3D_;

        //        int objId_;
        //        long timestamp_;
        //        std::vector<Feature2D> features_;
    };
}
#endif /* PATCH2D_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Mesh.h
 * Author: marzorati
 *
 * Created on August 3, 2017, 9:39 AM
 */

#ifndef MESH_H
#define MESH_H

#include "image/Image.h"
#include "math/Shape3D.h"
#include "math/GeomFace.h"

//#define LOG_TAG "Mesh"

#define UNIQUEID_MESH_MASK  1000
#define UNIQUEID_FACE_MASK  10
#define UNIQUEID_INSTANCE_MASK  1

namespace sapphire {

    class Mesh {
    public:
        Mesh(int _objId = -1, Image _texture = Image(), std::vector<Point3D<float> > _vertexes3D = std::vector<Point3D<float> >());

        virtual ~Mesh();


        virtual int LoadFromFile(string & _filename);

        virtual int AddFace(std::vector<int>& _vertexIndexes, Shape2D& _texCoords) {
            int ret = 0;
//            LOGD("Mesh:: Adding face");
            int uniqueFaceId = uniqueObjId_ + (faces3D_.size() + 1) * UNIQUEID_FACE_MASK;
            GeomFace face(uniqueFaceId); //objId_ + (faces3D_.size() + 1) * UNIQUEID_FACE_MASK/*,_vertexIndexes*/); //

            faces3DVertexIndexes_.push_back(_vertexIndexes);
            faces3D_.push_back(face);
            facesTexCoords_.push_back(_texCoords);

            LOGD("Mesh:: Rebuild Mesh");
            ReBuild();
            return ret;
        }

        virtual int SetObjId(int _objId) {
            int ret = 0;
            uniqueObjId_ = _objId*UNIQUEID_MESH_MASK;
            return ret;
        }

        virtual int SetTexture(Image& _texture) {
            int ret = 0;
            texture_ = _texture;
            ReBuild();
            return ret;
        }

        virtual int SetVertexes(std::vector<Point3D<float> >& _vertexes3D) {
            int ret = 0;
            vertexes3D_ = _vertexes3D;
            ReBuild();
            return ret;
        }

        virtual vector< vector<int> > GetVertexIndexes() {
            return faces3DVertexIndexes_;
        }

        virtual Shape2D GetShape2DByUniqueId(int _uniqueId) {

            int faceId = GetFaceId(_uniqueId); //(int) ((_uniqueId - (int) (_uniqueId / UNIQUEID_MESH_MASK) * UNIQUEID_MESH_MASK) / UNIQUEID_FACE_MASK);
            Shape2D shape(faces3D_[faceId - 1].GetRelativeTexCoords());
            return shape;
        }

        virtual int GetUniqueObjId() {
            return uniqueObjId_; //*UNIQUEID_MESH_MASK;
        }

        virtual int GetUniqueFaceId(int _faceId) {
            return uniqueObjId_/**UNIQUEID_MESH_MASK*/ + _faceId*UNIQUEID_FACE_MASK;
        }

        virtual bool CheckUniqueFaceId(int _uniqueId) {
            bool found = false;
            //        int objId=((int)(_uniqueId/UNIQUEID_MESH_MASK))*UNIQUEID_MESH_MASK;
            int faceId = _uniqueId; //-((int)(_uniqueId/UNIQUEID_MESH_MASK))*UNIQUEID_MESH_MASK;
            //        if (objId_==objId)
            //        {
            int i = 0;
            while (i < faces3D_.size() && !found) {
                if (faces3D_[i].GetUniqueFaceId() == faceId)
                    found = true;
                ++i;
            }
            //        }
            return found;
        }

        virtual vector<GeomFace> GetFaces() {
            return faces3D_;
        }

        virtual bool GetFaceByUniqueFaceId(int _uniqueFaceId, GeomFace& _faceOut) {
            int i = 0;
            bool found = false;
            while (i < faces3D_.size() && !found) {
                if (faces3D_[i].GetUniqueFaceId() == _uniqueFaceId) {
                    found = true;
                    _faceOut = faces3D_[i];
                }
                ++i;
            }
            return found;
        }

        virtual bool GetFaceByFaceId(int _faceId, GeomFace& _faceOut) {
            int i = 0;
            bool found = false;
            while (i < faces3D_.size() && !found) {
                if (GetFaceId(faces3D_[i].GetUniqueFaceId()) == _faceId) {
                    found = true;
                    _faceOut = faces3D_[i];
                }
                ++i;
            }
            return found;
        }

        virtual Image GetTexture() {
            return texture_;
        }

        virtual vector< Point3D<float> > GetVertexes3D() {
            return vertexes3D_;
        }

        static int GetUniqueId(int _objId, int _faceId) {
            return _objId * UNIQUEID_MESH_MASK + _faceId*UNIQUEID_FACE_MASK;
        }

        static int GetFaceId(int _uniqueFaceId) {
            int objId = (int) (_uniqueFaceId / UNIQUEID_MESH_MASK);

            int face = (int) ((_uniqueFaceId - objId * UNIQUEID_MESH_MASK) / UNIQUEID_FACE_MASK);

            return face;
        }

        static int GetObjId(int _uniqueId) {
            return (int) (_uniqueId / UNIQUEID_MESH_MASK);
        }

        //    virtual int GetUniqueId(int _numFace)
        //    {
        //        return faces3D_[_numFace].GetFaceId();
        //    }

    protected:

        virtual int ReBuild();

    private:
        std::vector<GeomFace> faces3D_;
        std::vector< vector<int> > faces3DVertexIndexes_;
        std::vector<Point3D<float> > vertexes3D_;
        std::vector< Shape2D > facesTexCoords_;
        Image texture_;
        int uniqueObjId_;
    };
}
#endif /* MESH_H */


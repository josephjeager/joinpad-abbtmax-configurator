/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CameraMotionTracking.h
 * Author: marzorati
 *
 * Created on February 23, 2018, 10:12 AM
 */

#ifndef CAMERAMOTIONTRACKING_H
#define CAMERAMOTIONTRACKING_H

#include <cv/MarkerRecognizer.h>
#include "cv/CameraMotionEstimator.h"
#include "CameraMotionTrackingConfig.h"

#define LOG_TAG "CameraMotionTracking"




#define MAX_MARKER_LIVES    20

#define FROM_FILE 0
#define FROM_STRING 1

using namespace sapphire;


class Marker
{
public:

    int detectedMarkerID_=-1;
    int markerLives_=-1;
    double timestamp_=-1;
    int numDetections_=0;

    double dimX_=0;
    double dimY_=0;
};


class CameraMotionTracking
{
public:
    CameraMotionTracking(string _configString,int _loadType);
    virtual ~CameraMotionTracking();

    int Init();
    
    int Configure();

    int Start();
    
    int Stop();

    int ProcessFrame(Image& _frame);
    
    int ResetHomography(Matrix & _initHomography);
    
    int GetCameraMotionHomographies(multimap<int,Matrix >& _homographiesOut);
    int GetHomography(Matrix& _homography);

    int GetMarker(Marker& _marker,Matrix& _markerHomography,Shape2D& _markerShape2D,double &_timestamp);

    int GetImage(Image& _imageOut);

    int SetImage(Image& _image);

    
    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV=0,double _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

    
private:

    int DetectMarkers(Image & _image,Marker& _markerOut,Matrix & _markerHomographyOut);

    std::string BuildDefaultConfigString();
    
private:
    
    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;

    vector<Image> frames_;

    Matrix mEye3x3;

    Marker detectedMarker_;
    Matrix markerHomography_;
    Matrix initHomography_;
    Shape2D markerShape2D_;

    Image imgScene_, imgScene_tmp_,imgSceneOut_;
    CameraMotionEstimator cameraMotionEstimator_;
    MarkerRecognizer markerRecognizer_;
   
    int numFrame_;
    
    string fileNameScene_;
    
    bool isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;
    bool loadType_;

};

#endif /* CAMERAMOTIONTRACKING_H */


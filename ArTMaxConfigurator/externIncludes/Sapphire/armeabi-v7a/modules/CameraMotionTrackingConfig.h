//
// Created by marzorati on 1/19/18.
//

#ifndef CAMERAMOTIONTRACKINGCONFIG_H
#define CAMERAMOTIONTRACKINGCONFIG_H

#define DEFAULT_CONFIG_STRING  "%YAML:1.0\n"\
                                "---\n"\
                                "cameramotiontracking:\n"\
                                "   cameraMotionEstimatorConfigFilename: \"cameramotionestimator\"\n"\
                                "   markerRecognizerConfigFilename: \"markerrecognizer\"\n"\
                                "cameramotionestimator:\n"\
                                "   featureDetectorType: 1\n"\
                                "   featureDetectorConfigFilename: \"ocvfeaturedetector\"\n"\
                                "   featureDescriptorType: 1\n"\
                                "   featureDescriptorConfigFilename: \"ocvfeaturedescriptor\"\n"\
                                "   featureMatcherType: 1\n"\
                                "   featureMatcherConfigFilename: \"ocvflannfeaturematcher\"\n"\
                                "   maxCount: 500\n"\
                                "   qualityLevel: 0.01\n"\
                                "   minDistance: 50\n"\
                                "   harrisK: 0.04\n"\
                                "   blockSize: 3\n"\
                                "   useHarrisDetector: 0\n"\
                                "   subPixWinSize: 3\n"\
                                "   pyrMaxLevel: 3\n"\
                                "   minEigThreshold: 0.0001\n"\
                                "   winSize: 11\n"\
                                "   enableRobustTracking: 0\n"\
                                "   recomputeNewPointsForEachLoop: 1\n"\
                                "ocvfeaturedescriptor:\n"\
                                "   descriptorType: \"BRISK\"\n"\
                                "   thresh: 30\n"\
                                "   octaves: 3\n"\
                                "   patternScale: 1.0\n"\
                                "ocvfeaturedetector:\n"\
                                "   detectorType: \"BRISK\"\n"\
                                "   thresh: 30\n"\
                                "   octaves: 3\n"\
                                "   patternScale: 1.0\n"\
                                "ocvflannfeaturematcher:\n"\
                                "   tableNumber: 10\n"\
                                "   keySize: 10\n"\
                                "   multiProbeLevel: 2\n"\
                                "   checks: 50\n"\
                                "   eps: 0\n"\
                                "   sorted: 1\n"\
                                "   numQueryMatches: 2\n"\
                                "markerrecognizer:\n"\
                                "   markerDetectorType: 1\n"\
                                "   markerDetectorConfigFilename: \"arucomarkerdetector\"\n"\
                                "   markerPoseEstimatorType: 1\n"\
                                "   markerPoseEstimatorConfigFilename: \"arucomarkerposeestimator\"\n"\                         
                                "arucomarkerdetector:\n"\
                                "   nmarkers: 1024\n"\
                                "   adaptiveThreshWinSizeMin: 3\n"\
                                "   adaptiveThreshWinSizeMax: 23\n"\
                                "   adaptiveThreshWinSizeStep: 10\n"\
                                "   adaptiveThreshWinSize: 21\n"\
                                "   adaptiveThreshConstant: 7\n"\
                                "   minMarkerPerimeterRate: 0.03\n"\
                                "   maxMarkerPerimeterRate: 4.0\n"\
                                "   polygonalApproxAccuracyRate: 0.05\n"\
                                "   minCornerDistance: 10.0\n"\
                                "   minDistanceToBorder: 3\n"\
                                "   minMarkerDistance: 10.0\n"\
                                "   minMarkerDistanceRate: 0.05\n"\
                                "   cornerRefinementWinSize: 5\n"\
                                "   cornerRefinementMaxIterations: 30\n"\
                                "   cornerRefinementMinAccuracy: 0.1\n"\
                                "   markerBorderBits: 1\n"\
                                "   perspectiveRemovePixelPerCell: 8\n"\
                                "   perspectiveRemoveIgnoredMarginPerCell: 0.13\n"\
                                "   maxErroneousBitsInBorderRate: 0.04\n"\
                                "   minOtsuStdDev: 5.0\n"\
                                "   errorCorrectionRate: 0.6\n"\
                                "   dictionary: 8\n"\
                                "   cornerRefinementType: 1\n"\
                                "arucomarkerposeestimator:\n"\
                                "   markerLength: 0.05"\
                                
#endif //CAMERAMOTIONTRACKINGCONFIG_H

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Obj2DLocalization.h
 * Author: marzorati
 *
 * Created on June 21, 2017, 3:48 PM
 */

#ifndef OBJ2DLOCALIZATION_H
#define OBJ2DLOCALIZATION_H

#define _USE_MATH_DEFINES

#include <vector>
#include <iostream>
#include <iomanip>
#include <math.h>

//#include <GL/glut.h>
//#include <GLES2/gl2.h>



#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"


#include "cv/Detector.h"
#include "cv/Feature2D.h"
#include "cv/FMatch.h"
#include "cv/OCVFeatureDetector.h"
#include "cv/OCVFeatureDescriptor.h"
#include "cv/OCVFlannFeatureMatcher.h"
#include "math/Homography.h"
#include "core/Time.h"
#include "core/Thread.h"
#include "cv/Matcher.h"
#include "cv/OCVBFFeatureMatcher.h"
#include "cv/ObjectDetector.h"
#include "cv/VISPTracker3D.h"
#include "cv/VISPPoseEstimator.h"
//#include "cv/MultiObject3DDetectorAndTracker3D.h"
#include "cv/MultiObjectDetectorTrackerAndPose.h"
#include "cv/P3PPoseEstimator.h"
#include "core/Info.h"
#include "utils/DrawingTools.h"

#include "Obj2DLocalizationConfig.h"

#define LOG_TAG "Obj2DLocalization"

//#define _DRAW

//#ifdef _DEBUG
#ifdef _ANDROID
//#include <android/log.h>
//#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
//#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
//#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
#endif

#define FROM_FILE 0
#define FROM_STRING 1


using namespace sapphire;

class Obj2DLocalization {
public:
    Obj2DLocalization(string _configString,int _loadType);
//    Obj2DLocalizationModule(const char* _pConfigParamsJSON);

    virtual ~Obj2DLocalization();
    
    int Init();
    int EvaluateReferenceObjectQuality(Image& _imgObj,int _numHBuckets,int _numVBuckets,vector<Feature2D> & _keypointsOut,vector<int>& _numValuesOut,vector<Point2D<float> >& _meanValuesOut,vector<Point2D<float> > & _varValuesOut);

    int AddNewObjectToDetect(int _objID,std::vector<Point3D<float> > & _shape3D,std::vector< std::vector<int> >& _facesIdxs,std::vector< std::vector<Point2D<float> > >& _shapes2D,Image& _imgObj,string _objName);

    int Configure();
    std::string BuildDefaultConfigString();

    int ProcessFrame(Image& _frame);
    
    int GetDetectedObjects(multimap<int,Shape2D >& _detectedObjsOut);

    //int GetDetectedMeshObjects(map<int,vector<Mesh> >& _detectedObjsOut);

    int GetHomographies(multimap<int,Matrix >& _homographiesOut);
    int GetRTMatrixes(multimap<int, Matrix>& _rtMatrixesOut);

//    int GetBoundingBoxes(multimap<int,  std::vector<Point2D<float> > > & _boundingBoxesOut);
    
    int GetDetectedObjects(multimap<int, Patch2D>& _detectedObjsOut);
    int GetReferenceObjects(vector<Patch2D>& _refObjsOut);
    int GetMeshReferenceObjects(map<int,Mesh>& _refMeshObjsOut);

    int Start();
    int Stop();

    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV=0,double _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

    //DEPRECATED
    int GetImage(Image& _imageOut);
    //DEPRECATED
    int SetImage(Image& _image);

    
private:
    
    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;

//    Camera camera_;
    MultiObjectDetectorTrackerAndPose multiObjDetectorTrackerAndPose_;

    vector<Image> frames_;

    Image imgScene_, imgScene_tmp_,imgSceneOut_;
    ObjectDetector objDetector_;
    MultiObjectTracker multiTracker_;
    P3PPoseEstimator poseEstimator_;
    
    std::vector < std::vector<Feature2D> > trainKeyPointsVec_;
    std::vector< Matrix > descriptors_objectVec_;
   
//    OCVFeatureDetector detectorObj_;
//    OCVFeatureDescriptor descriptorObj_;
//
//    Detector detector_;
//
//    OCVFlannFeatureMatcher keypoint_matcher_;
//
//    Matcher matcher_;

    int numFrame_;
    
//    Camera camera_;

    //cv::VideoCapture video_;
    string fileNameScene_;
    
    std::vector<Patch2D> refObjs_;
    std::map<int,Mesh> meshRefObjs_;
    
    std::map<int,Info> refObjName_;

    bool isTracking_,isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;
    bool loadType_;

    //OLD
//    Image imgScene_, imgScene_tmp_,imgSceneOut_;
//    ObjectDetector objDetector_;
//    
//    std::vector < std::vector<Feature2D> > trainKeyPointsVec_;
//    std::vector< Matrix > descriptors_objectVec_;
//    vector<Image> imgObjsVec_;
//    
//    OCVFeatureDetector detectorObj_;
//    OCVFeatureDescriptor descriptorObj_;
//
//    Detector detector_;
//    
//    OCVFlannFeatureMatcher keypoint_matcher_;
//
//    Matcher matcher_;
//    
// Camera camera_;
//    ObjectTracker objTracker_;
//    VISPTracker tracker_;
//    VISPPoseEstimator poseEstimator_;
//    
// //   cv::VideoCapture video_;
//    string fileNameScene_;
//    
//    bool isTracking_;
//    bool isInit_,isConfigurated_,isStarted_;

};

#endif /* OBJ2DLOCALIZATION_H */


//
// Created by marzorati on 1/24/18.
//

#ifndef MULTITRACKING2D_H
#define MULTITRACKING2D_H

//
//#include "rapidxml.hpp"
//#include "rapidxml_utils.hpp"
//
//#include "abstract_tracker.h"
//
//#include "3rdparty/CMTBrainTracker/CMT.h"
//#include "cmt_tracker.hpp"
//#include "kcf_tracker.hpp"


#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core/core.hpp"


#include "math/Homography.h"



#include "cv/MultiObjectTracker.h"

#include "DefaultMultiTracking2DConfig.h"

//#ifdef _DEBUG
#ifdef _ANDROID
#include <android/log.h>
#define LOG_TAG "MultiTracking2D"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
//#endif



using namespace sapphire;

class MultiTracking2D
{
public:
    MultiTracking2D(string _configString,int _loadType);

    virtual ~MultiTracking2D();

    int Init();
    int Configure();
    std::string BuildDefaultConfigString();


    int ProcessFrame(Image& _frame);

    virtual int AddNewObjectToTrack(Image& image,int _id,int _left,int _top,int _right,int _bottom);

    virtual int RemoveTrackedObject(int _id);

    int GetDetectedObjects(multimap<int,Shape2D >& _detectedObjsOut);


    int GetHomographies(multimap<int,Matrix >& _homographiesOut);

//    int GetDetectedObjects(multimap<int, Patch2D>& _detectedObjsOut);
//    int GetReferenceObjects(vector<Patch2D>& _refObjsOut);
//    int GetMeshReferenceObjects(map<int,Mesh>& _refMeshObjsOut);

    int Start();
    int Stop();

    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,int _hFOV=0,int _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

    int SetMaxNumTrackerLives(int _maxNumTrackerLives);
    int GetNumTrackerLives(int _trackerId);

    int GetImage(Image& _imageOut);

    int SetImage(Image& _image);

private:

    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;

//    Camera camera_;
    MultiObjectTracker multiTracker_;

//    vector<Image> frames_;

    Image imgScene_, imgScene_tmp_,imgSceneOut_;

//    std::vector < std::vector<Feature2D> > trainKeyPointsVec_;
//    std::vector< Matrix > descriptors_objectVec_;

//    int numFrame_;

    string fileNameScene_;

//    std::vector<Patch2D> refObjs_;
    std::map<int,Mesh> meshRefObjs_;

//    std::map<int,Info> refObjName_;

    bool isTracking_,isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;
    bool loadType_;

};


#endif //MULTITRACKING2D_H

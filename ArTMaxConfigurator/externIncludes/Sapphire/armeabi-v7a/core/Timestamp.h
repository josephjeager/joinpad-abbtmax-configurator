/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Timestamp.h
 * Author: marzorati
 *
 * Created on July 11, 2017, 11:49 AM
 */

#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <chrono>

namespace sapphire
{
    class Timestamp {
    public:
    Timestamp(bool _setCurrentTime=false);
        Timestamp(long _timestamp);
        virtual ~Timestamp();

        virtual long GetNow();

        virtual int Now();

    virtual long GetTimestamp()
    {
            return timestamp_;
        }

        virtual int SetTimestamp(long _timestamp);
    private:

        long timestamp_;

    };
}
#endif /* TIMESTAMP_H */


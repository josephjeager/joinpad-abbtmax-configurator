/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Thread.h
 * Author: marzorati
 *
 * Created on June 9, 2017, 11:30 AM
 */

#ifndef THREAD_H
#define THREAD_H

#include <thread>
#include <mutex>
#include <functional>

namespace sapphire
{


    class Thread {
    public:
        Thread();
        virtual ~Thread();

        template <class Type>
    int createThread(Type &_obj, void(Type::*_func)(void))
    {
        threadHnd_=new std::thread(std::bind(_func, _obj));
            return 0;
        }

    virtual int join()
    {
            threadHnd_->join();
            return 0;
        }

    private:

        std::thread* threadHnd_;

        std::mutex mutex_;

    };

}

#endif /* THREAD_H */


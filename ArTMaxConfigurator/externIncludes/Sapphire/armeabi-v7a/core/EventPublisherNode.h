/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EventPublisherNode.h
 * Author: marzorati
 *
 * Created on July 7, 2017, 11:14 AM
 */

#ifndef EVENTPUBLISHERNODE_H
#define EVENTPUBLISHERNODE_H

#include "core/EventSubscriberNode.h"
#include "core/EventType.h"


class EventPublisherNode
{
public:
    // The prefix "cbi" is to prevent naming clashes.
    virtual int Register(EventSubscriberNode* _pEventSubscriberNode,EventType _eventType)
    {
        int ret=0;
        eventSubscriberNodes_[_eventType].push_back(_pEventSubscriberNode);
        return ret;
    }

    virtual int SendEvent(EventType _eventType)
    {
        for (int i=0;i<eventSubscriberNodes_[_eventType].size();++i)
            eventSubscriberNodes_[_eventType][i]->Notify(_eventType);
        return 0;
    }

protected:

    map<EventType,vector<EventSubscriberNode*> > eventSubscriberNodes_;
};

#endif /* EVENTPUBLISHERNODE_H */


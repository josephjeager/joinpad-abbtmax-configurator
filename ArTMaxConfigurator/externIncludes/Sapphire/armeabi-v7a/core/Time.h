/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Time.h
 * Author: marzorati
 *
 * Created on June 9, 2017, 11:42 AM
 */

#ifndef TIME_H
#define TIME_H

#include <chrono>

namespace sapphire {

    class Time {
    public:
        Time();
        virtual ~Time();
        static int Start();
        static int Start(double& _startTime);
        static int Stop();
        static int Stop(double& _stopTime);

        static int GetTimestamp(double& timestamp_);
        static double GetElapsedTime();

    private:

#ifdef _ANDROID
        static std::chrono::system_clock::time_point startTime_, stopTime_;
#endif
#ifdef _LINUX
        static std::chrono::system_clock::time_point startTime_, stopTime_;
#endif
#ifdef _WINDOWS
	static std::chrono::steady_clock::time_point startTime_, stopTime_;
#endif
        static std::chrono::milliseconds elapsedTime_;

    };

}

#endif /* TIME_H */


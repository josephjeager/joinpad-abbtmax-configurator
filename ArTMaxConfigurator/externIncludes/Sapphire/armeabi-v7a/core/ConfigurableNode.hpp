/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConfigurableNode.hpp
 * Author: marzorati
 *
 * Created on July 21, 2017, 3:46 PM
 */

#ifndef CONFIGURABLENODE_HPP
#define CONFIGURABLENODE_HPP


#include "utils/Utils.h"

//#define LOG_TAG "ConfigurableNode"
//#ifdef _DEBUG
#ifdef _ANDROID
#include <android/log.h>
#define LOG_TAG "Hologram/recognition/TemplateDetector"
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
//#endif

#define FROM_FILE 0
#define FROM_STRING 1

namespace sapphire
{

class ConfigurableNode
{
public:

    ConfigurableNode()
    {
        isConfigurated_ = false;
        loadType_ = FROM_FILE;
    }

    virtual ~ConfigurableNode()
    {

    }

    virtual int Configure(std::string& _sectionName, std::string& _yamlString)
    {
        int ret = 0;
        loadType_ = FROM_FILE;
        

        LOGD("ConfigurableNode::Configure");
        LOGD("ConfigurableNode::Configure: ConfigFile: %s %d", _sectionName.c_str(), _sectionName.empty());
        if (!_sectionName.empty())
        {
            LOGD("ConfigurableNode::Configure: Open");

            if (_yamlString.empty())
            {
                loadType_ = FROM_FILE;
                cv::String fileToOpen = _sectionName;
                configFile_.open(fileToOpen, cv::FileStorage::READ);
                LOGD("ConfigurableNode::Configure from FILE: isOpen: %d", configFile_.isOpened());
                fileNode_=configFile_.root(0);
                
            }
            else
            {
                loadType_ = FROM_STRING;
                cv::String fileToOpen = _yamlString;
                configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
                LOGD("ConfigurableNode::Configure from YAML String: isOpen: %d", configFile_.isOpened());
//                LOGD("ConfigurableNode::Configure from YAML String: YAML String: %s", _yamlString.c_str());
                
                //carico la sezione corretta
                
                fileNode_=configFile_[_sectionName];

            }
        }
        else
            ret = -1;

        if (ret == 0)
        {
            if (configFile_.isOpened())
            {

                ret = ConfigureProc( _sectionName, _yamlString);
                LOGD("ConfigurableNode::LoadType: %d Ready", loadType_);
            }
            else
            {
                LOGD("Error: Config file doesn't exist");
                ret = -1;
            }
        }
        if (ret == 0)
        {
//            LOGD("ConfigurableNode::Configure is: %s",_yamlString.c_str());
//            yamlString_.reserve(_yamlString.size());
//            sectionName_.reserve(_sectionName.size());
            LOGD("ConfigurableNode::Configure yamlString_");
//            yamlString_=_yamlString;
            LOGD("ConfigurableNode::Configure _sectionName");
//            sectionName_=_sectionName;
            isConfigurated_ = true;
            LOGD("ConfigurableNode::Configure is Configurated %d",isConfigurated_);
        }
        LOGD("ConfigurableNode::Configure DONE %d",ret);
        
        return ret;
    }


protected:
    virtual int ConfigureProc(std::string& _sectionName, std::string& _yamlString) = 0;


protected:
    bool isConfigurated_;
//    std::string sectionName_;//,yamlString_;
//    cv::FileStorage configFile_;
    cv::FileStorage configFile_;
    cv::FileNode fileNode_;
    
    int loadType_;


};
}

#endif /* CONFIGURABLENODE_HPP */


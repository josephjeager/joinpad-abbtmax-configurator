﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEngine;
using Utils = BrainPad.Core.Utils;

namespace BrainPad.Obj2DLocalization
{
    public class Obj2DLocalizationDllTest : BrainPadCamBehaviour
    {
        [Header("Augmented Reality")] public Camera UnityCamera;

        [HideInInspector] public List<ArObject> ArObjects;

        private static Obj2DLocalizationDllTest _instance;
        private NativeObj2DLocalization _nativeObject;

        private Boolean _isTracking;

        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private Mat _frameMatrix;
        private Mat _frameMatrixPreprocessed;
        private Mat _poseMatrixes;
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private Texture2D texture;
        private Color32[] colors;
        private const TextureFormat format =
#if UNITY_IOS && !UNITY_EDITOR
            TextureFormat.BGRA32;
#else
            TextureFormat.RGBA32;
#endif

        void Awake()
        {
            for (int i = 0; i < ArObjects.Count; i++)
            {
                ArObjects[i].ArGameObjectRoot.SetActive(false);
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public override void OnStart()
        {
            base.OnStart();

//            releaseObjects();

            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _nativeObject = new NativeObj2DLocalization();
            _poseMatrixes = Mat.zeros(0, 0, CvType.CV_64FC1);
            _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);

            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            preview.color = Color.white;
        }

        public override void OnFrame()
        {
#if NATCAM_PROFESSIONAL
            if (!NatCam.PreviewMatrix(ref _frameMatrix)) return;
            colors = colors ?? new Color32[_frameMatrix.cols() * _frameMatrix.rows()];
            texture = texture ?? new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), format, false, false);
            if (texture.width != _frameMatrix.cols() || texture.height != _frameMatrix.rows())
                texture.Resize(_frameMatrix.cols(), _frameMatrix.rows());
            
            OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);                
            Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
            
            ProcessFrameWork();

            if (_isTracking)
            {
                OpenCVForUnity.Utils.matToTexture2D(_frameMatrix, texture, colors);
                preview.texture = texture;
            }
#endif
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                ReleaseObjects();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void ProcessFrameWork()
        {
            _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
            _nativeObject.GetRTMatrixes(_poseMatrixes);

            _foundObjects.Clear();

            for (int i = 0; i < _poseMatrixes.rows() / 5; i++)
            {
                int maskedObjectId = (int) _poseMatrixes.get(i * 5, 0)[0];
                int objectId = maskedObjectId / 1000;

                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                rtMatrix4X4.SetRow(0, new Vector4((float) _poseMatrixes.get(i * 5 + 1, 0)[0], (float) _poseMatrixes.get(i * 5 + 1, 1)[0], (float) _poseMatrixes.get(i * 5 + 1, 2)[0], (float) _poseMatrixes.get(i * 5 + 1, 3)[0]));
                rtMatrix4X4.SetRow(1, new Vector4((float) _poseMatrixes.get(i * 5 + 2, 0)[0], (float) _poseMatrixes.get(i * 5 + 2, 1)[0], (float) _poseMatrixes.get(i * 5 + 2, 2)[0], (float) _poseMatrixes.get(i * 5 + 2, 3)[0]));
                rtMatrix4X4.SetRow(2, new Vector4((float) _poseMatrixes.get(i * 5 + 3, 0)[0], (float) _poseMatrixes.get(i * 5 + 3, 1)[0], (float) _poseMatrixes.get(i * 5 + 3, 2)[0], (float) _poseMatrixes.get(i * 5 + 3, 3)[0]));
                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));

                _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
            }

            List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups = _foundObjects.GroupBy(i => i.Key).ToList();
            for (int i = 0; i < _arObjectsInternal.Count; i++)
            {
                IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject = foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
                if (foundObject == null)
                {
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        Destroy(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }
                }
                else
                {
                    // Add missing instances
                    while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
                    {
                        _arObjectsInternal[i].Instances.Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                    }

                    // Remove exceeding instances
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                    {
                        Destroy(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }

                    // New positions to Gameobjects
                    for (int sharedIndex = 0; sharedIndex < _arObjectsInternal[i].Instances.Count; sharedIndex++)
                    {
                        GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                        Matrix4x4 matrix4X4 = foundObject.ToList()[sharedIndex].Value;
                        
                        // Flip x and y axes for PortraitUpsideDown
                        if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                        {
                            Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                            gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x, -localPosition.y, localPosition.z);
                        }
                        else
                        {
                            gameObjectInstance.transform.localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                        }

                        gameObjectInstance.transform.localRotation = Utils.ExtractRotationFromMatrix(ref matrix4X4);
                        gameObjectInstance.SetActive(true);
                    }
                }
            }

            _isTracking = _foundObjects.Count > 0;
            if (!_isTracking && preview.texture != NatCam.Preview)
            {
                preview.texture = NatCam.Preview;
            }
        }

        void OnDestroy()
        {
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {
            if (_nativeObject != null)
            {
//                _nativeObject.Stop();
                _nativeObject.Destroy();
                _nativeObject = null;
            }
            
            // Hide artifacts generated by screen rotation 
            if (preview != null && preview.texture != null)
            {
                preview.texture = new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), format, false, false);
            }

            if (_poseMatrixes != null)
            {
                _poseMatrixes.release();
                _poseMatrixes = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                _frameMatrixPreprocessed.release();
                _frameMatrixPreprocessed = null;
            }
            
            for (int i = 0; i < _arObjectsInternal.Count; i++)
            {
                for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                {
                    Destroy(_arObjectsInternal[i].Instances[j]);
                    _arObjectsInternal[i].Instances.RemoveAt(j);
                }
            }

            _isTracking = false;
        }

        public class NativeObj2DLocalization : IBrainPadNative
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeObj2DLocalization()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                _nativeObjAddr = nativeCreate();
#endif
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle, float verticalViewAngle)
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeSetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle, verticalViewAngle);
#endif
            }

            public void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane)
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                Mat referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
                OpenCVForUnity.Utils.texture2DToMat(referenceImage, referenceImageMat);
                Mat referenceImageGreyMat = new Mat(referenceImageMat.rows(), referenceImageMat.cols(), CvType.CV_8UC1);
                Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);

                nativeAddNewObjectToDetect(_nativeObjAddr, objectId, referenceImageGreyMat.nativeObj, worldPlane.nativeObj);
#endif
            }

            public void Init()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeInit(_nativeObjAddr);
#endif
            }

            public void Configure()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeConfigure(_nativeObjAddr);
#endif
            }

            public void Start()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStart(_nativeObjAddr);
#endif
            }

            public void Stop()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStop(_nativeObjAddr);
#endif
            }

            public int ProcessFrame(Mat imageRgba)
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProcessFrame(_nativeObjAddr, imageRgba.nativeObj);
#else
            return 0;
#endif
            }

            public int GetRTMatrixes(Mat poseMatrixOut)
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeGetRTMatrixes(_nativeObjAddr, poseMatrixOut.nativeObj);
#else
            return 0;
#endif
            }

            public void Destroy()
            {
#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeDestroy(_nativeObjAddr);
#endif
            }

#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
            const string LIBNAME = "Obj2DLocalization";
#else
        const string LIBNAME = "__Internal";
#endif

            [DllImport(LIBNAME)]
            private static extern IntPtr nativeCreate();

            [DllImport(LIBNAME)]
            private static extern void nativeDestroy(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeAddNewObjectToDetect(IntPtr nativeObjAddr, int objectId, IntPtr queryFrame, IntPtr worldPlane);

            [DllImport(LIBNAME)]
            private static extern int nativeInit(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeSetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov, float vFov);

            [DllImport(LIBNAME)]
            private static extern int nativeConfigure(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStart(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStop(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProcessFrame(IntPtr nativeObjAddr, IntPtr rImage);

//        [DllImport(LIBNAME)]
//        private static extern int nativeGetHomographies(IntPtr nativeObjAddr, IntPtr rHomographiesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetRTMatrixes(IntPtr nativeObjAddr, IntPtr rRTMatrixesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetReferenceObjects(IntPtr nativeObjAddr, IntPtr rRefObjsOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetImage(IntPtr nativeObjAddr, IntPtr rImageOut);

            [DllImport(LIBNAME)]
            private static extern int nativeSetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}
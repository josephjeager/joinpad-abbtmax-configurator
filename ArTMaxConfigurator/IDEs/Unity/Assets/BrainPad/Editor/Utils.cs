﻿using System.Collections.Generic;
using BrainPad.Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.Editor
{
    public static class Utils
    {
        public static bool CheckConfiguration()
        {
            bool check = true;
            if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
            {
                check = false;
                if (EditorUtility.DisplayDialog("BuildTarget", "BrainPad needs Android as BuildTarget, switch BuildTarget to Android?", "Ok", "Cancel"))
                {
                    if (EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android))
                    {
                        check = true;
                    }
                }
            }

            if (!check)
            {
                return false;
            }

            if (PlayerSettings.Android.minSdkVersion < AndroidSdkVersions.AndroidApiLevel18)
            {
                check = false;
                if (EditorUtility.DisplayDialog("Android API level", "BrainPad needs Android API level >= 18, set Android minimum API level to 18?", "Ok", "Cancel"))
                {
                    PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel18;
                    PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;
                    check = true;
                }
            }

            if (!check)
            {
                return false;
            }

            if (!PlayerSettings.MTRendering || !PlayerSettings.GetMobileMTRendering(BuildTargetGroup.Android))
            {
                check = false;
                if (EditorUtility.DisplayDialog("Multithreaded Rendering", "BrainPad needs Android Multithreaded rendering to be enable, enable Android Multithreaded rendering?", "Ok", "Cancel"))
                {
                    PlayerSettings.SetMobileMTRendering(BuildTargetGroup.Android, true);
                    PlayerSettings.MTRendering = true;
                    check = true;
                }
            }

            if (!check)
            {
                return false;
            }

            if (PlayerSettings.defaultInterfaceOrientation == UIOrientation.AutoRotation)
            {
                check = false;

                int dialogResult = EditorUtility.DisplayDialogComplex("Device rotation", "BrainPad needs Android device orientation to be fixed, fix device Default Orientation?", "Portrait", "Landscape", "Cancel");
                if (dialogResult == 0)
                {
                    PlayerSettings.defaultInterfaceOrientation = UIOrientation.Portrait;
                    check = true;
                }
                else if (dialogResult == 1)
                {
                    PlayerSettings.defaultInterfaceOrientation = UIOrientation.LandscapeLeft;
                    check = true;
                }
            }

            if (!check)
            {
                return false;
            }

            if (Camera.main == null)
            {
                check = false;
                EditorUtility.DisplayDialog("Main Camera not found", "BrainPad needs a Unity Camera, please add a Camera to your scene and set it as \"MainCamera\"", "Ok");
            }

            return check;
        }

        public static bool CheckScene()
        {
            // Instantiate ProcessorUnit if it does not exist
            GameObject processorUnit = GameObject.Find("ProcessorUnit");
            if (processorUnit == null)
            {
                processorUnit = new GameObject();
                processorUnit.name = "ProcessorUnit";
            }

            // Instantiate BrainPadCanvas if it does not exist
            GameObject brainPadCanvas = GameObject.Find("BrainPadCanvas");
            if (brainPadCanvas == null)
            {
                brainPadCanvas = AssetDatabase.LoadAssetAtPath("Assets/BrainPad/Prefabs/BrainPadCanvas.prefab", typeof(GameObject)) as GameObject;
                if (brainPadCanvas != null)
                {
                    brainPadCanvas = Object.Instantiate(brainPadCanvas);
                    brainPadCanvas.name = "BrainPadCanvas";
                    brainPadCanvas.GetComponent<Canvas>().worldCamera = Camera.main;
                }
            }

            // Instantiate Obj2DLocalization if not attached to processorUnit
            Obj2DLocalization.Obj2DLocalization obj2DLocalization = processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>();
            if (obj2DLocalization == null)
            {
                processorUnit.AddComponent<Obj2DLocalization.Obj2DLocalization>();
            }

            if (processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().ArObjects == null)
            {
                processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().ArObjects = new List<ArObject>();
            }

            if (processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().UnityCamera == null)
            {
                processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().UnityCamera = Camera.main;
            }

            if (processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().preview == null)
            {
                processorUnit.GetComponent<Obj2DLocalization.Obj2DLocalization>().preview = GameObject.Find("WebcamPreview").GetComponent<RawImage>();
            }

            Camera.main.transform.position = new Vector3(0, 0, 0);
            return true;
        }
    }
}
﻿/* 
*   NatCam Pro
*   Copyright (c) 2016 Yusuf Olokoba
*/

// Make sure to uncomment the definition below in NatCam.cs (Assets>NatCam>Pro>Plugins>Managed>NatCam.cs)
#define OPENCV_API // Uncomment this to run this example properly

namespace NatCamU.Pro.Examples {

    using UnityEngine;
    using Core;
    using Pro;
    #if OPENCV_API
    using OpenCVForUnity;
    #endif

    public class VisionCam : NatCamBehaviour {
        
        #if OPENCV_API

        private Mat matrix;
        private Texture2D texture;
        private Color32[] pixels;
        private const TextureFormat textureFormat =
        #if UNITY_IOS && !UNITY_EDITOR 
        TextureFormat.BGRA32;
        #else
        TextureFormat.RGBA32;
        #endif

        private Mat _frameMatrixPreprocessed;
        
//        public override void OnStart()
//        {
//            base.OnStart();
//
////            releaseObjects();
//
//            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
//            _arObjectsInternal = new List<ArObjectInternal>();
//            _nativeObject = new NativeObj2DLocalization();
//            _poseMatrixes = Mat.zeros(0, 0, CvType.CV_64FC1);
//            _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
//
//            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
//            _nativeObject.Configure();
//            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);
//
//            _nativeObject.Init();
//            _nativeObject.Start();
//
//            preview.color = Color.white;
//        }
        
        
        public override void OnStart () {
            // Initialize the texture
            NatCam.PreviewMatrix(ref matrix);
            texture = new Texture2D(matrix.cols(), matrix.rows(), textureFormat, false, false);
            pixels = new Color32[texture.width * texture.height];
            
            _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
        }

        public override void OnFrame () {
            // Get the current preview frame as an OpenCV matrix
            NatCam.PreviewMatrix(ref matrix);
            // Draw a diagonal line on our image
            Imgproc.line(matrix, new Point(0, 0), new Point(matrix.cols(), matrix.rows()), new Scalar(255, 0, 0, 255), 4);
            // Update the texture
            Imgproc.cvtColor(matrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
            
            Utils.matToTexture2D(_frameMatrixPreprocessed, texture, pixels);
            // Display the result
            preview.texture = texture;
        }
        #endif
    }
}
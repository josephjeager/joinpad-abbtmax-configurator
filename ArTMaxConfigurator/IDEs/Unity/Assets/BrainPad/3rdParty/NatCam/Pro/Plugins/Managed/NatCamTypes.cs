/* 
*   NatCam Pro
*   Copyright (c) 2017 Yusuf Olokoba
*/

namespace NatCamU.Pro {

    using Core;

    #region --Delegates--
    /// <summary>
    /// A delegate type used to provide the path to a recorded video
    /// </summary>
    [ProDoc(@"VideoCallback")]
	public delegate void VideoCallback (string path);
    #endregion


    #region --Value Types--

    /// <summary>
    /// A value type used to specify recording configuration settings
    /// </summary>
    [ProDoc(@"Configuration")]
    public struct Configuration {

        #region --Op vars--

        [ProDoc(@"Bitrate")] public readonly int bitrate;
        [ProDoc(@"KeyframeInterval")] public readonly int keyframeInterval;
        [ProDoc(@"RecordAudio")] public readonly bool recordAudio;

        /// <summary>
        /// Default recording configuration
        /// </summary>
        [ProDoc(@"DefaultConfiguration")]
        public static readonly Configuration Default = new Configuration((int)(11.4f * 1280 * 720), 3, false);
        /// <summary>
        /// Default recording configuration with microphone audio
        /// </summary>
        [ProDoc(@"DefaultAudioConfiguration")]
        public static readonly Configuration DefaultWithAudio = new Configuration(Default.bitrate, Default.keyframeInterval, true);
        #endregion


        #region --Client API--

        /// <summary>
        /// Create a recoridng configuration
        /// </summary>
        /// <param name="bitrate">Bitrate of the video encoder</param>
        /// <param name="keyframeInterval">Keyframe interval in seconds of the video encoder</param>
        /// <param name="recordAudio">Whether audio should be recorded from the microphone</param>
        [ProDoc(@"ConfigurationCtor")]
        public Configuration (int bitrate, int keyframeInterval, bool recordAudio) {
            this.bitrate = bitrate;
            this.keyframeInterval = keyframeInterval;
            this.recordAudio = recordAudio;
        }
        #endregion
    }
    #endregion
}
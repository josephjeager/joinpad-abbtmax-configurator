/* 
*   NatCam Pro
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core.Platforms {

    using AOT;
    using System;
    using System.Runtime.InteropServices;
    using Dispatch;
    using Pro;

    public sealed partial class NatCamiOS {

        #region --Op vars--
        private VideoCallback videoCallback;
        #endregion


        #region --Properties--
        public bool IsRecording { get { return NatCamBridge.IsRecording(); }}
        #endregion


        #region --Client API--

        public void PreviewBuffer (out IntPtr ptr, out int width, out int height, out int size) {
            NatCamBridge.PreviewBuffer(out ptr, out width, out height, out size);
        }

        public void StartRecording (Configuration configuration, VideoCallback callback) {
            videoCallback = callback;
            NatCamBridge.StartRecording(configuration.bitrate, configuration.keyframeInterval, configuration.recordAudio);
        }

        public void StopRecording () {
            NatCamBridge.StopRecording();
        }
        #endregion


        #region --Callbacks--

        [MonoPInvokeCallback(typeof(VideoCallback))]
        private static void OnVideo (string path) {
            using (var dispatch = new MainDispatch()) dispatch.Dispatch(() => instance.videoCallback(path));
        }
        #endregion
    }
}
﻿/* 
*   NatCam Pro
*   Copyright (c) 2016 Yusuf Olokoba
*/

// Make sure to uncomment the definition below in NatCam.cs (Assets>NatCam>Pro>Plugins>Managed>NatCam.cs)

#define OPENCV_API // Uncomment this to run this example properly

using System;
using System.Collections.Generic;
using System.Linq;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using OpenCVForUnitySample;
using UnityEngine;
using Object = System.Object;
using Utils = BrainPad.Core.Utils;

namespace BrainPad._3rdParty.NatCam.Pro.Examples.VisionCam
{
#if OPENCV_API
#endif

    public class VisionToGreyCam : NatCamBehaviour
    {
#if OPENCV_API

        [Header("Augmented Reality")] public Camera UnityCamera;

        public List<ArObject> ArObjects;

        private Texture2D texture;
        private Color32[] colors;
        private const TextureFormat format =
#if UNITY_IOS && !UNITY_EDITOR
        TextureFormat.BGRA32;
        #else
            TextureFormat.RGBA32;
#endif

        private Boolean _isTracking;

        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private Mat _frameMatrix;
        private Mat _frameMatrixPreprocessed;
        private Mat _poseMatrixes;
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private static VisionToGreyCam _instance;
        private Obj2DLocalization.Obj2DLocalization.NativeObj2DLocalization _nativeObject;

        private static Object lockObj = new Object();

//        public override void OnStart()
//        {
//            base.OnStart();
//
////            releaseObjects();
//
//            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
//            _arObjectsInternal = new List<ArObjectInternal>();
//            _nativeObject = new NativeObj2DLocalization();
//            _poseMatrixes = Mat.zeros(0, 0, CvType.CV_64FC1);
//            _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
//
//            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
//            _nativeObject.Configure();
//            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);
//
//            _nativeObject.Init();
//            _nativeObject.Start();
//
//            preview.color = Color.white;
//        }


        private bool initialized;

        public override void OnStart()
        {
            // Initialize the texture
            NatCamU.Core.NatCam.PreviewMatrix(ref _frameMatrix);
            texture = new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), format, false, false);
            colors = new Color32[texture.width * texture.height];

//            base.OnStart();

//            releaseObjects();

            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _poseMatrixes = Mat.zeros(0, 0, CvType.CV_64FC1);

            _nativeObject = new Obj2DLocalization.Obj2DLocalization.NativeObj2DLocalization();
            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            _frameMatrixPreprocessed = new Mat(NatCamU.Core.NatCam.Preview.height, NatCamU.Core.NatCam.Preview.width, CvType.CV_8UC1);

            initialized = true;
        }

        public override void OnFrame()
        {
            // Get the current preview frame as an OpenCV matrix
            NatCamU.Core.NatCam.PreviewMatrix(ref _frameMatrix);

            colors = colors ?? new Color32[_frameMatrix.cols() * _frameMatrix.rows()];
            texture = texture ?? new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), format, false, false);
            if (texture.width != _frameMatrix.cols() || texture.height != _frameMatrix.rows())
                texture.Resize(_frameMatrix.cols(), _frameMatrix.rows());


            OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
//            
//            // Draw a diagonal line on our image
//            Imgproc.line(_frameMatrix, new Point(0, 0), new Point(_frameMatrix.cols(), _frameMatrix.rows()), new Scalar(255, 0, 0, 255), 4);
            lock (lockObj)
            {
                Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
            }
//             Mat frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
//             // Update the texture
//            Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
//            
//            _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
//            _nativeObject.GetRTMatrixes(_poseMatrixes);
//


//            _foundObjects.Clear();
////
//            for (int i = 0; i < _poseMatrixes.rows() / 5; i++)
//            {
//                int maskedObjectId = (int) _poseMatrixes.get(i * 5, 0)[0];
//                int objectId = maskedObjectId / 1000;
//
//                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
//                rtMatrix4X4.SetRow(0, new Vector4((float) _poseMatrixes.get(i * 5 + 1, 0)[0], (float) _poseMatrixes.get(i * 5 + 1, 1)[0], (float) _poseMatrixes.get(i * 5 + 1, 2)[0], (float) _poseMatrixes.get(i * 5 + 1, 3)[0]));
//                rtMatrix4X4.SetRow(1, new Vector4((float) _poseMatrixes.get(i * 5 + 2, 0)[0], (float) _poseMatrixes.get(i * 5 + 2, 1)[0], (float) _poseMatrixes.get(i * 5 + 2, 2)[0], (float) _poseMatrixes.get(i * 5 + 2, 3)[0]));
//                rtMatrix4X4.SetRow(2, new Vector4((float) _poseMatrixes.get(i * 5 + 3, 0)[0], (float) _poseMatrixes.get(i * 5 + 3, 1)[0], (float) _poseMatrixes.get(i * 5 + 3, 2)[0], (float) _poseMatrixes.get(i * 5 + 3, 3)[0]));
//                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
//
//                _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
//            }
//
////            Debug.Log("First For  ");
//
//            List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups = _foundObjects.GroupBy(i => i.Key).ToList();
//            for (int i = 0; i < _arObjectsInternal.Count; i++)
//            {
//                IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject = foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
//                if (foundObject == null)
//                {
//                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
//                    {
//                        Destroy(_arObjectsInternal[i].Instances[j]);
//                        _arObjectsInternal[i].Instances.RemoveAt(j);
//                    }
//                }
//                else
//                {
//                    // Add missing instances
//                    while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
//                    {
//                        _arObjectsInternal[i].Instances.Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
//                    }
//
//                    // Remove exceeding instances
//                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
//                    {
//                        Destroy(_arObjectsInternal[i].Instances[j]);
//                        _arObjectsInternal[i].Instances.RemoveAt(j);
//                    }
//
//                    // New positions to Gameobjects
//                    for (int sharedIndex = 0; sharedIndex < _arObjectsInternal[i].Instances.Count; sharedIndex++)
//                    {
//                        GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
//                        Matrix4x4 matrix4X4 = foundObject.ToList()[sharedIndex].Value;
//
//                        // Flip x and y axes for PortraitUpsideDown
//                        if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
//                        {
//                            Vector3 localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
//                            gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x, -localPosition.y, localPosition.z);
//                        }
//                        else
//                        {
//                            gameObjectInstance.transform.localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
//                        }
//
//                        gameObjectInstance.transform.localRotation = ARUtils.ExtractRotationFromMatrix(ref matrix4X4);
//                        gameObjectInstance.SetActive(true);
//                    }
//                }
//            }

//            Debug.Log("Second For  ");

//                    _isTracking = _foundObjects.Count > 0;
//                    if (!_isTracking && preview.texture != NatCamU.Core.NatCam.Preview)
//                    {
//                        preview.texture = NatCamU.Core.NatCam.Preview;
//                    }


            OpenCVForUnity.Utils.matToTexture2D(_frameMatrix, texture, colors);
//            // Display the result
            preview.texture = texture;
        }
#endif

        void Update()
        {
//            try
//            {
                if (initialized)
                {
                    Mat frameMatrixPreprocessed = new Mat();
                    lock (lockObj)
                    {
                        _frameMatrixPreprocessed.copyTo(frameMatrixPreprocessed);
                    }

                    _nativeObject.ProcessFrame(frameMatrixPreprocessed);
                    _nativeObject.GetRTMatrixes(_poseMatrixes);


                    _foundObjects.Clear();
//
                    for (int i = 0; i < _poseMatrixes.rows() / 5; i++)
                    {
                        int maskedObjectId = (int) _poseMatrixes.get(i * 5, 0)[0];
                        int objectId = maskedObjectId / 1000;

                        Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                        rtMatrix4X4.SetRow(0, new Vector4((float) _poseMatrixes.get(i * 5 + 1, 0)[0], (float) _poseMatrixes.get(i * 5 + 1, 1)[0], (float) _poseMatrixes.get(i * 5 + 1, 2)[0], (float) _poseMatrixes.get(i * 5 + 1, 3)[0]));
                        rtMatrix4X4.SetRow(1, new Vector4((float) _poseMatrixes.get(i * 5 + 2, 0)[0], (float) _poseMatrixes.get(i * 5 + 2, 1)[0], (float) _poseMatrixes.get(i * 5 + 2, 2)[0], (float) _poseMatrixes.get(i * 5 + 2, 3)[0]));
                        rtMatrix4X4.SetRow(2, new Vector4((float) _poseMatrixes.get(i * 5 + 3, 0)[0], (float) _poseMatrixes.get(i * 5 + 3, 1)[0], (float) _poseMatrixes.get(i * 5 + 3, 2)[0], (float) _poseMatrixes.get(i * 5 + 3, 3)[0]));
                        rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));

                        _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
                    }

//            Debug.Log("First For  ");

                    List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups = _foundObjects.GroupBy(i => i.Key).ToList();
                    for (int i = 0; i < _arObjectsInternal.Count; i++)
                    {
                        IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject = foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
                        if (foundObject == null)
                        {
                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                            {
                                Destroy(_arObjectsInternal[i].Instances[j]);
                                _arObjectsInternal[i].Instances.RemoveAt(j);
                            }
                        }
                        else
                        {
                            // Add missing instances
                            while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
                            {
                                _arObjectsInternal[i].Instances.Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                            }

                            // Remove exceeding instances
                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                            {
                                Destroy(_arObjectsInternal[i].Instances[j]);
                                _arObjectsInternal[i].Instances.RemoveAt(j);
                            }

                            // New positions to Gameobjects
                            for (int sharedIndex = 0; sharedIndex < _arObjectsInternal[i].Instances.Count; sharedIndex++)
                            {
                                GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                                Matrix4x4 matrix4X4 = foundObject.ToList()[sharedIndex].Value;

                                // Flip x and y axes for PortraitUpsideDown
                                if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                                {
                                    Vector3 localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
                                    gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x, -localPosition.y, localPosition.z);
                                }
                                else
                                {
                                    gameObjectInstance.transform.localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
                                }

                                gameObjectInstance.transform.localRotation = ARUtils.ExtractRotationFromMatrix(ref matrix4X4);
                                gameObjectInstance.SetActive(true);
                            }
                        }
                    }

//                    Debug.Log("Second For  ");

//                    _isTracking = _foundObjects.Count > 0;
//                    if (!_isTracking && preview.texture != NatCamU.Core.NatCam.Preview)
//                    {
//                        preview.texture = NatCamU.Core.NatCam.Preview;
//                    }
                }
            }
//            catch (Exception e)
//            {
//                Debug.Log("EXCEPTION!! " + e.Message);
//            }
//        }


//                    OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
//
//                    // Draw a diagonal line on our image
//                    Imgproc.line(_frameMatrix, new Point(0, 0), new Point(_frameMatrix.cols(), _frameMatrix.rows()), new Scalar(255, 0, 0, 255), 4);
//                    // Update the texture
//                    Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
//
//                    _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
//                    _nativeObject.GetRTMatrixes(_poseMatrixes);
//
//                    _foundObjects.Clear();
//
//                    for (int i = 0; i < _poseMatrixes.rows() / 5; i++)
//                    {
//                        int maskedObjectId = (int) _poseMatrixes.get(i * 5, 0)[0];
//                        int objectId = maskedObjectId / 1000;
//
//                        Matrix4x4 rtMatrix4X4 = new Matrix4x4();
//                        rtMatrix4X4.SetRow(0, new Vector4((float) _poseMatrixes.get(i * 5 + 1, 0)[0], (float) _poseMatrixes.get(i * 5 + 1, 1)[0], (float) _poseMatrixes.get(i * 5 + 1, 2)[0], (float) _poseMatrixes.get(i * 5 + 1, 3)[0]));
//                        rtMatrix4X4.SetRow(1, new Vector4((float) _poseMatrixes.get(i * 5 + 2, 0)[0], (float) _poseMatrixes.get(i * 5 + 2, 1)[0], (float) _poseMatrixes.get(i * 5 + 2, 2)[0], (float) _poseMatrixes.get(i * 5 + 2, 3)[0]));
//                        rtMatrix4X4.SetRow(2, new Vector4((float) _poseMatrixes.get(i * 5 + 3, 0)[0], (float) _poseMatrixes.get(i * 5 + 3, 1)[0], (float) _poseMatrixes.get(i * 5 + 3, 2)[0], (float) _poseMatrixes.get(i * 5 + 3, 3)[0]));
//                        rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));
//
//                        _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
//                    }
//
////            Debug.Log("First For  ");
//
//                    List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups = _foundObjects.GroupBy(i => i.Key).ToList();
//                    for (int i = 0; i < _arObjectsInternal.Count; i++)
//                    {
//                        IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject = foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
//                        if (foundObject == null)
//                        {
//                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
//                            {
//                                Destroy(_arObjectsInternal[i].Instances[j]);
//                                _arObjectsInternal[i].Instances.RemoveAt(j);
//                            }
//                        }
//                        else
//                        {
//                            // Add missing instances
//                            while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
//                            {
//                                _arObjectsInternal[i].Instances.Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
//                            }
//
//                            // Remove exceeding instances
//                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
//                            {
//                                Destroy(_arObjectsInternal[i].Instances[j]);
//                                _arObjectsInternal[i].Instances.RemoveAt(j);
//                            }
//
//                            // New positions to Gameobjects
//                            for (int sharedIndex = 0; sharedIndex < _arObjectsInternal[i].Instances.Count; sharedIndex++)
//                            {
//                                GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
//                                Matrix4x4 matrix4X4 = foundObject.ToList()[sharedIndex].Value;
//
//                                // Flip x and y axes for PortraitUpsideDown
//                                if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
//                                {
//                                    Vector3 localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
//                                    gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x, -localPosition.y, localPosition.z);
//                                }
//                                else
//                                {
//                                    gameObjectInstance.transform.localPosition = ARUtils.ExtractTranslationFromMatrix(ref matrix4X4);
//                                }
//
//                                gameObjectInstance.transform.localRotation = ARUtils.ExtractRotationFromMatrix(ref matrix4X4);
//                                gameObjectInstance.SetActive(true);
//                            }
//                        }
//                    }
//
////            Debug.Log("Second For  ");
//
////                    _isTracking = _foundObjects.Count > 0;
////                    if (!_isTracking && preview.texture != NatCamU.Core.NatCam.Preview)
////                    {
////                        preview.texture = NatCamU.Core.NatCam.Preview;
////                    }
//
//                    OpenCVForUnity.Utils.matToTexture2D(_frameMatrixPreprocessed, texture, colors);
//                    // Display the result
//                    preview.texture = texture;
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.Log("EXCEPTION!!! " + e.Message);
//            }
//        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                ReleaseObjects();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void OnDestroy()
        {
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {
            if (_nativeObject != null)
            {
//                _nativeObject.Stop();
                _nativeObject.Destroy();
                _nativeObject = null;
            }

            // Hide artifacts generated by screen rotation 
            if (preview != null && preview.texture != null)
            {
                preview.texture = new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), format, false, false);
            }

            if (_poseMatrixes != null)
            {
                _poseMatrixes.release();
                _poseMatrixes = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                _frameMatrixPreprocessed.release();
                _frameMatrixPreprocessed = null;
            }

            for (int i = 0; i < _arObjectsInternal.Count; i++)
            {
                for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                {
                    Destroy(_arObjectsInternal[i].Instances[j]);
                    _arObjectsInternal[i].Instances.RemoveAt(j);
                }
            }

            _isTracking = false;
        }
    }
}
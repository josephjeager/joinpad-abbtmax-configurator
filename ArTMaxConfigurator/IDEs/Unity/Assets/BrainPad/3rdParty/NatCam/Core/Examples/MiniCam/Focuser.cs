/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core.Examples {

    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;
    using Core;

    [RequireComponent(typeof(EventTrigger), typeof(Graphic))]
    public sealed class Focuser : MonoBehaviour, IPointerUpHandler {
        
        Vector3[] corners = new Vector3[4]; 

        void IPointerUpHandler.OnPointerUp (PointerEventData eventData) {
            Vector3 worldPoint;
            RectTransform transform = this.transform as RectTransform;
            if (!RectTransformUtility.ScreenPointToWorldPointInRectangle(transform, eventData.pressPosition, eventData.pressEventCamera, out worldPoint)) return;
            transform.GetWorldCorners(corners);
            var point = worldPoint - corners[0];
            var size = new Vector2(corners[3].x, corners[1].y) - (Vector2)corners[0];
            Vector2 relativePoint = new Vector2(point.x / size.x, point.y / size.y);
            if (NatCam.IsPlaying) NatCam.Camera.SetFocus(relativePoint);
        }
    }
}
/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

//#define CORE_DOC // Internal. Do not use
//#define PRO_DOC // Internal. Do not use // Remember to enable OPENCV_API

#if CORE_DOC || PRO_DOC
    #define DOC_GEN
#endif

namespace NatCamU.Core {

    using System;
    #if DOC_GEN
    using System.Linq;
    using Calligraphy;
    #endif

    public sealed class CoreDocAttribute : 
    #if CORE_DOC
    CADescriptionAttribute {
        public CoreDocAttribute (string descriptionKey) : base(Docs.docs[descriptionKey]) {}
        public CoreDocAttribute (string summaryKey, string descriptionKey) : base(Docs.docs[descriptionKey], Docs.docs[summaryKey]) {}
    #else
    Attribute {
        public CoreDocAttribute (string descriptionKey) {}
        public CoreDocAttribute (string summaryKey, string descriptionKey) {}
    #endif
    }

    public sealed class ProDocAttribute : 
    #if PRO_DOC
    CADescriptionAttribute {
        public ProDocAttribute (string descriptionKey) : base(Docs.docs[descriptionKey]) {}
        public ProDocAttribute (string summaryKey, string descriptionKey) : base(Docs.docs[descriptionKey], Docs.docs[summaryKey]) {}
    #else
    Attribute {
        public ProDocAttribute (string descriptionKey) {}
        public ProDocAttribute (string summaryKey, string descriptionKey) {}
    #endif
    }

    public sealed class CodeAttribute :
    #if DOC_GEN
    CACodeExampleAttribute {
        public CodeAttribute (string key) : base(Docs.examples[key]) {}
    #else
    Attribute {
        public CodeAttribute (string key) {}
    #endif
    }

    public sealed class RefAttribute :
    #if DOC_GEN
    CASeeAlsoAttribute {
        public RefAttribute (params string[] keys) : base(keys.Select(k => Docs.references[k]).ToArray()) {}
    #else
    Attribute {
        public RefAttribute (params string[] keys) {}
    #endif
    }
}

/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Dispatch {

    using UnityEngine;
    using System;
    using System.Runtime.InteropServices;

    public sealed class RenderDispatch : MainDispatch {

        #region --Ctor--

        /// <summary>
        /// Creates a dispatcher that will execute delegates on the render thread
        /// </summary>
        public RenderDispatch () : base () {
            base.Dispatch(() => GL.IssuePluginEvent(NatCamRenderDelegate(), 0x6723872), true);
            Debug.Log("NatCam Dispatch: Initialized render dispatch");
        }
        #endregion


        #region --Client API--

        /// <summary>
        /// DO NOT USE
        /// </summary>
        public override void Dispatch (Action action, bool repeat) {}
        #endregion


        #region --Native Interop--

        #if UNITY_IOS
        [DllImport("__Internal")]
        #else
        [DllImport("NatCamRenderDispatch")]
        #endif
        private static extern IntPtr NatCamRenderDelegate ();
        #endregion
    }
}
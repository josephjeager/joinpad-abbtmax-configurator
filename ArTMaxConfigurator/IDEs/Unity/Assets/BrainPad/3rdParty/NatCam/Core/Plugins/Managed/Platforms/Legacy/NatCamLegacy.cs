/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core.Platforms {

    using UnityEngine;
    using Dispatch;

    public sealed partial class NatCamLegacy : INatCam {

        #region --Events--
        public event PreviewCallback OnStart;
        public event PreviewCallback OnFrame;
        #endregion


        #region --Op vars--
        private Color32[] photoBuffer;
        private int camera = -1;
        private bool firstFrame;
        private IDispatch dispatch;
        private readonly IDeviceCamera device;
        #endregion


        #region --Properties--
        public IDeviceCamera Device { get { return device; }}
        public int Camera {
            get {
                return camera;
            }
            set {
                if (IsPlaying) {
                    Pause();
                    camera = value;
                    WebCamTexture.Destroy(PreviewTexture);
                    PreviewTexture = null;
                    Play();
                } else {
                    camera = value;
                    if (PreviewTexture) WebCamTexture.Destroy(PreviewTexture);
                    PreviewTexture = null;
                }
            }
        }
        public WebCamTexture PreviewTexture { get; private set; }
        public Texture Preview { get { return PreviewTexture; }}
        public bool IsInitialized { get { return PreviewTexture; }}
        public bool IsPlaying { get { return PreviewTexture && PreviewTexture.isPlaying; }}
        public bool Verbose { set {}}
        public bool HasPermissions { get { return true; }}
        #endregion


        #region --Ctor--

        public NatCamLegacy () {
            device = new NatCamDeviceLegacy();
            Debug.Log("NatCam: Initialized NatCam 1.6 Legacy backend");
        }
        #endregion
        

        #region --Operations--

        public void Play () {
            // Create dispatch
            if (dispatch == null) {
                dispatch = new MainDispatch();
                dispatch.Dispatch(Update, true);
            }
            // Create preview
            string name = WebCamTexture.devices[camera].name;
            var resolution = device.GetPreviewResolution(camera);
            var rate = Mathf.Max(30, (int)device.GetFramerate(camera));
            PreviewTexture = resolution.width == 0 ?  new WebCamTexture(name) : new WebCamTexture(name, resolution.width, resolution.height, rate);
            // Start preview
            Resume();      
        }

        public void Pause () {
            PreviewTexture.Stop();
        }

        public void Resume () {
            firstFrame = true;
            PreviewTexture.Play();
        }

        public void Release () {
            if (!PreviewTexture) return;
            OnStart =
            OnFrame = null;
            #if NATCAM_PRO || NATCAM_PROFESSIONAL
            ReleaseBuffer();
            #endif
            PreviewTexture.Stop();
            WebCamTexture.Destroy(PreviewTexture);
            PreviewTexture = null;
            photoBuffer = null;
            dispatch.Release();
            dispatch = null;
            camera = -1;
        }

        public void CapturePhoto (PhotoCallback callback) {
            if (!PreviewTexture || !PreviewTexture.isPlaying) return;
            var photo = new Texture2D(PreviewTexture.width, PreviewTexture.height, TextureFormat.RGB24, false, false);
            if (photoBuffer == null) photoBuffer = PreviewTexture.GetPixels32();
            else PreviewTexture.GetPixels32(photoBuffer);
            photo.SetPixels32(photoBuffer);
            photo.Apply();
            if (callback != null) callback(photo);
        }
        #endregion


        #region --State Management--

        private void Update () {
            if (!PreviewTexture || !PreviewTexture.isPlaying) return;
            if (!PreviewTexture.didUpdateThisFrame || PreviewTexture.width + PreviewTexture.height == 16 << 1) return;
            if (firstFrame) {
                #if NATCAM_PRO || NATCAM_PROFESSIONAL
                InitializeBuffer();
                #endif
                if (OnStart != null) OnStart();
                firstFrame = false;
            }
            if (OnFrame != null) OnFrame();
        }
        #endregion
    }
}
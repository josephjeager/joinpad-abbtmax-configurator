/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core {

	using UnityEngine;
	using System;

    #region --Delegates--
    /// <summary>
    /// A delegate type that NatCam uses to initialize NatCamPreviewBehaviours.
    /// </summary>
    [CoreDoc(@"PreviewCallback")]
	public delegate void PreviewCallback ();
    /// <summary>
    /// A delegate type that NatCam uses to pass a captured photo to subscribers.
    /// </summary>
	[CoreDoc(@"PhotoCallback")]
    public delegate void PhotoCallback (Texture2D photo);
    #endregion


    #region --Enumerations--
    [CoreDoc(@"ExposureMode")] public enum ExposureMode : byte {
	    [CoreDoc(@"AutoExpose")] AutoExpose = 0,
		[CoreDoc(@"Locked")] Locked = 1
	}
    [CoreDoc(@"FlashMode")] public enum FlashMode : byte {
		[CoreDoc(@"FlashAuto")] Auto = 0,
		[CoreDoc(@"FlashOn")] On = 1,
		[CoreDoc(@"FlashOff")] Off = 2
	}
	[CoreDoc(@"FocusMode"), Flags] public enum FocusMode : byte {
        [CoreDoc(@"FocusOff")] Off = 0,
        [CoreDoc(@"TapToFocus")] TapToFocus = 1,
        [CoreDoc(@"AutoFocus")] AutoFocus = 2
    }
    #endregion


    #region --Value Types--
    
    [Serializable, CoreDoc(@"Resolution")]
    public struct Resolution {
        [CoreDoc(@"ResolutionWidth")] public int width;
        [CoreDoc(@"ResolutionHeight")] public int height;
        [CoreDoc(@"ResolutionCtor")] public Resolution (int width, int height) { this.width = width; this.height = height; }
        [CoreDoc(@"480p")] public static readonly Resolution _640x480 = new Resolution(640, 480);
        [CoreDoc(@"720p")] public static readonly Resolution _1280x720 = new Resolution(1280, 720);
        [CoreDoc(@"1080p")] public static readonly Resolution _1920x1080 = new Resolution(1920, 1080);
        [CoreDoc(@"LowestResolution")] public static readonly Resolution Lowest = new Resolution(50, 50);       // NatCam will pick the highest resolution close to this
        [CoreDoc(@"HighestResolution")] public static readonly Resolution Highest = new Resolution(9999, 9999);   // NatCam will pick the lowest resolution close to this
    }
    #endregion
}
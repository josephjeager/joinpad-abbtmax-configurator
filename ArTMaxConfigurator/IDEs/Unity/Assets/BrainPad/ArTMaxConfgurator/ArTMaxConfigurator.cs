﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BrainPad.Core;
using NatCamU.Core;
using OpenCVForUnity;
using UnityEditor;
using UnityEngine;
using Object = System.Object;
using Utils = BrainPad.Core.Utils;

namespace BrainPad.ArTMaxConfigurator
{
    public class ArTMaxConfigurator : BrainPadCamBehaviour
    {
        public GameObject ButtonsXT2Poli;
        public GameObject InstructionXT2;
        public GameObject MenuXT5;
        public GameObject MenuXT24Poli;
        public GameObject MenuXT23Poli;

        public GameObject ImageBtn;
        public GameObject VideoBtn;
        public GameObject PdbBtn;

        [Header("Augmented Reality")] public Camera UnityCamera;

        public List<ArObject> ArObjects;

        public static ArTMaxConfigurator _instance;
        private NativeArTMaxConfigurator _nativeObject;

        public bool _isTracking;

        //public GameObject ViewFinderBtn;
        public GameObject MainMenu;
        public GameObject ViewFinder;
       


        private List<KeyValuePair<int, Matrix4x4>> _foundObjects;
        private Mat _frameMatrix;

        private Mat _frameMatrixPreprocessed;

        private Mat _poseMatrix;
        private Matrix4x4 _rtMatrix4X4;
        private List<ArObjectInternal> _arObjectsInternal;

        private bool _initialized;
        private static Object _lock = new Object();

        private Texture2D _texture;
        private Color32[] _colors;
        private const TextureFormat Format =
#if UNITY_IOS && !UNITY_EDITOR
            TextureFormat.BGRA32;
#else
            TextureFormat.RGBA32;
#endif

        void Awake()
        {
            for (int i = 0; i < ArObjects.Count; i++)
            {
                ArObjects[i].ArGameObjectRoot.SetActive(false);
            }

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
        }

        public override void OnStart()
        {
            base.OnStart();

            ReleaseObjects();

            _foundObjects = new List<KeyValuePair<int, Matrix4x4>>();
            _arObjectsInternal = new List<ArObjectInternal>();
            _nativeObject = new NativeArTMaxConfigurator();
            _poseMatrix = new Mat(5, 4, CvType.CV_64FC1);
            lock (_lock)
            {
                _frameMatrixPreprocessed = new Mat(NatCam.Preview.height, NatCam.Preview.width, CvType.CV_8UC1);
            }

            Utils.ConfigureCamera(UnityCamera, preview, _nativeObject);
            _nativeObject.Configure();
            Utils.ConfigureArObjectsInternal(ArObjects, _arObjectsInternal, _nativeObject);

            _nativeObject.Init();
            _nativeObject.Start();

            preview.color = Color.white;

            _initialized = true;
        }

        public override void OnFrame()
        {
//            try
//            {
//#if NATCAM_PRO || NATCAM_PROFESSIONAL
                if (!NatCam.PreviewMatrix(ref _frameMatrix)) return;
                _colors = _colors ?? new Color32[_frameMatrix.cols() * _frameMatrix.rows()];
                _texture = _texture ?? new Texture2D(_frameMatrix.cols(), _frameMatrix.rows(), Format, false, false);
                if (_texture.width != _frameMatrix.cols() || _texture.height != _frameMatrix.rows())
                    _texture.Resize(_frameMatrix.cols(), _frameMatrix.rows());

                OpenCVForUnity.Core.flip(_frameMatrix, _frameMatrix, 0);
                lock (_lock)
                {
                    Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
                }

//            New logic, now ProcessFrameWork() is executed on Unity main thread (Update())
//            ProcessFrameWork();

//            Not needed anymore
//            if (_isTracking)
//            {
//            OpenCVForUnity.Utils.matToTexture2D(_frameMatrix, _texture, _colors);
                OpenCVForUnity.Utils.fastMatToTexture2D(_frameMatrix, _texture);
                preview.texture = _texture;
//            }
//            catch (Exception e)
//            {
//                Debug.Log(e.StackTrace);
//            }

//            }
//#endif
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                ReleaseObjects();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                ReleaseObjects();
            }
        }

        void ProcessFrameWork()
        {
            //            try
            //            {



          



            if (_initialized)
                {
                    lock (_lock)
                    {
                        _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
                    }

                try { 
               // Debug.Log("PRE RT detected");
                _nativeObject.GetRTMatrix(_poseMatrix);


              //  Debug.Log("POST RT detected");
                _foundObjects.Clear();

                    for (int i = 0; i < _poseMatrix.rows() / 5; i++)
                    {

                 //   Debug.Log("obj detected");

                   //     Debug.Log(GameObject.FindGameObjectWithTag("ViewFinderBtn"));

                        //GameObject ViewFinderBtn = GameObject.FindGameObjectWithTag("ViewFinderBtn");
                        //ViewFinderBtn.SetActive(true);
                 
                        ViewFinder.SetActive(false);

                        /*GameObject[] Areas = GameObject.FindGameObjectsWithTag("Area");
                        foreach (GameObject Area in Areas)
                        {
                            Area.GetComponent<OpenArea>().UnlitComponents();
                        }
                        */




                        int maskedObjectId = (int) _poseMatrix.get(i * 5, 0)[0];
                        int objectId = maskedObjectId / 1000;
                        
                        /*
                        //check position
                        double nx = (float) _poseMatrix.get(i * 5 + 1, 0)[0];//_rtMatrix.at<double>(0, 0);
                        double ny = (float) _poseMatrix.get(i * 5 + 2, 0)[0];//_rtMatrix.at<double>(1, 0);
                        double nz = (float) _poseMatrix.get(i * 5 + 3, 0)[0];//_rtMatrix.at<double>(2, 0);
                        double sz = (float) _poseMatrix.get(i * 5 + 3, 1)[0];//_rtMatrix.at<double>(2, 1);
                        double az = (float) _poseMatrix.get(i * 5 + 3, 2)[0];//_rtMatrix.at<double>(2, 2);

                        double sy = Math.Sqrt(nx * nx + ny * ny);

                        double phi= Math.Atan2(sz, az); //atan2(sinTheta*ax-cosTheta*ay,cosTheta*sy-sinTheta*sx);
                        double gamma = Math.Atan2(-nz, sy);
                        double theta = Math.Atan2(ny, nx);

                        if (Math.Abs(phi) < 0.1 && Math.Abs(gamma) < 0.1 && Math.Abs(theta) < 0.1)
                        {
                        */
                            Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                            rtMatrix4X4.SetRow(0,
                                new Vector4((float) _poseMatrix.get(i * 5 + 1, 0)[0],
                                    (float) _poseMatrix.get(i * 5 + 1, 1)[0],
                                    (float) _poseMatrix.get(i * 5 + 1, 2)[0],
                                    (float) _poseMatrix.get(i * 5 + 1, 3)[0]));
                            rtMatrix4X4.SetRow(1,
                                new Vector4((float) _poseMatrix.get(i * 5 + 2, 0)[0],
                                    (float) _poseMatrix.get(i * 5 + 2, 1)[0],
                                    (float) _poseMatrix.get(i * 5 + 2, 2)[0],
                                    (float) _poseMatrix.get(i * 5 + 2, 3)[0]));
                            rtMatrix4X4.SetRow(2,
                                new Vector4((float) _poseMatrix.get(i * 5 + 3, 0)[0],
                                    (float) _poseMatrix.get(i * 5 + 3, 1)[0],
                                    (float) _poseMatrix.get(i * 5 + 3, 2)[0],
                                    (float) _poseMatrix.get(i * 5 + 3, 3)[0]));
                            rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));

                            _foundObjects.Add(new KeyValuePair<int, Matrix4x4>(objectId, rtMatrix4X4));
                        //}
                    }

                    List<IGrouping<int, KeyValuePair<int, Matrix4x4>>> foundObjetsGroups =
                        _foundObjects.GroupBy(i => i.Key).ToList();


                    for (int i = 0; i < _arObjectsInternal.Count; i++)
                    {

                        

                        IGrouping<int, KeyValuePair<int, Matrix4x4>> foundObject =
                            foundObjetsGroups.FirstOrDefault(foundObjetsGroup => foundObjetsGroup.Key == i);
                        if (foundObject == null)
                        {
                            

                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                            {
                                //                            Destroy(_arObjectsInternal[i].Instances[j]);
                                //                            _arObjectsInternal[i].Instances.RemoveAt(j);
                                _arObjectsInternal[i].Instances[j].SetActive(false);
                            }
                        }
                        else
                        {
                            // Add missing instances
                            while (_arObjectsInternal[i].Instances.Count < foundObject.Count())
                            {
                                _arObjectsInternal[i].Instances
                                    .Add(Instantiate(_arObjectsInternal[i].ArObject.ArGameObjectRoot));
                            }

                            //                        // Remove exceeding instances
                            //                        for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                            //                        {
                            ////                            Destroy(_arObjectsInternal[i].Instances[j]);
                            ////                            _arObjectsInternal[i].Instances.RemoveAt(j);
                            //                            _arObjectsInternal[i].Instances[j].SetActive(false);
                            //                        }

                            // Hide exceeding instances
                            for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= foundObject.Count(); j--)
                            {
                                //                            Destroy(_arObjectsInternal[i].Instances[j]);
                                //                            _arObjectsInternal[i].Instances.RemoveAt(j);
                                _arObjectsInternal[i].Instances[j].SetActive(false);
                            }

                            // New positions to Gameobjects
                            List<KeyValuePair<int, Matrix4x4>> foundObjectList = foundObject.ToList();
                            for (int sharedIndex = 0;
                                sharedIndex < _arObjectsInternal[i].Instances.Count &&
                                sharedIndex < foundObjectList.Count;
                                sharedIndex++)
                            {
                                GameObject gameObjectInstance = _arObjectsInternal[i].Instances[sharedIndex];
                                Matrix4x4 matrix4X4 = foundObjectList[sharedIndex].Value;

                                // Flip x and y axes for PortraitUpsideDown
                                if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                                {
                                    Vector3 localPosition = Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                                    gameObjectInstance.transform.localPosition = new Vector3(-localPosition.x,
                                        -localPosition.y, localPosition.z);
                                }
                                else
                                {
                                    gameObjectInstance.transform.localPosition =
                                        Utils.ExtractTranslationFromMatrix(ref matrix4X4);
                                }

                                gameObjectInstance.transform.localRotation =
                                    Utils.ExtractRotationFromMatrix(ref matrix4X4);
                                gameObjectInstance.SetActive(true);
                            }
                        }
                    }

                    if (!_isTracking && _foundObjects.Count > 0)
                    {
                        OnObjectDetected(_foundObjects[0].Key);
                    }
                    else if (_isTracking && _foundObjects.Count <= 0)
                    {
                        OnObjectLost();
                    }

                    _isTracking = _foundObjects.Count > 0;

                    //            Not needed anymore
                    //            if (!_isTracking && preview.texture != NatCam.Preview)
                    //            {
                    //                preview.texture = NatCam.Preview;
                    //            }

                    if (_foundObjects.Count == 0)
                    {
                       // MainMenu.SetActive(false);
                      //  ViewFinder.SetActive(true);
                        GameObject[] AreaLists = GameObject.FindGameObjectsWithTag("AreaList");
                        foreach (GameObject AreaList in AreaLists)
                        {
                            AreaList.SetActive(false);
                        }

                        GameObject[] Assets = GameObject.FindGameObjectsWithTag("Asset");
                        foreach (GameObject Asset in Assets)
                        {
                            Asset.SetActive(false);
                        }
                    }
                    else if (_foundObjects.Count > 0)
                    {
                        //MainMenu.SetActive(true);
                        //ViewFinder.SetActive(false);
                    }

                }
     
                catch (Exception e)
                {

                    Debug.Log(e.StackTrace);
                }
            }
        }

        void Update()
        {
            ProcessFrameWork();
        }

        void OnDestroy()
        {

          
            ReleaseObjects();
            _instance = null;
        }

        void ReleaseObjects()
        {

            _initialized = false;
            if (_arObjectsInternal != null)
            {
                for (int i = 0; i < _arObjectsInternal.Count; i++)
                {
                    for (int j = _arObjectsInternal[i].Instances.Count - 1; j >= 0; j--)
                    {
                        DestroyImmediate(_arObjectsInternal[i].Instances[j]);
                        _arObjectsInternal[i].Instances.RemoveAt(j);
                    }
                }
            }

            if (_nativeObject != null)
            {
                _nativeObject.Destroy();
                _nativeObject = null;
            }

            // Hide artifacts generated by screen rotation 
            if (preview != null && preview.texture != null && _frameMatrix != null)
            {
                preview.texture = null;
                preview.color = Color.black;
            }

            if (_poseMatrix != null)
            {
                _poseMatrix.release();
                _poseMatrix = null;
            }

            if (_frameMatrixPreprocessed != null)
            {
                lock (_lock)
                {
                    _frameMatrixPreprocessed.release();
                    _frameMatrixPreprocessed = null;
                }
            }

            _isTracking = false;
           
        }

        int currentObject = -1;

        public void OnObjectDetected(int id)
        {
            ViewFinder.SetActive(false);
            currentObject = id;
            ResetArea();
            if (id == 0)
            {
                MainMenu.SetActive(true);
                ClearUiBottons();
                MenuXT5.SetActive(true);

                ImageBtn.SetActive(true);
                VideoBtn.SetActive(true);
                PdbBtn.SetActive(true);
            }
            else if (id == 1)
            {
                HideXT2Areas();
                ClearUiBottons();
                ButtonsXT2Poli.SetActive(true);
                InstructionXT2.SetActive(true);
                MainMenu.SetActive(false);

                ImageBtn.SetActive(false);
                VideoBtn.SetActive(false);
                PdbBtn.SetActive(false);
            }

        }

        public void OnObjectLost()
        {
            MainMenu.SetActive(false);
            ViewFinder.SetActive(true);

            ClosePanels();
            ClearUiBottons();
            ClearTitle();
        }

        public void ClearTitle() {
            MenuXT5.SetActive(false);
            MenuXT24Poli.SetActive(false);
            MenuXT23Poli.SetActive(false);
        }

        public void ClearUiBottons() {
            ButtonsXT2Poli.SetActive(false);
            InstructionXT2.SetActive(false);
        }

        public void ViewFinderBtnClick() {
            if (currentObject == 1)
            {
                MainMenu.SetActive(false);
                ButtonsXT2Poli.SetActive(true);
                InstructionXT2.SetActive(true);
                HideXT2Areas();
            }
            else
            {
                ButtonsXT2Poli.SetActive(false);
                InstructionXT2.SetActive(false);
            }
        }

        public void HandleTitle() {
            ClearTitle();
            if (currentObject == 0) { 
                MenuXT5.SetActive(true);
            }
        }

        public void HideXT2Areas()
        {
            foreach (GameObject gameObject in _arObjectsInternal[1].Instances)
            {
                foreach (Transform transform in gameObject.transform)
                {
                    transform.gameObject.SetActive(false);
                }
            }
        }

        public string SelectedPoli;
        
        public void ShowXT2_3PoliAreas()
        {
            SelectedPoli = "3poli";
            foreach (GameObject gameObject in _arObjectsInternal[1].Instances)
            {
                foreach (Transform transform in gameObject.transform)
                {
                    if (transform.gameObject.name != "Blue4p")
                    {
                        transform.gameObject.SetActive(true);
                    }
                }
            }
        }

        public void ShowXT2_4PoliAreas()
        {
            SelectedPoli = "4poli";
            foreach (GameObject gameObject in _arObjectsInternal[1].Instances)
            {
                foreach (Transform transform in gameObject.transform)
                {
                    transform.gameObject.SetActive(true);
                }
            }
        }

        public void Xt4PoliClick()
        {
            ShowXT2_4PoliAreas();
            ClearTitle();
            MainMenu.SetActive(true);
            InstructionXT2.SetActive(false);
            MenuXT24Poli.SetActive(true);
            ButtonsXT2Poli.SetActive(false);
        }

        public void Xt3PoliClick()
        {
            ShowXT2_3PoliAreas();
            ClearTitle();
            MainMenu.SetActive(true);
            InstructionXT2.SetActive(false);
            MenuXT23Poli.SetActive(true);
            ButtonsXT2Poli.SetActive(false);
        }

        public void ResetArea()
        {
            GameObject[] Areas = GameObject.FindGameObjectsWithTag("Area");
            foreach (GameObject Area in Areas)
            {
                Area.GetComponent<OpenArea>().UnlitComponents();
            }

            GameObject[] AreaLists = GameObject.FindGameObjectsWithTag("AreaList");
            foreach (GameObject AreaList in AreaLists)
            {
                AreaList.SetActive(false);
            }

            GameObject[] Assets = GameObject.FindGameObjectsWithTag("Asset");
            foreach (GameObject Asset in Assets)
            {
                Asset.SetActive(false);
            }
        }

        public void ClosePanels()
        {
            GameObject[] AreaLists = GameObject.FindGameObjectsWithTag("AreaList");
            foreach (GameObject AreaList in AreaLists)
            {
                AreaList.SetActive(false);
            }
        }

        public class NativeArTMaxConfigurator : IBrainPadNative
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeArTMaxConfigurator()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                _nativeObjAddr = nativeCreate();
//#endif
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle,
                float verticalViewAngle)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeSetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle,
                    verticalViewAngle);
//#endif
            }

            public void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                Mat referenceImageMat;
                Mat referenceImageGreyMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                if (referenceImage.format == TextureFormat.RGBA32)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);
                }
                else if (referenceImage.format == TextureFormat.RGB24)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC3);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                    Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGB2GRAY);
                }
                else if (referenceImage.format == TextureFormat.Alpha8)
                {
                    referenceImageMat = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC1);
                    OpenCVForUnity.Utils.fastTexture2DToMat(referenceImage, referenceImageMat);
                }
                else
                {
                    Debug.LogError(
                        "Error: wrong image target fromat, the image target should be 32bit RGB with alpha, 24bit RGB or 8bit GreyScale");
//                    EditorUtility.DisplayDialog("Error: wrong reference image", "Your project is correctly setup to run BrainPad", "Ok");
                    return;
                }

//                Old handling with OpenCVForUnity.Utils.texture2DToMat
//                Mat referenceImageMat; = new Mat(referenceImage.height, referenceImage.width, CvType.CV_8UC4);
//                OpenCVForUnity.Utils.texture2DToMat(referenceImage, referenceImageMat);
//                Mat referenceImageGreyMat = new Mat(referenceImageMat.rows(), referenceImageMat.cols(), CvType.CV_8UC1);
//                Imgproc.cvtColor(referenceImageMat, referenceImageGreyMat, Imgproc.COLOR_RGBA2GRAY);

                nativeAddNewObjectToDetect(_nativeObjAddr, objectId, referenceImageGreyMat.nativeObj,
                    worldPlane.nativeObj);
//#endif
            }

            public void Init()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeInit(_nativeObjAddr);
//#endif
            }

            public void Configure()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                nativeConfigure(_nativeObjAddr);
//#endif
            }

            public void Start()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStart(_nativeObjAddr);
//#endif
            }

            public void Stop()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeStop(_nativeObjAddr);
//#endif
            }

            public int ProcessFrame(Mat imageRgba)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeProcessFrame(_nativeObjAddr, imageRgba.nativeObj);
//#else
//            return 0;
//#endif
            }

            public int GetRTMatrix(Mat poseMatrixOut)
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
                return nativeGetRTMatrix(_nativeObjAddr, poseMatrixOut.nativeObj);
//#else
//            return 0;
//#endif
            }

            public void Destroy()
            {
//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5

                nativeDestroy(_nativeObjAddr);
//#endif
            }

//#if UNITY_PRO_LICENSE || ((UNITY_ANDROID || UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR) || UNITY_5
#if !UNITY_EDITOR
            const string LIBNAME = "ArTMaxConfigurator";
#else
//        const string LIBNAME = "__Internal";
            const string LIBNAME = "Sapphire";
#endif

            [DllImport(LIBNAME)]
            private static extern IntPtr nativeCreate();

            [DllImport(LIBNAME)]
            private static extern void nativeDestroy(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeAddNewObjectToDetect(IntPtr nativeObjAddr, int objectId, IntPtr queryFrame,
                IntPtr worldPlane);

            [DllImport(LIBNAME)]
            private static extern int nativeInit(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeSetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov,
                float vFov);

            [DllImport(LIBNAME)]
            private static extern int nativeConfigure(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStart(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeStop(IntPtr nativeObjAddr);

            [DllImport(LIBNAME)]
            private static extern int nativeProcessFrame(IntPtr nativeObjAddr, IntPtr rImage);

//        [DllImport(LIBNAME)]
//        private static extern int nativeGetHomographies(IntPtr nativeObjAddr, IntPtr rHomographiesOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetRTMatrix(IntPtr nativeObjAddr, IntPtr rRTMatrixOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetReferenceObjects(IntPtr nativeObjAddr, IntPtr rRefObjsOut);

            [DllImport(LIBNAME)]
            private static extern int nativeGetImage(IntPtr nativeObjAddr, IntPtr rImageOut);

            [DllImport(LIBNAME)]
            private static extern int nativeSetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}
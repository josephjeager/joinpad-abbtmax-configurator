﻿using OpenCVForUnity;
using UnityEngine;

namespace BrainPad.Core
{
    public interface IBrainPadNative
    {
        void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle, float verticalViewAngle);
        void AddNewObjectToDetect(int objectId, Texture2D referenceImage, Mat worldPlane);
    }
}
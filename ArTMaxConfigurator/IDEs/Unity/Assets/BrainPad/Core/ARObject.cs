﻿using System;
using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace BrainPad.Core
{
    [Serializable]
    public class ArObject
    {
        public GameObject ArGameObjectRoot;
        public Texture2D ReferenceImage;
        public float ReferenceImageWidth;
        public float ReferenceImageHeight;

        public string ReferenceImageName;
    }

    public class ArObjectInternal
    {
        public ArObject ArObject;
        public List<GameObject> Instances = new List<GameObject>();
        private Mat _worldPlane = new Mat(4, 3, CvType.CV_32F);

        public void SetupWorldPlane( /*Enums.MeasureUnits measureUnit*/)
        {
            float referenceImageWidthScaled = ArObject.ReferenceImageWidth;
            float referenceImageHeightScaled = ArObject.ReferenceImageHeight;

//            switch (ArObject.MeasureUnit)
//            {
//                case Enums.MeasureUnits.mm:
//                    referenceImageWidthScaled *= 1000;
//                    referenceImageHeightScaled *= 1000;
//                    break;
//                case Enums.MeasureUnits.cm:
//                    referenceImageWidthScaled *= 100;
//                    referenceImageHeightScaled *= 100;
//                    break;
//                case Enums.MeasureUnits.dm:
//                    referenceImageWidthScaled *= 10;
//                    referenceImageHeightScaled *= 10;
//                    break;
//            }

            _worldPlane.put(0, 0, -referenceImageWidthScaled / 2);
            _worldPlane.put(0, 1, 0f);
            _worldPlane.put(0, 2, referenceImageHeightScaled / 2);

            _worldPlane.put(1, 0, -referenceImageWidthScaled / 2);
            _worldPlane.put(1, 1, 0f);
            _worldPlane.put(1, 2, -referenceImageHeightScaled / 2);

            _worldPlane.put(2, 0, referenceImageWidthScaled / 2);
            _worldPlane.put(2, 1, 0f);
            _worldPlane.put(2, 2, -referenceImageHeightScaled / 2);

            _worldPlane.put(3, 0, referenceImageWidthScaled / 2);
            _worldPlane.put(3, 1, 0f);
            _worldPlane.put(3, 2, referenceImageHeightScaled / 2);
        }

        public Mat GetWorldPlane()
        {
            return _worldPlane;
        }
    }
}
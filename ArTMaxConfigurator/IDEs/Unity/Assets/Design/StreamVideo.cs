﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour {

    public RawImage rawImage;
    public VideoPlayer videoPlayer; 

    //public VideoClip[] videoClips;
    //private int videoClipIndex;

    //private Renderer playButtonRenderer;
    //public Button playButtonMaterial;
    //public Material pauseButtonMaterial;
    public Button PlayBtn;
    public Button PauseBtn;

    public Text currentSeconds;
    public Text currentMinutes;
    public Text totalMinutes;
    public Text totalSeconds;

    public Image progressBar;

    //public float prova = 30.0f;
    private float current;


    /*void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }*/
    
    /*void Start()
    {
        videoPlayer.clip = videoClips[0];
    }*/

   

    /*public void SetNextClip()
    {
        videoClipIndex++;

        if (videoClipIndex >= videoClips.Length)
        {
            videoClipIndex = videoClipIndex % videoClips.Length;
        }
        videoPlayer.clip = videoClips[videoClipIndex];
        videoPlayer.Play();
    }*/

    /*public void PlayPause()
    {
        if (videoPlayer.isPlaying)
        {
            
            videoPlayer.Pause();
            //playButtonRenderer.material = playButtonMaterial;


        }
        else {
            videoPlayer.Play();
            //playButtonRenderer.material = pauseButtonMaterial;
        }
    }*/

    // Use this for initialization
    public void PlayButton () {
       
        StartCoroutine(PlayVideo());
        //setCurrentTimeUI();
    }
	
    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
        rawImage.texture = videoPlayer.texture;
        
        videoPlayer.Play();
        setTotalTimeUI();
        progressBar.fillAmount = 0f;
        
        //audioSource.Play();  
    }


    void Update()
    {
        if (videoPlayer.isPlaying)
        {
            setCurrentTimeUI();
            //setProgressBar();
            float currentFloat = (float)videoPlayer.time;
            float totalFloat = (float)videoPlayer.clip.length;
           
            progressBar.fillAmount = currentFloat / totalFloat;
        }
    }


    public void setCurrentTimeUI()
    {
        string minutes = Mathf.Floor((int) videoPlayer.time / 60).ToString("00");
        string seconds = ((int) videoPlayer.time % 60).ToString("00");
       

        currentMinutes.text = minutes;
        currentSeconds.text = seconds;

        
    }

    public void setTotalTimeUI()
    {
        string minutes = Mathf.Floor((int)videoPlayer.clip.length / 60).ToString("00");
        string seconds = ((int)videoPlayer.clip.length % 60).ToString("00");
        

        totalMinutes.text = minutes;
        totalSeconds.text = seconds;
    }

  


}

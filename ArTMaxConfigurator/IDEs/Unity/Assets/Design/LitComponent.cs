﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitComponent : MonoBehaviour
{
    public Color LitColor;
    public Color UnlitColor;

    public void Lit()
    {
        gameObject.GetComponent<SpriteRenderer>().color = LitColor;
    }

    public void Unlit()
    {
        gameObject.GetComponent<SpriteRenderer>().color = UnlitColor;
    }
}

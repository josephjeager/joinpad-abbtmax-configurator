﻿using UnityEngine;

public class ResetArea : MonoBehaviour
{

    public void resetArea()
    {
        GameObject[] Areas = GameObject.FindGameObjectsWithTag("Area");
        foreach (GameObject Area in Areas)
        {
            Area.GetComponent<OpenArea>().UnlitComponents();
        }

        GameObject[] AreaLists = GameObject.FindGameObjectsWithTag("AreaList");
        foreach (GameObject AreaList in AreaLists)
        {
            AreaList.SetActive(false);
        }

        GameObject[] Assets = GameObject.FindGameObjectsWithTag("Asset");
        foreach (GameObject Asset in Assets)
        {
            Asset.SetActive(false);
        }
 

    }
  
}

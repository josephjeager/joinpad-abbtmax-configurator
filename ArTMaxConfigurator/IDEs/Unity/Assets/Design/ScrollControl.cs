﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollControl : MonoBehaviour {

    public ScrollRect scrollRect1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ScrollTo(int page)
    {
        Canvas.ForceUpdateCanvases();
        scrollRect1.horizontalScrollbar.value = page;
        Canvas.ForceUpdateCanvases();
    }
}

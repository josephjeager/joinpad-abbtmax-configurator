﻿using BrainPad.ArTMaxConfigurator;
using UnityEngine;

public class OpenArea : MonoBehaviour
{
    public GameObject ActiveArea;
    public GameObject[] Components;
    public Color LitColor;
    public Color UnlitColor;

    public GameObject RCCoilMenuEntry;
    public GameObject RCCoilMenuEntry2;

    private void OnMouseDown ()
    {
        GameObject[] Areas = GameObject.FindGameObjectsWithTag("Area");
        foreach (GameObject Area in Areas)
        {
            Area.GetComponent<OpenArea>().UnlitComponents();
        }

        GameObject[] Assets = GameObject.FindGameObjectsWithTag("Asset");
        foreach (GameObject Asset in Assets)
        {
            Asset.SetActive(false);
        }

        if (ActiveArea.activeInHierarchy)
        {
            ActiveArea.SetActive(false);
            UnlitComponents();
        }
        else
        {
            LitComponents();
            GameObject[] AreaLists = GameObject.FindGameObjectsWithTag("AreaList");
            foreach (GameObject AreaList in AreaLists)
            {
                AreaList.SetActive(false);
            }
            ActiveArea.SetActive(true);

            if (RCCoilMenuEntry != null)
            {
                if (ArTMaxConfigurator._instance.SelectedPoli == "4poli")
                {
                    RCCoilMenuEntry.SetActive(true);
                }
                else
                {
                    RCCoilMenuEntry.SetActive(false);
                }
            }
            
            if (RCCoilMenuEntry2 != null)
            {
                if (ArTMaxConfigurator._instance.SelectedPoli == "4poli")
                {
                    RCCoilMenuEntry2.SetActive(true);
                }
                else
                {
                    RCCoilMenuEntry2.SetActive(false);
                }
            }
        }
    }

    public void LitComponents()
    {
        foreach(GameObject Component in Components)
        {
            Component.GetComponent<SpriteRenderer>().color = LitColor;
        }
    }

    public void UnlitComponents()
    {
        foreach (GameObject Component in Components)
        {
            Component.GetComponent<SpriteRenderer>().color = UnlitColor;
        }
    }

    public bool IsLit()
    {
        return gameObject.GetComponent<SpriteRenderer>().color == LitColor;
    }
}

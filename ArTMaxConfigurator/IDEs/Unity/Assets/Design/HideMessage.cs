﻿using BrainPad.ArTMaxConfigurator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideMessage : MonoBehaviour
{
	void Update ()
    {
        gameObject.SetActive(!ArTMaxConfigurator._instance._isTracking);

    }
}

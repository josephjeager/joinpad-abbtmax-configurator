﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitAssetComponent : MonoBehaviour
{
    public string[] RelatedComponents;

    public void LitRelatedComponents()
    {
        GameObject[] Components = GameObject.FindGameObjectsWithTag("Component");
        foreach (GameObject Component in Components)
        {
            Component.GetComponent<LitComponent>().Unlit();
        }

        foreach (string RelatedComponent in RelatedComponents)
        {
            GameObject.Find(RelatedComponent).GetComponent<LitComponent>().Lit();
        }
    }
}

#include <app/ArTMaxConfigurator.h>
#include "utils/Utils.h"
#ifdef _ANDROID

#include <android/log.h>

#endif

#ifdef _LINUX
#define DllExport 
#endif
#ifdef _ANDROID
#define DllExport 
#endif
#ifdef _WINDOWS
#define DllExport  __declspec( dllexport )
#endifllExport  __declspec( dllexport )
#endif

#ifdef __cplusplus
extern "C" {
#endif

DllExport long nativeCreate();

DllExport void nativeDestroy(long thiz);

DllExport int nativeInit(long thiz);
DllExport int nativeAddNewObjectToDetect(long thiz, int objectId, long queryFrame, long worldPlane);
DllExport int nativeSetCameraParams(long thiz, int _width, int _height, float _hFOV, float _vFOV);
DllExport int nativeConfigure(long thiz);
DllExport int nativeStart(long thiz);
DllExport int nativeStop(long thiz);

DllExport int nativeProcessFrame(long thiz, long rImage);

DllExport int nativeGetRTMatrix(long thiz, long rRTMatrixOut);
DllExport int nativeGetReferenceObjects(long thiz, long rRefObjsOut);

DllExport int nativeGetImage(long thiz, long rImageOut);
DllExport int nativeSetImage(long thiz, long rImage);


#ifdef __cplusplus
}
#endif

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArTMaxConfigurator.cpp
 * Author: marzorati
 * 
 * Created on June 21, 2017, 3:48 PM
 */

#include "ArTMaxConfigurator.h"


ArTMaxConfigurator::ArTMaxConfigurator(std::string _configString, int _loadType)// : camera_(800, 800, 320, 240, 50, 50, 640, 480)
{


    LOGD("ArTMaxConfigurator::Configure");

    if (!_configString.empty())
    {
        LOGD("ArTMaxConfigurator::Configure: Open");

        if (_loadType==FROM_FILE)
        {
            cv::String fileToOpen = _configString;
            configFile_.open(fileToOpen, cv::FileStorage::READ);
            LOGD("ArTMaxConfigurator::Configure from FILE: isOpen: %d", configFile_.isOpened());
            fileNode_ = configFile_.getFirstTopLevelNode();

        }
        else if (_loadType==FROM_STRING)
        {
            cv::String fileToOpen = _configString;
            configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
            LOGD("ArTMaxConfigurator::Configure from YAML String: isOpen: %d", configFile_.isOpened());
            LOGD("ArTMaxConfigurator::Configure from YAML String: YAML String: %s", _configString.c_str());

            //carico la sezione corretta
            LOGD("ArTMaxConfigurator::Configure from YAML String: YAML String: %s", configFile_.getFirstTopLevelNode().name().c_str());

            fileNode_ = configFile_["artmaxconfigurator"];


            yamlString_=_configString;
        }

    }
    else
    {
        LOGD("ArTMaxConfigurator::BuildDefaultConfigString()");
        _configString=BuildDefaultConfigString();
        cv::String fileToOpen = _configString;
        configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
        LOGD("ArTMaxConfigurator::Configure from YAML String: isOpen: %d", configFile_.isOpened());
        LOGD("ArTMaxConfigurator::Configure from YAML String: YAML String: %s", _configString.c_str());

        //carico la sezione corretta
        LOGD("ArTMaxConfigurator::Configure from YAML String: YAML String: %s", configFile_.getFirstTopLevelNode().name().c_str());

        fileNode_ = configFile_["artmaxconfigurator"];


        yamlString_= _configString;
    }


//    configFile_.release();

    isTracking_ = false;
    isInit_ = false;
    isConfigurated_ = false;
    isStarted_ = false;
    objectDetected_=false;
    isValidRT_=false;
    isValidH_=false;
    enableCheckObjectPose_=true;
//    isValidObj_=false;

    numFrame_ = 0;


}


ArTMaxConfigurator::~ArTMaxConfigurator()
{

    multiObjDetectorTrackerAndPose_.Stop();
}


std::string ArTMaxConfigurator::BuildDefaultConfigString()
{
    return DEFAULT_CONFIG_STRING;

}


int ArTMaxConfigurator::Configure()
{
    int ret = 0;

    LOGD("ArTMaxConfigurator::Configure() -> Configuring...");

    //    multiObjDetectorAndTracker_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/MultiObjectDetectorAndTracker.yaml");
    //    detectorObj_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFeatureDetector.yaml");
    //    descriptorObj_.Setup();//("/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFeatureDescriptor.yaml");
    //    keypoint_matcher_.Setup();//"/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFlannFeatureMatcher.yaml");
    //    objDetector_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/ObjectDetector.yaml");
    //    multiTracker.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/MultiTracker.yaml");




    cv::String strTmp;
    string str;
    fileNode_["multiObjDetectorTrackerAndPoseConfigFilename"] >> strTmp;
    str=strTmp;
    LOGD("ArTMaxConfigurator:: %s", str.c_str());
    multiObjDetectorTrackerAndPose_.Configure(str,yamlString_);
    //    configFile_["featureDetectorConfigFilename"] >> str;
    //    LOGD("BrainPadRecognizerX:: %s", str.c_str());
    //    detectorObj_.Configure(str);
    //    configFile_["featureDescriptorConfigFilename"] >> str;
    //    LOGD("BrainPadRecognizerX:: %s", str.c_str());
    //    descriptorObj_.Configure(str); //Setup();
    //    configFile_["matcherConfigFilename"] >> str;
    //    LOGD("BrainPadRecognizerX:: %s", str.c_str());
    //    keypoint_matcher_.Configure(str);
    fileNode_["objectDetectorConfigFilename"] >> strTmp;
    str=strTmp;
    LOGD("ArTMaxConfigurator:: %s", str.c_str());
    objDetector_.Configure(str,yamlString_);
    fileNode_["multiObjectTrackerConfigFilename"] >> strTmp;
    str=strTmp;
    LOGD("ArTMaxConfigurator:: %s", str.c_str());
    multiTracker_.Configure(str,yamlString_);
    fileNode_["poseEstimatorConfigFilename"] >> strTmp;
    str=strTmp;
    LOGD("ArTMaxConfigurator:: %s", str.c_str());
    poseEstimator_.Configure(str,yamlString_);

    fileNode_["enableCheckObjectPose"]>>enableCheckObjectPose_;

    fileNode_["angleToll"]>>angleToll_;
    fileNode_["toll"]>>toll_;
    fileNode_["maxDistance"]>>maxDistance_;






    //    detector_.SetFeatureDetector(&detectorObj_);
    //    detector_.SetFeatureDescriptor(&descriptorObj_);
    //
    //    matcher_.SetFeatureMatcher(&keypoint_matcher_);
    //
    //    objDetector_.SetDetector(&detector_);
    //    objDetector_.SetMatcher(&matcher_);

    ret = multiObjDetectorTrackerAndPose_.SetObjectDetector(&objDetector_);
    if (ret == 0)
        ret = multiObjDetectorTrackerAndPose_.SetMultiTracker(&multiTracker_);
    if (ret==0)
        ret=multiObjDetectorTrackerAndPose_.SetPoseEstimator(&poseEstimator_);

    if (ret == 0)
        ret = multiObjDetectorTrackerAndPose_.Init();

    if (ret == 0)
        isConfigurated_ = true;

    LOGD("ArTMaxConfigurator::Configure() -> Done: %d", ret);

    return ret;
}

int ArTMaxConfigurator::SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV,double _vFOV,double _k1,double _k2,double _k3,double _p1,double _p2)
{

    if (_fx==0 && _fy==0 && _cx==0 && _cy==0)
    {
        _cx = _width / 2.0;
        _cy = _height / 2.0;
        _fx = FOV2FOCAL(_width, DEG2RAD(_hFOV));
        _fy = FOV2FOCAL(_height, DEG2RAD(_vFOV));
    }




    multiTracker_.SetCameraDevice(_fx,_fy,_cx,_cy,_width,_height,_hFOV,_vFOV);
    poseEstimator_.SetCameraDevice(_fx,_fy,_cx,_cy,_width,_height,_hFOV,_vFOV);

//    std::string yamlStr;
//    yamlStr="camera640x480:\n"
//            "  fx: 800\n"
//            "  fy: 800\n"
//            "  cx: 320\n"
//            "  cy: 240\n"
//            "  k1: 0\n"
//            "  k2: 0\n"
//            "  k3: 0\n"
//            "  hFOV: 50\n"
//            "  vFOV: 50\n"
//            "  frameWidth: 640\n"
//            "  frameHeight: 480";
//
//    std::string sectionName="camera640x480";
//    camera_.Configure(sectionName,yamlStr);




//    std::istringstream str(yamlString_);
//    std::stringstream yamlOut;
//    string line;
//    while (getline(str, line))
//    {
//        if (line.find("fx") != string::npos)
//        {
//            yamlOut<<"\tfx: "<<_fx;
//        }
//        else if (line.find("fy") != string::npos)
//        {
//            yamlOut<<"\tfy: "<<_fy;
//        }
//        else if (line.find("cx") != string::npos)
//        {
//            yamlOut<<"\tcx: "<<_cx;
//        }
//        else if (line.find("cy") != string::npos)
//        {
//            yamlOut<<"\tcy: "<<_cy;
//        }
//        else if (line.find("k1") != string::npos)
//        {
//            yamlOut<<"\tk1: "<<_k1;
//        }
//        else if (line.find("k2") != string::npos)
//        {
//            yamlOut<<"\tk2: "<<_k2;
//        }
//        else if (line.find("k3") != string::npos)
//        {
//            yamlOut<<"\tk3: "<<_k3;
//        }
//        else if (line.find("width") != string::npos)
//        {
//            yamlOut<<"\twidth: "<<_width;
//        }
//        else if (line.find("height") != string::npos)
//        {
//            yamlOut<<"\theight: "<<_height;
//        }
//        else if (line.find("hFOV") != string::npos)
//        {
//            yamlOut<<"\thFOV: "<<_hFOV;
//        }
//        else if (line.find("vFOV") != string::npos)
//        {
//            yamlOut<<"\tvFOV: "<<_vFOV;
//        }
//        else
//            yamlOut<<line;
//    }
//
//    yamlString_=yamlOut.str();
//
//    cv::String fileToOpen = yamlString_;
//    configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
//
//    fileNode_ = configFile_["brainpadrecognizerxplus"];

    LOGD("ArTMaxConfigurator:: AAAAAAAAAAAAAA: %s",yamlString_.c_str());

    return 0;
}


int ArTMaxConfigurator::Init()
{
    int ret = 0;

    LOGD("ArTMaxConfigurator::Init() -> Initializing...");

    if (isConfigurated_ )
    {

        if (!fileNode_["numObjectsToDetect"].empty())
        {
            numFrame_ = 0;
            int numObjs;
            fileNode_["numObjectsToDetect"] >> numObjs;

            LOGD("ArTMaxConfigurator::NumObjectsToDetect: %d", numObjs);

            /************************
             *  COMPUTE TARGET OBJECTS KEYPOINTS
             ************************/

            int objID;
            for (int numObj = 0; numObj < numObjs; ++numObj)
            {

                //Load Image

                Image imgObj, imgObj_tmp;

                std::string fileNameObj, filenameObjConfig;

                std::string objName;

                fileNode_["objectModelFilenames"][numObj] >> filenameObjConfig; //"/media/marzorati/Data_Windows/Android/StudioProjects/SapphireProject/IDEs/NetBeans/Sapphire/res/object.jpg"; //"/home/marzorati/Documents/projects/ABB_datasets/ABB_objects_selected/Type2/IMG_7443_crop_scaled.JPG"; //"/home/marzorati/Documents/projects/Sapphire/res/object.jpg"; //../../res/Emax2_4.jpg";

                cv::FileStorage configFileObj;
                configFileObj.open(filenameObjConfig, cv::FileStorage::READ);

                LOGD("ArTMaxConfigurator::ObjectModelFilenames: %s", filenameObjConfig.c_str());
                if (configFileObj.isOpened())
                {

                    configFileObj["objectModelFilename"] >> fileNameObj;
                    configFileObj["objectName"] >> objName;
                    configFileObj["objectID"] >> objID;
                    LOGD("ArTMaxConfigurator::ObjectToDetect %s ID: %d Filename: %s ", objName.c_str(), objID, fileNameObj.c_str());


                    //NEW PART

                    std::vector<Point3D<float> > shape3D;
                    std::vector<std::vector<float> > shapes3DXYZ;

                    configFileObj["shape3D"] >> shapes3DXYZ;

                    int numVertexCoord = 0;
                    for (int numPoint = 0; numPoint < shapes3DXYZ.size(); ++numPoint)
                    {
                        numVertexCoord = 0;
                        //                std::vector<Point3D<float> > shape3D;
                        for (int m = 0; m < shapes3DXYZ[numPoint].size(); m = m + 3)
                        {
                            Point3D<float> point3D(shapes3DXYZ[numPoint][m], shapes3DXYZ[numPoint][m + 1], shapes3DXYZ[numPoint][m + 2]);
                            shape3D.push_back(point3D);
                        }
                        //                shapes3D.push_back(shape3D);
                    }

                    //SET OBJECT TEXTURE POINTS 2D
//                std::vector< Shape2D > shapes2D;
                    std::vector<std::vector<Point2D<float> > > shapes2D;
                    std::vector<std::vector<float> > shapes2DXY;

                    configFileObj["shape2D"] >> shapes2DXY;


                    //TEMP
//                Shape2D shape2D2;
                    //END TEMP

                    for (int numFace = 0; numFace < shapes2DXY.size(); ++numFace)
                    {
                        numVertexCoord = 0;
                        vector<Point2D<float> > shape2D;
                        for (int m = 0; m < shapes2DXY[numFace].size(); m = m + 2)
                        {
                            Point2D<float> point2D(shapes2DXY[numFace][m], shapes2DXY[numFace][m + 1]);
                            shape2D.push_back(point2D);
                        }

                        //TEMP
//                    shape2D2 = shape2D;
                        //END TEMP

                        shapes2D.push_back(shape2D);
                    }


                    utils::LoadImageFromFile(fileNameObj, cv::IMREAD_UNCHANGED, imgObj_tmp);

                    LOGD("ArTMaxConfigurator::Loaded Image: %s", fileNameObj.c_str());

                    if (imgObj_tmp.GetHeight() * imgObj_tmp.GetWidth() <= 0)
                    {
                        LOGD("ArTMaxConfigurator::Image %s is empty or cannot be found\n", fileNameObj.c_str());
                        continue;
                    }

                    imgObj = imgObj_tmp;


                    std::vector<std::vector<int> > facesIdxs;
                    configFileObj["faces"] >> facesIdxs;

//                //CREATE OBJECT MESH
//                LOGD("BrainPadRecognizerXPlus:: Adding mesh");
//                Mesh meshObj(objID, imgObj, shape3D);
//
//                LOGD("BrainPadRecognizerXPlus:: Adding %d faces", (int)facesIdxs.size());
//                for (int numFace = 0; numFace < facesIdxs.size(); ++numFace)
//                {
//                    meshObj.AddFace(facesIdxs[numFace], shapes2D[numFace]);
//                }
//
//                Patch2D imgPatch(meshObj.GetUniqueObjId(), imgObj, shape2D2.GetShape()); //, objCenterX, objCenterY, shape3D);
//
//                refObjs_.push_back(imgPatch);
//
//
//                meshRefObjs_.insert(pair<int, Mesh> (objID, meshObj));
//
//                refObjName_[objID] = Info(objName);



                    AddNewObjectToDetect(objID, shape3D, facesIdxs, shapes2D, imgObj, objName);

                }
                else
                {
                    std::cout << "Error: Object Config file doesn't exist" << endl;
                }
            }

        }
        //        map<int,string>::iterator it;
        //        for (it=refObjName_.begin();it!=refObjName_.end();it++)
        //        {
        //            LOGD("BrainPadRecognizerX::Adding Ref Object: %s ID: %d",it->second.c_str(),it->first);
        //        }

        LOGD("ArTMaxConfigurator::Init() -> refObjs_.size() %d ",refObjs_.size());
        LOGD("ArTMaxConfigurator::Init() -> meshRefObjs_.size() %d ",meshRefObjs_.size());


        ret = multiObjDetectorTrackerAndPose_.AddReferenceObjects(refObjs_, meshRefObjs_);

        if (ret == 0)
        {
            isInit_ = true;
        }
    }

    LOGD("ArTMaxConfigurator::Init() -> Done: %d", ret);


    return ret;
}


int ArTMaxConfigurator::AddNewObjectToDetect(int _objID,std::vector<Point3D<float> > & _shape3D,std::vector< std::vector<int> >& _facesIdxs,std::vector< std::vector<Point2D<float> > >& _shapes2D,Image& _imgObj,string _objName)
{
    int ret = 0;

    LOGD("ArTMaxConfigurator::Init() -> Initializing...");

    if (isConfigurated_)
    {

        numFrame_ = 0;

        std::vector< Shape2D > shapes2D;
        Shape2D shape2D2;

        LOGD("ArTMaxConfigurator::Init() -> Faces num %d", _shapes2D.size());

        for (int numFace = 0; numFace < _shapes2D.size(); numFace++)
        {
            Shape2D shape2D;
            for (int m = 0; m < _shapes2D[numFace].size(); m++)
            {
                Point2D<float> point2D(_shapes2D[numFace][m].x_, _shapes2D[numFace][m].y_);
                shape2D.AddVertex2D(point2D);


                LOGD("ArTMaxConfigurator::Init() -> Points 2D num %d %f %f",numFace, point2D.x_, point2D.y_);
                LOGD("ArTMaxConfigurator::Init() -> Points 3D num %d %f %f %f",numFace, _shape3D[m].x_, _shape3D[m].y_,_shape3D[m].z_);

            }
            shape2D2 = shape2D;
            shapes2D.push_back(shape2D);
        }

        Mesh meshObj(_objID, _imgObj, _shape3D);

        for (int numFace = 0; numFace < _facesIdxs.size(); ++numFace)
        {

            LOGD("ArTMaxConfigurator::Init() -> _facesIdxs[numFace] %d %d %d %d",_facesIdxs[numFace][0],_facesIdxs[numFace][1],_facesIdxs[numFace][2],_facesIdxs[numFace][3]);

            meshObj.AddFace(_facesIdxs[numFace], shapes2D[numFace]);
        }

//        meshObj.AddFace(_facesIdxs, shape2D);

        LOGD("ArTMaxConfigurator::Init() -> Patch");
        Patch2D imgPatch(meshObj.GetUniqueObjId(), _imgObj, shape2D2.GetShape()); //, objCenterX, objCenterY, shape3D);
        LOGD("ArTMaxConfigurator::Init() -> imgPatch");

        refObjs_.push_back(imgPatch);
        LOGD("ArTMaxConfigurator::Init() -> insert");

        meshRefObjs_.insert(pair<int, Mesh> (_objID, meshObj));
        LOGD("ArTMaxConfigurator::Init() -> ObjID %d",_objID);

        Info info(_objName);
        refObjName_.insert(pair<int,Info>(_objID,info));
        LOGD("ArTMaxConfigurator::Init() -> ObjName %s",_objName.c_str());

//        ret = multiObjDetectorTrackerAndPose_.AddReferenceObjects(refObjs_, meshRefObjs_);
//
//        if (ret == 0)
//        {
//            isInit_ = true;
//        }

    }

    LOGD("ArTMaxConfigurator::Init() -> Done: %d", ret);


    return ret;
}

int ArTMaxConfigurator::Stop()
{
    LOGD("ArTMaxConfigurator::Stop() -> Stopping...");
    int ret = 0;
    ret = multiObjDetectorTrackerAndPose_.Stop();

    /*
    string fileNameScene="/storage/emulated/0/projects/BrainPadRecognizerX/videos/VID_20170904_145320.mp4";
    video_.open(fileNameScene);

    if (video_.isOpened()) {
        ret=0;
    } else
        ret=-1;
     */
    LOGD("ArTMaxConfigurator::Stop() -> Done: %d", ret);
    return ret;

}

int ArTMaxConfigurator::Start()
{
    LOGD("ArTMaxConfigurator::Start() -> Starting...");
    int ret = 0;
    ret = multiObjDetectorTrackerAndPose_.Start();

    /*
    string fileNameScene="/storage/emulated/0/projects/BrainPadRecognizerX/videos/VID_20170904_145320.mp4";
    video_.open(fileNameScene);

    if (video_.isOpened()) {
        ret=0;
    } else
        ret=-1;
     */
    LOGD("ArTMaxConfigurator::Start() -> Done: %d", ret);
    return ret;

}

bool ArTMaxConfigurator::CheckObjectPose(Matrix& _rtW_C,double _angleToll,double _toll,double _maxDistance)
{
    bool ret=false;
    //check position
    double nx = _rtW_C.at<double>(0, 0);
    double ny =  _rtW_C.at<double>(1, 0);
    double nz =  _rtW_C.at<double>(2, 0);
    double sz =  _rtW_C.at<double>(2, 1);
    double az =  _rtW_C.at<double>(2, 2);

    double sy = sqrt(nx * nx + ny * ny);

    double phi = atan2(sz,az); //atan2(sinTheta*ax-cosTheta*ay,cosTheta*sy-sinTheta*sx);
    double gamma = atan2(-nz, sy);
    double theta = atan2(ny, nx);
    double x=_rtW_C.at<double>(0,3);
    double y=_rtW_C.at<double>(1,3);
    double z=_rtW_C.at<double>(2,3);

//    Matrix xO_C(4,1,CV_64F);
//    xO_C.at<double>(0,0)=INIT_POSE_PHI;
//    xO_C.at<double>(1,0)=INIT_POSE_GAMMA;
//    xO_C.at<double>(2,0)=theta;
//    xO_C.at<double>(3,0)=x;
//    xO_C.at<double>(4,0)=y;
//    xO_C.at<double>(5,0)=z;
//
//    Matrix rtC_O=(cv::Mat)(utils::CreateRTMatrix(xO_C).inv());
//    Matrix rtW_O=(cv::Mat)(_rtW_C*rtC_O);
//
//    Matrix xW_O=utils::CreateRTVector(rtW_O);

    LOGD("ArTMaxConfigurator::Pose %f %f %f %f %f %f", phi, gamma, theta,x,y,z);

    LOGD("ArTMaxConfigurator::Pose Diff %f<%f %f<%f %f<%f %f<%f %f<%f ", fabs(phi + INIT_POSE_PHI),_angleToll, fabs(gamma + INIT_POSE_GAMMA),_angleToll, fabs(x/z)+INIT_POSE_X,_toll,fabs(y/z)+INIT_POSE_Y,_toll,z,_maxDistance);

    if (fabs(phi + INIT_POSE_PHI) < _angleToll && fabs(gamma + INIT_POSE_GAMMA) < _angleToll && fabs(x/z)+INIT_POSE_X<_toll && fabs(y/z)+INIT_POSE_Y<_toll && z<_maxDistance) {
        LOGD("ArTMaxConfigurator::Pose Check OK");
        ret = true;
    }
    else {
        LOGD("ArTMaxConfigurator::Pose Check FAILED");
        ret = false;
    }
    return ret;
}

int ArTMaxConfigurator::ProcessFrame(Image& _frame)
{
    int ret = 0;
    LOGD("ArTMaxConfigurator::ProcessFrame() -> Processing Frame...");

    if (isConfigurated_)
    {
        int numProcessedFrame = 0;

        numFrame_++;

        if (_frame.GetHeight() * _frame.GetWidth() <= 0)
        {
            LOGD("ArTMaxConfigurator::Image is empty or cannot be found");
            return -1;
        }
        else
        {

            imgScene_ = _frame;
            cv::cvtColor(_frame.GetImageData(), imgSceneOut_.GetImageData(), cv::COLOR_GRAY2RGBA, 4);

            LOGD("ArTMaxConfigurator::ImageType %d %d", imgScene_.GetImageData().type(),imgScene_.GetImageData().rows);

            isValidRT_=false;
            isValidH_=false;
//            isValidObj_=false;
            ret = multiObjDetectorTrackerAndPose_.ProcessFrame(imgScene_);

            multimap<int, Matrix> rtMatrixes = multiObjDetectorTrackerAndPose_.GetTransforms(isValidRT_);
//            multimap<int,Patch2D> detectedObjects=multiObjDetectorTrackerAndPose_.GetImageObjects(isValidObj_);
            multimap<int,Matrix> homographies=multiObjDetectorTrackerAndPose_.GetHomographies(isValidH_);

            multimap<int, Matrix>::iterator itRT;
            multimap<int, Matrix>::iterator itH;
//            multimap<int, Patch2D>::iterator itObj;
            LOGD("ArTMaxConfigurator::isValidRT: %d isValidH: %d",isValidRT_,isValidH_);

            //only one object must be detected
            if (isValidRT_  && isValidH_ && rtMatrixes.size()==1)
            {
                itRT=rtMatrixes.begin();
//                itObj=detectedObjects.begin();
                itH=homographies.begin();



                if (!objectDetected_)
                {
                    if (!enableCheckObjectPose_  || CheckObjectPose(itRT->second,angleToll_,toll_,maxDistance_))
                    {
                        LOGD("ArTMaxConfigurator::ImageType %d %d",imgScene_.GetImageData().type(),imgScene_.GetImageData().rows);
                        objectDetected_ = true;
//                        detectedObject_=*itObj;
                        detectedHomography_=*itH;
                        detectedRT_=*itRT;
                        LOGD("ArTMaxConfigurator::Data Copied");

                        ret=CheckDeviceType(homographies,_frame);

                        LOGD("ArTMaxConfigurator::Data 4");
#ifdef _DRAW
                        vector<cv::KeyPoint> queryKeyPointsOCV, trainKeyPointsOCV;
        //                vector<cv::DMatch> matchesOCV;

                        utils::ConvertToOCV(objDetector_.queryDetector_.GetKeyPoints(), queryKeyPointsOCV);
                        utils::ConvertToOCV(objDetector_.trainDetector_.GetKeyPoints(), trainKeyPointsOCV);
        //                utils::ConvertToOCV(objDetector_.matcher_.GetMatches()[0], matchesOCV);
        //                cv::drawMatches(imgSceneOut_.GetImageData(), queryKeyPointsOCV, imgSceneOut_.GetImageData(), trainKeyPointsOCV,
        //                                matchesOCV, imgSceneOut_.GetImageData(), cv::Scalar::all(-1), cv::Scalar::all(-1),
        //                                vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                        cv::Mat tmp = cv::Mat(_frame.GetImageData().rows, _frame.GetImageData().cols, CV_8UC1);
                        cv::drawKeypoints(_frame.GetImageData(), queryKeyPointsOCV, tmp, cv::Scalar(0, 255, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
                        cv::drawKeypoints(tmp, trainKeyPointsOCV, imgSceneOut_.GetImageData(), cv::Scalar(255, 255, 0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);


        //                int colorR = 255 / clusters.size();
        //                int colorB = 255 / clusters.size();
        //                for (it = clusters.begin(); it != clusters.end(); it++)
        //                {
        //                    for (int j = 0; j < it->second.size(); j++)
        //                    {
        //                        circle(img_matches, cv::Point(it->second[j].first.x_, it->second[j].first.y_), 15, cv::Scalar(colorB * (it->first + 1), 0, colorR * (it->first) + 1), -1);
        //                    }
        //                }
#endif
                    } else {
                        multiObjDetectorTrackerAndPose_.RemoveObject(itRT->first);
                        objectDetected_=false;
                    }
                } else
                {
                    LOGD("ArTMaxConfigurator::Not isANedwObject");
                    detectedHomography_=*itH;
                    detectedRT_=*itRT;
                    objectDetected_=true;
                }
            } else
            {
                isValidRT_=false;
                isValidH_=false;
//                isValidObj_=false;
                objectDetected_=false;
            }
        }
    }

    LOGD("ArTMaxConfigurator::ProcessFrame() -> Done: %d", ret);
    cout << "----------------------------------" << endl;

    return ret;
}


int ArTMaxConfigurator::CheckDeviceType(multimap<int, Matrix>& _homographies,Image& _frame)
{
    int ret=-1;

//                        LOGD("ArTMaxConfigurator::Homography Obj %d: %f %f %f %f %f %f %f %f %f",itH->first,itH->second.at<double>(0,0),itH->second.at<double>(0,1),itH->second.at<double>(0,2),itH->second.at<double>(1,0),itH->second.at<double>(1,1),itH->second.at<double>(1,2),itH->second.at<double>(2,0),itH->second.at<double>(2,1),itH->second.at<double>(2,2));

    multimap<int, Matrix>::iterator itH=_homographies.begin();

    Image imageRef=meshRefObjs_[(int)(itH->first/1000)].GetTexture();
//                        LOGD("ArTMaxConfigurator::Data 1 %d %d",imageRef.GetHeight(),imageRef.GetWidth());
    Matrix imageRefGray(imageRef.GetHeight(),imageRef.GetWidth(),CV_8UC1);
    Matrix newImage(imageRef.GetHeight(),imageRef.GetWidth(),CV_8UC1);
//                        LOGD("ArTMaxConfigurator::Data 2 %d %d", _frame.GetHeight(),_frame.GetWidth());
//                        cv::cvtColor(imageRef.GetImageData(),imageRefGray,cv::COLOR_RGB2GRAY);
//                        LOGD("ArTMaxConfigurator::Data 3");

    cv::Mat H=itH->second;
    //Y
    for (int i=0;i<newImage.GetRows();i++)
    {
        //X
        for (int j=0;j<newImage.GetCols();j++)
        {
            int x=j;
            int y=i;
            float newXD=H.at<double>(0,0)*x+H.at<double>(0,1)*y+H.at<double>(0,2);
            float newYD=H.at<double>(1,0)*x+H.at<double>(1,1)*y+H.at<double>(1,2);
            float newWD=H.at<double>(2,0)*x+H.at<double>(2,1)*y+H.at<double>(2,2);
            int newJ=(int)(newXD/newWD);
            int newI=(int)(newYD/newWD);

//                                LOGD("ArTMaxConfigurator::Pixel Before %d,%d -> %d,%d",i,j,newI,newJ);
            newImage.at<char>(i,j)=_frame.GetImageData().at<char>(newI,newJ);
//                                LOGD("ArTMaxConfigurator::Pixel After %d,%d -> %d,%d %d",i,j,newI,newJ,_frame.GetImageData().at<char>(newI,newJ));
        }

    }

    LOGD("ArTMaxConfigurator::Homography Obj %d: %f %f %f %f %f %f %f %f %f",itH->first,H.at<double>(0,0),H.at<double>(0,1),H.at<double>(0,2),H.at<double>(1,0),H.at<double>(1,1),H.at<double>(1,2),H.at<double>(2,0),H.at<double>(2,1),H.at<double>(2,2));

    cv::imwrite("/storage/emulated/0/JoinPad/test.jpg",newImage);

//                        Image templImage=meshRefObjs_[1].GetTexture();
    cv::Mat templImage=cv::imread("/storage/emulated/0/JoinPad/XT2_1p_template.png",cv::IMREAD_GRAYSCALE);
    cv::Mat maskImage=cv::imread("/storage/emulated/0/JoinPad/XT2_1p_template.png",cv::IMREAD_GRAYSCALE);

    for (int i=0;i<maskImage.rows;i++)
        for (int j=0;j<maskImage.cols;j++)
        {
            if (maskImage.at<char>(i,j)>0)
                maskImage.at<char>(i,j)=1;
        }

    LOGD("ArTMaxConfigurator::Data 1 %d %d",templImage.rows,templImage.cols);
    int result_cols =  newImage.cols - templImage.cols+ 1;
    int result_rows = newImage.rows - templImage.rows + 1;
    cv::Mat resultImage;
    resultImage.create( result_rows, result_cols, CV_32FC1 );
    cv::Mat graph(255,result_cols,CV_8UC1);
    for (int i=0;i<255;i++)
        for (int j=0;j<result_cols;j++)
        {
            graph.at<char>(i,j)=0;
        }
    LOGD("ArTMaxConfigurator::Data 2");
//                        Image result=meshRefObjs_[1].GetTexture();
    cv::matchTemplate( newImage, templImage, resultImage, CV_TM_CCORR_NORMED,maskImage);
    LOGD("ArTMaxConfigurator::Data 3");

//                        for (int i=0;i<resultImage.cols;i++)
//                        {
//                            LOGD("ArTMaxConfigurator::Data %f",resultImage.at<float>(0,i));
//
//                        }

    cv::normalize( resultImage, resultImage, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
    resultImage=resultImage*255.0;


    for (int i=0;i<resultImage.cols;i++)
    {
        int val=(int)resultImage.at<float>(0,i);
//                            LOGD("ArTMaxConfigurator::VAL: %d",val);
        graph.at<char>(val,i)=250;
    };
    cv::imwrite("/storage/emulated/0/JoinPad/testTemplateMatching.jpg",resultImage);
    cv::imwrite("/storage/emulated/0/JoinPad/testTemplateMatchingGraph.jpg",graph);

    ret=0;
    return ret;
}

int ArTMaxConfigurator::SetImage(Image & _image)
{
    stringstream fileNameScene_str, fileNameScene_pre;

    //    fileNameScene_pre << "/storage/emulated/0/Projects/BrainPadRecognizerX/IMG_7457";//"/storage/emulated/0/Projects/BrainPadRecognizerX/object";
    //    fileNameScene_str << fileNameScene_pre.str() << ".JPG";
    //    fileNameScene_ = fileNameScene_str.str();
    //    utils::LoadImageFromFile(fileNameScene_, cv::IMREAD_GRAYSCALE, imgScene_tmp_);

    LOGD("Image Captured: %d %d", _image.GetHeight(), _image.GetWidth());

    imgScene_tmp_ = _image;

    LOGD("Image Captured 2: %d %d", imgScene_tmp_.GetHeight(), imgScene_tmp_.GetWidth());

    return 0;
}

int ArTMaxConfigurator::GetImage(Image & _imageOut)
{

    LOGD("ArTMaxConfigurator::GetImage");
    _imageOut = imgSceneOut_;

    return 0;
}

//int ArTMaxConfigurator::GetDetectedObject(pair<int, Patch2D>& _detectedObjOut)
//{
////    bool isValid = false;
////    multimap<int, Patch2D > detectedObjs = multiObjDetectorTrackerAndPose_.GetImageObjects(isValid);
////    if (isValid)
////        _detectedObjsOut = detectedObjs;
//    if (& objectDetected_)
//        _detectedObjOut=detectedObject_;
//    return 0;
//}

int ArTMaxConfigurator::GetHomography(pair<int, Matrix>& _homographyOut)
{
//    bool isValid = false;
//    multimap<int, Matrix> homographies = multiObjDetectorTrackerAndPose_.GetHomographies(isValid);
//    if (isValid)
//        _homographiesOut = homographies;
    if (isValidH_ && objectDetected_)
        _homographyOut=detectedHomography_;
    return 0;
}

int ArTMaxConfigurator::GetRTMatrix(pair<int, Matrix>& _rtMatrixOut)
{
//    bool isValid = false;
//    multimap<int, Matrix> rtMatrixes = multiObjDetectorTrackerAndPose_.GetTransforms(isValid);
//    multimap<int, Matrix>::iterator it;
//    LOGD("ArTMaxConfigurator::isValid: ",isValid);
//    for (it=rtMatrixes.begin();it!=rtMatrixes.end();it++)
//    {
//        LOGD("ArTMaxConfigurator::Transform: %f %f %f %f", it->second.at<double>(0, 0), it->second.at<double>(0, 1), it->second.at<double>(0, 2), it->second.at<double>(0, 3));
//        LOGD("ArTMaxConfigurator::Transform: %f %f %f %f", it->second.at<double>(1, 0), it->second.at<double>(1, 1), it->second.at<double>(1, 2), it->second.at<double>(1, 3));
//        LOGD("ArTMaxConfigurator::Transform: %f %f %f %f", it->second.at<double>(2, 0), it->second.at<double>(2, 1),it->second.at<double>(2, 2), it->second.at<double>(2, 3));
//    }
//    if (isValid)
//        _rtMatrixesOut = rtMatrixes;
    if (isValidRT_ && objectDetected_)
        _rtMatrixOut=detectedRT_;

    return 0;
}

int ArTMaxConfigurator::GetReferenceObjects(vector<Patch2D> &_refObjsOut)
{
    _refObjsOut = refObjs_;
    return 0;
}

int ArTMaxConfigurator::GetMeshReferenceObjects(map<int, Mesh> &_meshRefObjsOut)
{
    _meshRefObjsOut = meshRefObjs_;
    return 0;
}

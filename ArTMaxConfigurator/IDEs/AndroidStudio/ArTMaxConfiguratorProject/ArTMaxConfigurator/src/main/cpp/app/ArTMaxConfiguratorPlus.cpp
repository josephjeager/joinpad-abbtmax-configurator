/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArTMaxConfiguratorPlus.cpp
 * Author: marzorati
 * 
 * Created on June 21, 2017, 3:48 PM
 */

#include "ArTMaxConfiguratorPlus.h"


ArTMaxConfiguratorPlus::ArTMaxConfiguratorPlus(std::string _configString, int _loadType)// : camera_(800, 800, 320, 240, 50, 50, 640, 480)
{


    LOGD("ArTMaxConfiguratorPlus::Configure");

    if (!_configString.empty())
    {
        LOGD("ArTMaxConfiguratorPlus::Configure: Open");

        if (_loadType==FROM_FILE)
        {
            cv::String fileToOpen = _configString;
            configFile_.open(fileToOpen, cv::FileStorage::READ);
            LOGD("ArTMaxConfiguratorPlus::Configure from FILE: isOpen: %d", configFile_.isOpened());
            fileNode_ = configFile_.getFirstTopLevelNode();

        }
        else if (_loadType==FROM_STRING)
        {
            cv::String fileToOpen = _configString;
            configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
            LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: isOpen: %d", configFile_.isOpened());
            LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: YAML String: %s", _configString.c_str());

            //carico la sezione corretta
            LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: YAML String: %s", configFile_.getFirstTopLevelNode().name().c_str());

            fileNode_ = configFile_["artmaxconfiguratorplus"];


            yamlString_=_configString;
        }

    }
    else
    {
        LOGD("ArTMaxConfiguratorPlus::BuildDefaultConfigString()");
        _configString=BuildDefaultConfigString();
        string fileToOpen = _configString;
        configFile_.open(fileToOpen, cv::FileStorage::READ | cv::FileStorage::MEMORY);
        LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: isOpen: %d", configFile_.isOpened());
        LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: YAML String: %s", _configString.c_str());

        //carico la sezione corretta
        LOGD("ArTMaxConfiguratorPlus::Configure from YAML String: YAML String: %s", configFile_.getFirstTopLevelNode().name().c_str());

        fileNode_ = configFile_["artmaxconfiguratorplus"];
        LOGD("ArTMaxConfiguratorPlus:: DONE");


//        FileNode features = configFile_["artmaxconfiguratorplus"];
//        FileNodeIterator it = features.begin(), it_end = features.end();
//        int idx = 0;
//        std::vector<uchar> lbpval;
//        for( ; it != it_end; ++it, idx++ )
//        {
//            LOGD("feature #%d" ,idx );
//            cout << "x=" << (int)(*it)["x"] << ", y=" << (int)(*it)["y"] << ", lbp: (";
//            // you can also easily read numerical arrays using FileNode >> std::vector operator.
//            (*it)["lbp"] >> lbpval;
//            for( int i = 0; i < (int)lbpval.size(); i++ )
//                cout << " " << (int)lbpval[i];
//            cout << ")" << endl;
//        }
//        fs2.release();


//       float a,b,c;
//        fileNode_["angleToll"] >> a;
//        LOGD("ArTMaxConfiguratorPlus:: DONE %f",a);
//        fileNode_["toll"] >> b;
//        LOGD("ArTMaxConfiguratorPlus:: DONE %f",b);
//        fileNode_["maxDistance"] >> c;
//        LOGD("ArTMaxConfiguratorPlus:: DONE %f",c);
//
//        //TEST - START
//        cv::String str3;
//        fileNode_["multiObjectClassificatorConfigFilename"] >> str3;

        yamlString_= _configString;
    }


//    configFile_.release();

    isTracking_ = false;
    isInit_ = false;
    isConfigurated_ = false;
    isStarted_ = false;
    objectDetected_=false;
    isValidRT_=false;
    isValidH_=false;
    modality_=1;
//    isValidObj_=false;

    numFrame_ = 0;


}


ArTMaxConfiguratorPlus::~ArTMaxConfiguratorPlus()
{

    multiObjDetectorTrackerAndPose_.Stop();
}


std::string ArTMaxConfiguratorPlus::BuildDefaultConfigString()
{
    return DEFAULT_CONFIG_STRING;

}


int ArTMaxConfiguratorPlus::Configure()
{
    int ret = 0;

    LOGD("ArTMaxConfiguratorPlus::Configure() -> Configuring...");

    LOGD("ArTMaxConfiguratorPlus:: BBBBBBBB: %s",yamlString_.c_str());
    //    multiObjDetectorAndTracker_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/MultiObjectDetectorAndTracker.yaml");
    //    detectorObj_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFeatureDetector.yaml");
    //    descriptorObj_.Setup();//("/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFeatureDescriptor.yaml");
    //    keypoint_matcher_.Setup();//"/storage/emulated/0/projects/BrainPadRecognizerX/config/OCVFlannFeatureMatcher.yaml");
    //    objDetector_.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/ObjectDetector.yaml");
    //    multiTracker.Configure("/storage/emulated/0/projects/BrainPadRecognizerX/config/MultiTracker.yaml");




    string strTmp;
    cv::String str;

    fileNode_["multiObjDetectorTrackerAndPoseConfigFilename"] >> str;
    LOGD("ArTMaxConfiguratorPlus:: %s", str.c_str());
    strTmp=str;
    multiObjDetectorTrackerAndPose_.Configure(strTmp,yamlString_);

    cv::String str2;
    fileNode_["markerRecognizerConfigFilename"] >> str2;
    LOGD("ArTMaxConfiguratorPlus:: %s", str2.c_str());
    strTmp=str2;
    markerRecognizer_.Configure(strTmp,yamlString_);
    LOGD("ArTMaxConfiguratorPlus:: DONE");

    //TEST - START
    cv::String str3;
    fileNode_["multiObjectClassificatorConfigFilename"] >> str3;
    LOGD("ArTMaxConfiguratorPlus:: %s", str3.c_str());
    strTmp=str3;
    multiObjectClassificator_.Configure(strTmp,yamlString_);
    //TEST - END


    fileNode_["objectDetectorConfigFilename"] >> str;
    strTmp=str;
    LOGD("ArTMaxConfiguratorPlus:: %s", str.c_str());
    objDetector_.Configure(strTmp,yamlString_);
    fileNode_["multiObjectTrackerConfigFilename"] >> str;
    strTmp=str;
    LOGD("ArTMaxConfiguratorPlus:: %s", str.c_str());
    multiTracker_.Configure(strTmp,yamlString_);
    fileNode_["poseEstimatorConfigFilename"] >> str;
    strTmp=str;
    LOGD("ArTMaxConfiguratorPlus:: %s", str.c_str());
    poseEstimator_.Configure(strTmp,yamlString_);


    fileNode_["angleToll"]>>angleToll_;
    fileNode_["toll"]>>toll_;
    fileNode_["maxDistance"]>>maxDistance_;
    fileNode_["modality"]>>modality_;



    //    detector_.SetFeatureDetector(&detectorObj_);
    //    detector_.SetFeatureDescriptor(&descriptorObj_);
    //
    //    matcher_.SetFeatureMatcher(&keypoint_matcher_);
    //
    //    objDetector_.SetDetector(&detector_);
    //    objDetector_.SetMatcher(&matcher_);

    ret = multiObjDetectorTrackerAndPose_.SetObjectDetector(&objDetector_);
    if (ret == 0)
        ret = multiObjDetectorTrackerAndPose_.SetMultiTracker(&multiTracker_);
    if (ret==0)
        ret=multiObjDetectorTrackerAndPose_.SetPoseEstimator(&poseEstimator_);

    if (ret == 0)
        ret = multiObjDetectorTrackerAndPose_.Init();
    if (ret == 0)
        ret = markerRecognizer_.Init();

    //TEST - START
    if (ret == 0)
        ret = multiObjectClassificator_.Init();
    //TEST - END


    if (ret == 0)
        isConfigurated_ = true;

    LOGD("ArTMaxConfiguratorPlus::Configure() -> Done: %d", ret);

    return ret;
}

int ArTMaxConfiguratorPlus::SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV,double _vFOV,double _k1,double _k2,double _k3,double _p1,double _p2)
{

    if (_fx==0 && _fy==0 && _cx==0 && _cy==0)
    {
        _cx = _width / 2.0;
        _cy = _height / 2.0;
        _fx = FOV2FOCAL(_width, DEG2RAD(_hFOV));
        _fy = FOV2FOCAL(_height, DEG2RAD(_vFOV));
    }



    multiTracker_.SetCameraDevice(_fx,_fy,_cx,_cy,_width,_height,_hFOV,_vFOV);
    poseEstimator_.SetCameraDevice(_fx,_fy,_cx,_cy,_width,_height,_hFOV,_vFOV);
    markerRecognizer_.SetCameraDevice(_fx,_fy,_cx,_cy,_width,_height,_hFOV,_vFOV);


    LOGD("ArTMaxConfiguratorPlus:: AAAAAAAAAAAAAA: %s",yamlString_.c_str());

    return 0;
}


int ArTMaxConfiguratorPlus::Init()
{
    int ret = 0;

    LOGD("ArTMaxConfiguratorPlus::Init() -> Initializing...");

    if (isConfigurated_ )
    {

        if (!fileNode_["numObjectsToDetect"].empty())
        {
            numFrame_ = 0;
            int numObjs;
            fileNode_["numObjectsToDetect"] >> numObjs;

            LOGD("ArTMaxConfiguratorPlus::NumObjectsToDetect: %d", numObjs);

            /************************
             *  COMPUTE TARGET OBJECTS KEYPOINTS
             ************************/

            int objID;
            for (int numObj = 0; numObj < numObjs; ++numObj)
            {

                //Load Image

                Image imgObj, imgObj_tmp;

                std::string fileNameObj, filenameObjConfig;

                std::string objName;

                fileNode_["objectModelFilenames"][numObj] >> filenameObjConfig; //"/media/marzorati/Data_Windows/Android/StudioProjects/SapphireProject/IDEs/NetBeans/Sapphire/res/object.jpg"; //"/home/marzorati/Documents/projects/ABB_datasets/ABB_objects_selected/Type2/IMG_7443_crop_scaled.JPG"; //"/home/marzorati/Documents/projects/Sapphire/res/object.jpg"; //../../res/Emax2_4.jpg";

                cv::FileStorage configFileObj;
                configFileObj.open(filenameObjConfig, cv::FileStorage::READ);

                LOGD("ArTMaxConfiguratorPlus::ObjectModelFilenames: %s", filenameObjConfig.c_str());
                if (configFileObj.isOpened())
                {

                    configFileObj["objectModelFilename"] >> fileNameObj;
                    configFileObj["objectName"] >> objName;
                    configFileObj["objectID"] >> objID;
                    LOGD("ArTMaxConfiguratorPlus::ObjectToDetect %s ID: %d Filename: %s ", objName.c_str(), objID, fileNameObj.c_str());


                    //NEW PART

                    std::vector<Point3D<float> > shape3D;
                    std::vector<std::vector<float> > shapes3DXYZ;

                    configFileObj["shape3D"] >> shapes3DXYZ;

                    int numVertexCoord = 0;
                    for (int numPoint = 0; numPoint < shapes3DXYZ.size(); ++numPoint)
                    {
                        numVertexCoord = 0;
                        //                std::vector<Point3D<float> > shape3D;
                        for (int m = 0; m < shapes3DXYZ[numPoint].size(); m = m + 3)
                        {
                            Point3D<float> point3D(shapes3DXYZ[numPoint][m], shapes3DXYZ[numPoint][m + 1], shapes3DXYZ[numPoint][m + 2]);
                            shape3D.push_back(point3D);
                        }
                        //                shapes3D.push_back(shape3D);
                    }

                    //SET OBJECT TEXTURE POINTS 2D
//                std::vector< Shape2D > shapes2D;
                    std::vector<std::vector<Point2D<float> > > shapes2D;
                    std::vector<std::vector<float> > shapes2DXY;

                    configFileObj["shape2D"] >> shapes2DXY;


                    //TEMP
//                Shape2D shape2D2;
                    //END TEMP

                    for (int numFace = 0; numFace < shapes2DXY.size(); ++numFace)
                    {
                        numVertexCoord = 0;
                        vector<Point2D<float> > shape2D;
                        for (int m = 0; m < shapes2DXY[numFace].size(); m = m + 2)
                        {
                            Point2D<float> point2D(shapes2DXY[numFace][m], shapes2DXY[numFace][m + 1]);
                            shape2D.push_back(point2D);
                        }

                        //TEMP
//                    shape2D2 = shape2D;
                        //END TEMP

                        shapes2D.push_back(shape2D);
                    }


                    sapphire::utils::LoadImageFromFile(fileNameObj, cv::IMREAD_UNCHANGED, imgObj_tmp);

                    LOGD("ArTMaxConfiguratorPlus::Loaded Image: %s", fileNameObj.c_str());

                    if (imgObj_tmp.GetHeight() * imgObj_tmp.GetWidth() <= 0)
                    {
                        LOGD("ArTMaxConfiguratorPlus::Image %s is empty or cannot be found\n", fileNameObj.c_str());
                        continue;
                    }

                    imgObj = imgObj_tmp;


                    std::vector<std::vector<int> > facesIdxs;
                    configFileObj["faces"] >> facesIdxs;

//                //CREATE OBJECT MESH
//                LOGD("BrainPadRecognizerXPlus:: Adding mesh");
//                Mesh meshObj(objID, imgObj, shape3D);
//
//                LOGD("BrainPadRecognizerXPlus:: Adding %d faces", (int)facesIdxs.size());
//                for (int numFace = 0; numFace < facesIdxs.size(); ++numFace)
//                {
//                    meshObj.AddFace(facesIdxs[numFace], shapes2D[numFace]);
//                }
//
//                Patch2D imgPatch(meshObj.GetUniqueObjId(), imgObj, shape2D2.GetShape()); //, objCenterX, objCenterY, shape3D);
//
//                refObjs_.push_back(imgPatch);
//
//
//                meshRefObjs_.insert(pair<int, Mesh> (objID, meshObj));
//
//                refObjName_[objID] = Info(objName);



                    AddNewObjectToDetect(objID, shape3D, facesIdxs, shapes2D, imgObj, objName);

                }
                else
                {
                    std::cout << "Error: Object Config file doesn't exist" << endl;
                }
            }

        }
        //        map<int,string>::iterator it;
        //        for (it=refObjName_.begin();it!=refObjName_.end();it++)
        //        {
        //            LOGD("BrainPadRecognizerX::Adding Ref Object: %s ID: %d",it->second.c_str(),it->first);
        //        }

        LOGD("ArTMaxConfiguratorPlus::Init() -> refObjs_.size() %d ",refObjs_.size());
        LOGD("ArTMaxConfiguratorPlus::Init() -> meshRefObjs_.size() %d ",meshRefObjs_.size());


        ret = multiObjDetectorTrackerAndPose_.AddReferenceObjects(refObjs_, meshRefObjs_);

        if (ret == 0)
        {
            isInit_ = true;
        }
    }

    LOGD("ArTMaxConfiguratorPlus::Init() -> Done: %d", ret);


    return ret;
}


int ArTMaxConfiguratorPlus::AddNewObjectToDetect(int _objID,std::vector<Point3D<float> > & _shape3D,std::vector< std::vector<int> >& _facesIdxs,std::vector< std::vector<Point2D<float> > >& _shapes2D,Image& _imgObj,string _objName)
{
    int ret = 0;

    LOGD("ArTMaxConfiguratorPlus::Init() -> Initializing...");

    if (isConfigurated_)
    {

        numFrame_ = 0;

        std::vector< Shape2D > shapes2D;
        Shape2D shape2D2;

        LOGD("ArTMaxConfiguratorPlus::Init() -> Faces num %d", _shapes2D.size());

        for (int numFace = 0; numFace < _shapes2D.size(); numFace++)
        {
            Shape2D shape2D;
            for (int m = 0; m < _shapes2D[numFace].size(); m++)
            {
                Point2D<float> point2D(_shapes2D[numFace][m].x_, _shapes2D[numFace][m].y_);
                shape2D.AddVertex2D(point2D);


                LOGD("ArTMaxConfiguratorPlus::Init() -> Points 2D num %d %f %f",numFace, point2D.x_, point2D.y_);
                LOGD("ArTMaxConfiguratorPlus::Init() -> Points 3D num %d %f %f %f",numFace, _shape3D[m].x_, _shape3D[m].y_,_shape3D[m].z_);

            }
            shape2D2 = shape2D;
            shapes2D.push_back(shape2D);
        }

        Mesh meshObj(_objID, _imgObj, _shape3D);

        for (int numFace = 0; numFace < _facesIdxs.size(); ++numFace)
        {

            LOGD("ArTMaxConfiguratorPlus::Init() -> _facesIdxs[numFace] %d %d %d %d",_facesIdxs[numFace][0],_facesIdxs[numFace][1],_facesIdxs[numFace][2],_facesIdxs[numFace][3]);

            meshObj.AddFace(_facesIdxs[numFace], shapes2D[numFace]);
        }

//        meshObj.AddFace(_facesIdxs, shape2D);

        LOGD("ArTMaxConfiguratorPlus::Init() -> Patch");
        Patch2D imgPatch(meshObj.GetUniqueObjId(), _imgObj, shape2D2.GetShape()); //, objCenterX, objCenterY, shape3D);
        LOGD("ArTMaxConfiguratorPlus::Init() -> imgPatch");

        refObjs_.push_back(imgPatch);
        LOGD("ArTMaxConfiguratorPlus::Init() -> insert");

        meshRefObjs_.insert(pair<int, Mesh> (_objID, meshObj));
        LOGD("ArTMaxConfiguratorPlus::Init() -> ObjID %d",_objID);

        Info info(_objName);
        refObjName_.insert(pair<int,Info>(_objID,info));
        LOGD("ArTMaxConfiguratorPlus::Init() -> ObjName %s",_objName.c_str());

//        ret = multiObjDetectorTrackerAndPose_.AddReferenceObjects(refObjs_, meshRefObjs_);
//
//        if (ret == 0)
//        {
//            isInit_ = true;
//        }

    }

    LOGD("ArTMaxConfiguratorPlus::Init() -> Done: %d", ret);


    return ret;
}

int ArTMaxConfiguratorPlus::Stop()
{
    LOGD("ArTMaxConfiguratorPlus::Stop() -> Stopping...");
    int ret = 0;
    ret = multiObjDetectorTrackerAndPose_.Stop();

    ret = markerRecognizer_.Stop();

    //TEST - START
    ret = multiObjectClassificator_.Stop();
    //TEST - END

    LOGD("ArTMaxConfiguratorPlus::Stop() -> Done: %d", ret);
    return ret;

}

int ArTMaxConfiguratorPlus::Start()
{
    LOGD("ArTMaxConfiguratorPlus::Start() -> Starting...");
    int ret = 0;
    ret = multiObjDetectorTrackerAndPose_.Start();

    ret = markerRecognizer_.Start();

    //TEST - START
    ret = multiObjectClassificator_.Start();
    //TEST - END

    LOGD("ArTMaxConfiguratorPlus::Start() -> Done: %d", ret);
    return ret;

}

bool ArTMaxConfiguratorPlus::CheckObjectPose(Matrix& _rtW_C,double _angleToll,double _toll,double _maxDistance)
{
    bool ret=false;
    //check position
    double nx = _rtW_C.at<double>(0, 0);
    double ny =  _rtW_C.at<double>(1, 0);
    double nz =  _rtW_C.at<double>(2, 0);
    double sz =  _rtW_C.at<double>(2, 1);
    double az =  _rtW_C.at<double>(2, 2);

    double sy = sqrt(nx * nx + ny * ny);

    double phi = atan2(sz,az); //atan2(sinTheta*ax-cosTheta*ay,cosTheta*sy-sinTheta*sx);
    double gamma = atan2(-nz, sy);
    double theta = atan2(ny, nx);
    double x=_rtW_C.at<double>(0,3);
    double y=_rtW_C.at<double>(1,3);
    double z=_rtW_C.at<double>(2,3);

//    Matrix xO_C(4,1,CV_64F);
//    xO_C.at<double>(0,0)=INIT_POSE_PHI;
//    xO_C.at<double>(1,0)=INIT_POSE_GAMMA;
//    xO_C.at<double>(2,0)=theta;
//    xO_C.at<double>(3,0)=x;
//    xO_C.at<double>(4,0)=y;
//    xO_C.at<double>(5,0)=z;
//
//    Matrix rtC_O=(cv::Mat)(utils::CreateRTMatrix(xO_C).inv());
//    Matrix rtW_O=(cv::Mat)(_rtW_C*rtC_O);
//
//    Matrix xW_O=utils::CreateRTVector(rtW_O);

    LOGD("ArTMaxConfiguratorPlus::Pose %f %f %f %f %f %f", phi, gamma, theta,x,y,z);

    LOGD("ArTMaxConfiguratorPlus::Pose Diff %f<%f %f<%f %f<%f %f<%f %f<%f ", fabs(phi + INIT_POSE_PHI),_angleToll, fabs(gamma + INIT_POSE_GAMMA),_angleToll, fabs(x/z)+INIT_POSE_X,_toll,fabs(y/z)+INIT_POSE_Y,_toll,z,_maxDistance);

    if (fabs(phi + INIT_POSE_PHI) < _angleToll && fabs(gamma + INIT_POSE_GAMMA) < _angleToll && fabs(x/z)+INIT_POSE_X<_toll && fabs(y/z)+INIT_POSE_Y<_toll && z<_maxDistance) {
        LOGD("ArTMaxConfiguratorPlus::Pose Check OK");
        ret = true;
    }
    else {
        LOGD("ArTMaxConfiguratorPlus::Pose Check FAILED");
        ret = false;
    }
    return ret;
}


int ArTMaxConfiguratorPlus::DetectMarkers(Image & _image,multimap<int,Marker>& _markersOut)
{

    int ret=-1;

    _markersOut.clear();
//    _rtsOut.clear();
//    cv::Mat rtEye(4,4,CV_64F);
//    _rtPmtt_MOut=(cv::Mat)rtEye.eye(4,4,CV_64F);

    //detect marker
    //TODO: Da non fare sempre ma eseguirlo in un thread a parte
    ret = markerRecognizer_.ProcessFrame(_image);

    bool isValid=false;

    //recover the position of the marker w.r.t. current position of the camera (in the camera reference frame)
    double timestamp=-1;
    std::multimap<int, Matrix> rtPmkt_Ms = markerRecognizer_.GetTransforms(isValid, timestamp);
    std::multimap<int, Matrix>::iterator itRTPmkt_M;

    //take only the first marker identified
    for (itRTPmkt_M=rtPmkt_Ms.begin();itRTPmkt_M!=rtPmkt_Ms.end();itRTPmkt_M++)
    {
        Marker markerOut;
        markerOut.timestamp_=timestamp;


        //marker ID and marker position w.r.t. camera reference frame
        markerOut.detectedMarkerID_ = itRTPmkt_M->first;

        //if the marker is detected, then reset the marker lives
        markerOut.markerLives_=MAX_MARKER_LIVES;
        markerOut.numDetections_++;

        vector<Point3D<float> > markerWorldPoints;
        MarkerPoseEstimator* pPoseEstimator;
        markerRecognizer_.GetMarkerPoseEstimator(pPoseEstimator);
        pPoseEstimator->GetWorldPoints3D(markerWorldPoints);

        double minX=1e10,maxX=-1e10;
        double minY=1e10,maxY=-1e10;
        for (int i=0;i<markerWorldPoints.size();++i)
        {
            if (minX>markerWorldPoints[i].x_)
                minX=markerWorldPoints[i].x_;
            if (minY>markerWorldPoints[i].y_)
                minY=markerWorldPoints[i].y_;
            if (maxX<markerWorldPoints[i].x_)
                maxX=markerWorldPoints[i].x_;
            if (maxY<markerWorldPoints[i].y_)
                maxY=markerWorldPoints[i].y_;
        }
        markerOut.dimX_=maxX-minX;
        markerOut.dimY_=maxY-minY;

        LOGD("ArTMaxConfiguratorPlus::Marker Length: %f %f",maxX-minX,maxY-minY);



        //compute the RT from the device frame and the marker (M) and return it
//        _rtPmtt_MOut = (cv::Mat) (itRTPmkt_M->second);

        LOGD("ArTMaxConfiguratorPlus::itRTPmkt_M->second %f %f %f %f", itRTPmkt_M->second.at<double>(0, 0), itRTPmkt_M->second.at<double>(0, 1), itRTPmkt_M->second.at<double>(0, 2), itRTPmkt_M->second.at<double>(0, 3));
        LOGD("ArTMaxConfiguratorPlus::itRTPmkt_M->second %f %f %f %f", itRTPmkt_M->second.at<double>(1, 0), itRTPmkt_M->second.at<double>(1, 1), itRTPmkt_M->second.at<double>(1, 2), itRTPmkt_M->second.at<double>(1, 3));
        LOGD("ArTMaxConfiguratorPlus::itRTPmkt_M->second %f %f %f %f", itRTPmkt_M->second.at<double>(2, 0), itRTPmkt_M->second.at<double>(2, 1), itRTPmkt_M->second.at<double>(2, 2), itRTPmkt_M->second.at<double>(2, 3));
        LOGD("ArTMaxConfiguratorPlus::itRTPmkt_M->second %f %f %f %f", itRTPmkt_M->second.at<double>(3, 0), itRTPmkt_M->second.at<double>(3, 1), itRTPmkt_M->second.at<double>(3, 2), itRTPmkt_M->second.at<double>(3, 3));


        markerOut.rtC_M_=itRTPmkt_M->second;

        pair<int,Marker> tmp(itRTPmkt_M->first,markerOut);
        _markersOut.insert(tmp);

//        _rtsOut.push_back(itRTPmkt_M->second);
        ret=0;
    }


    return ret;
}

int ArTMaxConfiguratorPlus::ProcessFrame(Image& _frame)
{
    int ret = 0;
    LOGD("ArTMaxConfiguratorPlus::ProcessFrame() -> Processing Frame...");

    if (isConfigurated_)
    {
        int numProcessedFrame = 0;

        numFrame_++;

        if (_frame.GetHeight() * _frame.GetWidth() <= 0)
        {
            LOGD("ArTMaxConfiguratorPlus::Image is empty or cannot be found");
            return -1;
        }
        else
        {


            if (modality_==PROCESS_MODE_OBJTRACKANDPOSE || modality_==PROCESS_MODE_MARKERSDETECT) {
                cv::cvtColor(_frame.GetImageData(), imgScene_.GetImageData(), cv::COLOR_RGB2GRAY,
                             1);
            }
            else if (modality_==PROCESS_MODE_OBJCLASSIFY)
            {
                imgScene_ = _frame;
            }
            LOGD("ArTMaxConfiguratorPlus::ImageType %d %d", imgScene_.GetImageData().type(),imgScene_.GetImageData().rows);


            //MULTI OBJECT DETECTOR TRACKER AND POSE
            if (modality_==PROCESS_MODE_OBJTRACKANDPOSE) {
                isValidRT_ = false;
                isValidH_ = false;
//            isValidObj_=false;
                ret = multiObjDetectorTrackerAndPose_.ProcessFrame(imgScene_);

                multimap<int, Matrix> rtMatrixes = multiObjDetectorTrackerAndPose_.GetTransforms(
                        isValidRT_);
//            multimap<int,Patch2D> detectedObjects=multiObjDetectorTrackerAndPose_.GetImageObjects(isValidObj_);
                multimap<int, Matrix> homographies = multiObjDetectorTrackerAndPose_.GetHomographies(
                        isValidH_);

                multimap<int, Matrix>::iterator itRT;
                multimap<int, Matrix>::iterator itH;
//            multimap<int, Patch2D>::iterator itObj;
                LOGD("ArTMaxConfiguratorPlus::isValidRT: %d isValidH: %d", isValidRT_, isValidH_);

                //only one object must be detected
                if (isValidRT_ && isValidH_ && rtMatrixes.size() == 1) {
                    itRT = rtMatrixes.begin();
//                itObj=detectedObjects.begin();
                    itH = homographies.begin();

                    if (!objectDetected_) {
                        if (CheckObjectPose(itRT->second, angleToll_, toll_, maxDistance_)) {
                            LOGD("ArTMaxConfiguratorPlus::ImageType %d %d",
                                 imgScene_.GetImageData().type(), imgScene_.GetImageData().rows);
                            objectDetected_ = true;
//                        detectedObject_=*itObj;
                            detectedHomography_ = *itH;
                            detectedRT_ = *itRT;
                            LOGD("ArTMaxConfiguratorPlus::Data Copied");

#ifdef _DRAW
                            vector<cv::KeyPoint> queryKeyPointsOCV, trainKeyPointsOCV;
            //                vector<cv::DMatch> matchesOCV;

                            utils::ConvertToOCV(objDetector_.queryDetector_.GetKeyPoints(), queryKeyPointsOCV);
                            utils::ConvertToOCV(objDetector_.trainDetector_.GetKeyPoints(), trainKeyPointsOCV);
            //                utils::ConvertToOCV(objDetector_.matcher_.GetMatches()[0], matchesOCV);
            //                cv::drawMatches(imgSceneOut_.GetImageData(), queryKeyPointsOCV, imgSceneOut_.GetImageData(), trainKeyPointsOCV,
            //                                matchesOCV, imgSceneOut_.GetImageData(), cv::Scalar::all(-1), cv::Scalar::all(-1),
            //                                vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                            cv::Mat tmp = cv::Mat(_frame.GetImageData().rows, _frame.GetImageData().cols, CV_8UC1);
                            cv::drawKeypoints(_frame.GetImageData(), queryKeyPointsOCV, tmp, cv::Scalar(0, 255, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
                            cv::drawKeypoints(tmp, trainKeyPointsOCV, imgSceneOut_.GetImageData(), cv::Scalar(255, 255, 0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);


            //                int colorR = 255 / clusters.size();
            //                int colorB = 255 / clusters.size();
            //                for (it = clusters.begin(); it != clusters.end(); it++)
            //                {
            //                    for (int j = 0; j < it->second.size(); j++)
            //                    {
            //                        circle(img_matches, cv::Point(it->second[j].first.x_, it->second[j].first.y_), 15, cv::Scalar(colorB * (it->first + 1), 0, colorR * (it->first) + 1), -1);
            //                    }
            //                }
#endif
                        } else {
                            multiObjDetectorTrackerAndPose_.RemoveObject(itRT->first);
                            objectDetected_ = false;
                        }
                    } else {
                        LOGD("ArTMaxConfiguratorPlus::Not isANedwObject");
                        detectedHomography_ = *itH;
                        detectedRT_ = *itRT;
                        objectDetected_ = true;
                    }
                } else {
                    isValidRT_ = false;
                    isValidH_ = false;
//                isValidObj_=false;
                    objectDetected_ = false;
                }
            }

            if (modality_==PROCESS_MODE_MARKERSDETECT) {
                //MULTI MARKER RECOGNIZER
                multimap<int, Marker> markers;
                ret = DetectMarkers(imgScene_, markers);
                LOGD("ArTMaxConfiguratorPlus::DetectMarkers: %d", ret);
                multimap<int, Marker>::iterator it;

                for (it = markers.begin(); it != markers.end(); ++it) {
                    //if the markerID is equal to the last detected marker ID or thsi is the first time I detect a marker
                    multimap<int, Marker>::iterator detectedMarkerIt = detectedMarkers_.find(
                            it->first);
                    LOGD("ArTMaxConfiguratorPlus::DetectMarker ID: %d", it->first);
                    if (detectedMarkerIt !=
                        detectedMarkers_.end())//second.detectedMarkerID_==detectedMarker_.detectedMarkerID_ || detectedMarker_.detectedMarkerID_==-1)
                    {
                        LOGD("ArTMaxConfiguratorPlus::Already detected marker");

                        detectedMarkerIt->second.detectedMarkerID_ = it->second.detectedMarkerID_;

                        //if the marker is detected, then reset the marker lives
                        detectedMarkerIt->second.markerLives_ = it->second.markerLives_;
                        detectedMarkerIt->second.numDetections_++;
                        detectedMarkerIt->second.dimX_ = it->second.dimX_;
                        detectedMarkerIt->second.dimY_ = it->second.dimY_;
                        detectedMarkerIt->second.rtC_M_ = it->second.rtC_M_;

                    } else {
                        LOGD("ArTMaxConfiguratorPlus::New detected marker");

                        pair<int, Marker> tmp(it->first, it->second);
                        detectedMarkers_.insert(tmp);
                    }
                }

                ret = UpdateMarkers(detectedMarkers_);
                LOGD("ArTMaxConfiguratorPlus::Updated markers");
            }

            if (modality_==PROCESS_MODE_OBJCLASSIFY) {
                //TEST - START
//            cv::cvtColor(imgScene_.GetImageData(), imgScene_.GetImageData(), COLOR_BGR2RGB);

//            LOGD("OCVMultiObjectClassificator:: IMAGE DEPTH: %d",imgScene_.GetImageData().channels());
                ret = multiObjectClassificator_.ProcessFrame(imgScene_);
                LOGD("ArTMaxConfiguratorPlus::Multi Object Classificator - END");
                //TEST - END
            }

        }
    }

    LOGD("ArTMaxConfiguratorPlus::ProcessFrame() -> Done: %d", ret);
    cout << "----------------------------------" << endl;

    return ret;
}

int ArTMaxConfiguratorPlus::UpdateMarkers(multimap<int,Marker>& _markers)
{
    int ret=0;
    LOGD("ArTMaxConfiguratorPlus::Update markers");

    multimap<int,Marker>::iterator it=_markers.begin();
    for (it=_markers.begin();it!=_markers.end();++it)
    {
        it->second.markerLives_--;
        if (it->second.markerLives_<0)
        {
            _markers.erase(it);
        }
    }

    return ret;
}

int ArTMaxConfiguratorPlus::SetImage(Image & _image)
{
    stringstream fileNameScene_str, fileNameScene_pre;

    //    fileNameScene_pre << "/storage/emulated/0/Projects/BrainPadRecognizerX/IMG_7457";//"/storage/emulated/0/Projects/BrainPadRecognizerX/object";
    //    fileNameScene_str << fileNameScene_pre.str() << ".JPG";
    //    fileNameScene_ = fileNameScene_str.str();
    //    utils::LoadImageFromFile(fileNameScene_, cv::IMREAD_GRAYSCALE, imgScene_tmp_);

    LOGD("Image Captured: %d %d", _image.GetHeight(), _image.GetWidth());

    imgScene_tmp_ = _image;

    LOGD("Image Captured 2: %d %d", imgScene_tmp_.GetHeight(), imgScene_tmp_.GetWidth());

    return 0;
}

int ArTMaxConfiguratorPlus::GetImage(Image & _imageOut)
{

    LOGD("ArTMaxConfiguratorPlus::GetImage");
    _imageOut = imgSceneOut_;

    return 0;
}

//int ArTMaxConfiguratorPlus::GetDetectedObject(pair<int, Patch2D>& _detectedObjOut)
//{
////    bool isValid = false;
////    multimap<int, Patch2D > detectedObjs = multiObjDetectorTrackerAndPose_.GetImageObjects(isValid);
////    if (isValid)
////        _detectedObjsOut = detectedObjs;
//    if (& objectDetected_)
//        _detectedObjOut=detectedObject_;
//    return 0;
//}

int ArTMaxConfiguratorPlus::GetHomography(pair<int, Matrix>& _homographyOut)
{
//    bool isValid = false;
//    multimap<int, Matrix> homographies = multiObjDetectorTrackerAndPose_.GetHomographies(isValid);
//    if (isValid)
//        _homographiesOut = homographies;
    if (isValidH_ && objectDetected_)
        _homographyOut=detectedHomography_;
    return 0;
}

int ArTMaxConfiguratorPlus::GetRTMatrix(pair<int, Matrix>& _rtMatrixOut)
{
//    bool isValid = false;
//    multimap<int, Matrix> rtMatrixes = multiObjDetectorTrackerAndPose_.GetTransforms(isValid);
//    multimap<int, Matrix>::iterator it;
//    LOGD("ArTMaxConfiguratorPlus::isValid: ",isValid);
//    for (it=rtMatrixes.begin();it!=rtMatrixes.end();it++)
//    {
//        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(0, 0), it->second.at<double>(0, 1), it->second.at<double>(0, 2), it->second.at<double>(0, 3));
//        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(1, 0), it->second.at<double>(1, 1), it->second.at<double>(1, 2), it->second.at<double>(1, 3));
//        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(2, 0), it->second.at<double>(2, 1),it->second.at<double>(2, 2), it->second.at<double>(2, 3));
//    }
//    if (isValid)
//        _rtMatrixesOut = rtMatrixes;
    if (isValidRT_ && objectDetected_)
        _rtMatrixOut=detectedRT_;

    return 0;
}

int ArTMaxConfiguratorPlus::GetReferenceObjects(vector<Patch2D> &_refObjsOut)
{
    _refObjsOut = refObjs_;
    return 0;
}

int ArTMaxConfiguratorPlus::GetMeshReferenceObjects(map<int, Mesh> &_meshRefObjsOut)
{
    _meshRefObjsOut = meshRefObjs_;
    return 0;
}

int ArTMaxConfiguratorPlus::GetMarkersDetectedObjects(multimap<int, Shape2D>& _detectedObjsOut)
{
    bool isValid = false;
    double timestamp=-1;
    multimap<int, Shape2D > detectedObjs = markerRecognizer_.GetShapeObjects(isValid,timestamp);
    if (isValid)
        _detectedObjsOut = detectedObjs;
    return 0;
}

int ArTMaxConfiguratorPlus::GetMarkersHomographies(multimap<int, Matrix>& _homographiesOut)
{
    bool isValid = false;
    double timestamp=-1;
    multimap<int, Matrix> homographies = markerRecognizer_.GetHomographies(isValid,timestamp);
    if (isValid)
        _homographiesOut = homographies;
    return 0;
}

int ArTMaxConfiguratorPlus::GetMarkersRTMatrixes(multimap<int, Matrix>& _rtMatrixesOut)
{
    bool isValid = false;
    double timestamp=-1;

    multimap<int, Matrix> rtMatrixes = markerRecognizer_.GetTransforms(isValid,timestamp);
    multimap<int, Matrix>::iterator it;
    LOGD("ArTMaxConfiguratorPlus::isValid: ",isValid);
    for (it=rtMatrixes.begin();it!=rtMatrixes.end();it++)
    {
        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(0, 0), it->second.at<double>(0, 1), it->second.at<double>(0, 2), it->second.at<double>(0, 3));
        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(1, 0), it->second.at<double>(1, 1), it->second.at<double>(1, 2), it->second.at<double>(1, 3));
        LOGD("ArTMaxConfiguratorPlus::Transform: %f %f %f %f", it->second.at<double>(2, 0), it->second.at<double>(2, 1),it->second.at<double>(2, 2), it->second.at<double>(2, 3));
    }
    if (isValid)
        _rtMatrixesOut = rtMatrixes;
    return 0;
}


//TEST - START

int ArTMaxConfiguratorPlus::GetDetectedClassifiedBoxes(multimap<int, Shape2D>& _detectedBoxesOut)
{
    bool isValid = false;
    double timestamp=-1;
    multimap<int, Shape2D > detectedBoxes = multiObjectClassificator_.GetShapeObjects(isValid,timestamp);
    if (isValid)
        _detectedBoxesOut = detectedBoxes;
    return 0;
}

int ArTMaxConfiguratorPlus::GetDetectedClassifiedObjects(multimap<int, Patch2D>& _detectedObjectsOut)
{
    bool isValid = false;
    double timestamp=-1;
    multimap<int, Patch2D > detectedObjects = multiObjectClassificator_.GetImageObjects(isValid,timestamp);
    if (isValid)
        _detectedObjectsOut = detectedObjects;
    return 0;
}
int ArTMaxConfiguratorPlus::SetInferenceGraphBuffer(char* _pInferenceGraphBuffer,int _inferenceGraphBufferLength,char* _pInferenceGraphConfigBuffer,int _inferenceGraphConfigBufferLength,char* _pObjectLabelsBuffer,int _objectLabelsBufferLength)
{
    int ret=-1;
    ret  = multiObjectClassificator_.SetInferenceGraphBuffer(_pInferenceGraphBuffer,_inferenceGraphBufferLength,_pInferenceGraphConfigBuffer,_inferenceGraphConfigBufferLength,_pObjectLabelsBuffer,_objectLabelsBufferLength);
    return ret;
}
//TEST - END
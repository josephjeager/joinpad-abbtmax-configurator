#include "ArTMaxConfiguratorPlus_interop.h"



long nativeCreate(/*Obj2DLocalization ** outPtr */) {
	long ret = 0;
	//ret=(long)new cv::Mat(4,4,CV_32F);
	ret=(long)  new ArTMaxConfiguratorPlus("", 2);
    return ret;
}

void nativeDestroy(long thiz) {
    if (thiz != 0) {
        delete ((ArTMaxConfiguratorPlus *) thiz);
    }
}

int nativeInit(long thiz) {
    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->Init();
    return ret;
}


int nativeAddNewObjectToDetect(long thiz, int objectId, long queryFrame, long worldPlane) {
    Image queryFrameImage;
    Matrix queryFrameMat;
    queryFrameMat = *(cv::Mat *) queryFrame;
    queryFrameImage.SetImageData(queryFrameMat);
//    ret = ((Obj2Dlocalization *) thiz)->ProcessFrame(img);
//

//
//    Mat queryFrameMat = *(cv::Mat *) queryFrame;
//    Mat v = *(cv::Mat *) worldPlane;
//
//    Point2D point2D;
//
//    Point3D point3D;
//
//    worldPlanePoints.push_back(cvPoint3D32f());
//    worldPlanePoints.push_back(cvPoint3D32f());
//    worldPlanePoints.push_back(cvPoint3D32f());
//    worldPlanePoints.push_back(cvPoint3D32f());

    Mat *worldPlaneMat = (Mat *) worldPlane;
    std::vector<Point3D<float> > point3dVect;
    point3dVect.push_back(Point3D<float>(worldPlaneMat->at<float>(0, 0), worldPlaneMat->at<float>(0, 1), worldPlaneMat->at<float>(0, 2)));
    point3dVect.push_back(Point3D<float>(worldPlaneMat->at<float>(1, 0), worldPlaneMat->at<float>(1, 1), worldPlaneMat->at<float>(1, 2)));
    point3dVect.push_back(Point3D<float>(worldPlaneMat->at<float>(2, 0), worldPlaneMat->at<float>(2, 1), worldPlaneMat->at<float>(2, 2)));
    point3dVect.push_back(Point3D<float>(worldPlaneMat->at<float>(3, 0), worldPlaneMat->at<float>(3, 1), worldPlaneMat->at<float>(3, 2)));

    std::vector<std::vector<Point2D<float> > > point2dFacesVect;
    std::vector<Point2D<float> > point2dFaceVect;
    point2dFaceVect.push_back(Point2D<float>(0, 0));
    point2dFaceVect.push_back(Point2D<float>(0, queryFrameMat.rows));
    point2dFaceVect.push_back(Point2D<float>(queryFrameMat.cols, queryFrameMat.rows));
    point2dFaceVect.push_back(Point2D<float>(queryFrameMat.cols, 0));
    point2dFacesVect.push_back(point2dFaceVect);

    std::vector<std::vector<int>> faces;
    std::vector<int> face;
    face.push_back(0);
    face.push_back(1);
    face.push_back(2);
    face.push_back(3);
    faces.push_back(face);
//    Shape2D facesShape(faces);

//    int _objID,std::vector<Point3D<float> > & _shape3D,std::vector<int> _facesIdxs,std::vector<Point2D<float> > _shape2D,Image& _imgObj

    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->AddNewObjectToDetect(objectId, point3dVect, faces, point2dFacesVect, queryFrameImage, "test");
    return ret;
}

int nativeSetCameraParams(long thiz, int _width, int _height, float _hFOV, float _vFOV) {
    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->SetCameraParams(0, 0, 0, 0, _width, _height, _hFOV, _vFOV);
    return ret;
}

int nativeConfigure(long thiz) {
    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->Configure();
    return ret;
}

int nativeStart(long thiz) {
    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->Start();
    return ret;
}

int nativeStop(long thiz) {
    int ret = 0;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->Stop();
    return ret;
}

int nativeProcessFrame(long thiz, long rImage) {
    int ret = 0;
    Image img;
    Matrix mat;
    mat = *(cv::Mat *) rImage;
    img.SetImageData(mat);
    ret = ((ArTMaxConfiguratorPlus *) thiz)->ProcessFrame(img);
    return ret;
}

int nativeGetRTMatrix(long thiz, long rRTMatrixOut) {
    LOGD("ArTMaxConfiguratorPlus_interop: INIT");

//    LOGD("Java_net_joinpad_brainpadrecognizerx_wrapper_BrainPadRecognizerX_nativeGetRTMatrixes");
    int ret = 0;
    pair<int, Matrix> rtMatrix;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->GetRTMatrix(rtMatrix);



    if (rtMatrix.second.cols == 4 && rtMatrix.second.rows == 4) {

        cv::Mat tmp((4 + 1), 4, CV_64FC1);
        Matrix tmp2(4, 4, CV_64FC1);

//    multimap<int, Matrix>::iterator it = rtMatrixes.begin();
//    for (it = rtMatrixes.begin(); it != rtMatrixes.end(); ++it) {
        LOGD("ArTMaxConfiguratorPlus_interop: RT Matrix : ObjId: %d", rtMatrix.first);

        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j) {
                LOGD("ArTMaxConfiguratorPlus_interop: RT Matrix Befor ConvertRHSToLHS (%d,%d): %f", i, j, rtMatrix.second.at<double>(i, j));
            }

        tmp2 = sapphire::utils::ConvertRHSToLHS(rtMatrix.second);

        tmp.at<double>(0, 0) = rtMatrix.first;
        tmp.at<double>(0, 1) = 0;
        tmp.at<double>(0, 2) = 0;
        tmp.at<double>(0, 3) = 0;
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j) {
                tmp.at<double>(i + 1, j) = tmp2.at<double>(i, j);
                LOGD("ArTMaxConfiguratorPlus_interop: it->second RT Matrixes (%d,%d): %f", i, j, rtMatrix.second.at<double>(i, j));
                LOGD("ArTMaxConfiguratorPlus_interop: tmp2 RT Matrixes (%d,%d): %f", i, j, tmp2.at<double>(i, j));
                LOGD("ArTMaxConfiguratorPlus_interop: AAAA: %d %d", i, j);
            }

        *(cv::Mat *) rRTMatrixOut = tmp;

    } else {
        cv::Mat tmp;
        *(cv::Mat *) rRTMatrixOut = tmp;
    }

    LOGD("ArTMaxConfiguratorPlus_interop: END");
    return ret;
}



int nativeGetMarkersRTMatrixes(long thiz, long rRTMatrixesOut) {
    LOGD("ArTMaxConfiguratorPlus_interop: INIT");

//    LOGD("Java_net_joinpad_brainpadrecognizerx_wrapper_BrainPadRecognizerX_nativeGetRTMatrixes");
    int ret = 0;
    multimap<int, Matrix> rtMatrixes;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->GetMarkersRTMatrixes(rtMatrixes);

    int numMarkers=0;
    multimap<int,Matrix>::iterator it;

    int k=0;
    for (it=rtMatrixes.begin();it!=rtMatrixes.end();it++) {
        if (it->second.cols == 4 && it->second.rows == 4) {

            cv::Mat tmp((4 + 1), 4, CV_64FC1);
            Matrix tmp2(4, 4, CV_64FC1);

//    multimap<int, Matrix>::iterator it = rtMatrixes.begin();
//    for (it = rtMatrixes.begin(); it != rtMatrixes.end(); ++it) {
            LOGD("ArTMaxConfiguratorPlus_interop: RT Matrix : MarkerID: %d", it->first);

            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j) {
                    LOGD("ArTMaxConfiguratorPlus_interop: RT Matrix Befor ConvertRHSToLHS (%d,%d): %f",
                         i, j, it->second.at<double>(i, j));
                }

            tmp2 = sapphire::utils::ConvertRHSToLHS(it->second);

            ((cv::Mat *) rRTMatrixesOut)->at<double>(k*5+0, 0) = it->first;
            ((cv::Mat *) rRTMatrixesOut)->at<double>(k*5+0, 1) = 0;
            ((cv::Mat *) rRTMatrixesOut)->at<double>(k*5+0, 2) = 0;
            ((cv::Mat *) rRTMatrixesOut)->at<double>(k*5+0, 3) = 0;
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j) {
                    ((cv::Mat *) rRTMatrixesOut)->at<double>(k*5+i + 1, j) = tmp2.at<double>(i, j);
                    LOGD("ArTMaxConfiguratorPlus_interop: it->second Marker RT Matrixes (%d,%d): %f", i, j,
                         it->second.at<double>(i, j));
                    LOGD("ArTMaxConfiguratorPlus_interop: tmp2 Marker RT Matrixes (%d,%d): %f", i, j,
                         tmp2.at<double>(i, j));
                }

            numMarkers++;
//            *(cv::Mat *) rRTMatrixesOut = tmp;

        } else {
            cv::Mat tmp;
            *(cv::Mat *) rRTMatrixesOut = tmp;
        }
        k++;
    }

//    (*(int*)rNumMarkersOut)=numMarkers;

    LOGD("ArTMaxConfiguratorPlus_interop: END");
    return numMarkers;
}


//TEST - START

int nativeSetInferenceGraphBuffer(long  thiz, long rInferenceGraphBuffer,int inferenceGraphBufferLength,long rInferenceGraphConfigBuffer,int inferenceGraphConfigBufferLength,long rObjectLabelsBuffer,int objectLabelsBufferLength)
{
    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer START");
    int ret=0;


    char* pInferenceGraphBuffer=(char*)rInferenceGraphBuffer;
    char* pInferenceGraphConfigBuffer=(char*)rInferenceGraphConfigBuffer;
    char* pObjectLabelsBuffer=(char*)rObjectLabelsBuffer;

//    char *pInferenceGraphBuffer=(char*)malloc(sizeof(char)*inferenceGraphBufferLength);
//    memcpy(pInferenceGraphBuffer,(char*)rInferenceGraphBuffer,sizeof(char)*inferenceGraphBufferLength);
//    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer - 1 %d %d",pInferenceGraphBuffer,inferenceGraphBufferLength);
//
//    char *pInferenceGraphConfigBuffer=(char*)malloc(sizeof(char)*inferenceGraphConfigBufferLength);
//    memcpy(pInferenceGraphConfigBuffer,(char*)rInferenceGraphConfigBuffer,sizeof(char)*inferenceGraphConfigBufferLength);
//    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer - 2 %d %d",pInferenceGraphConfigBuffer,inferenceGraphConfigBufferLength);
//    cv::String str(pInferenceGraphConfigBuffer,inferenceGraphConfigBufferLength);
//    for (int i=0;i<str.size();i++) {
//        LOGD("ArTMaxConfiguratorPlus_interop::Loading .pbtxt: %c", str[i]);
//    }
//    char *pObjectLabelsBuffer=(char*)malloc(sizeof(char)*objectLabelsBufferLength);
//    memcpy(pObjectLabelsBuffer,(char*)rObjectLabelsBuffer,sizeof(char)*objectLabelsBufferLength);
//    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer - 3 %d %d",pObjectLabelsBuffer,objectLabelsBufferLength);

    ret = ((ArTMaxConfiguratorPlus *) thiz)->SetInferenceGraphBuffer(pInferenceGraphBuffer,inferenceGraphBufferLength,pInferenceGraphConfigBuffer,inferenceGraphConfigBufferLength,pObjectLabelsBuffer,objectLabelsBufferLength);

//    string inferenceGraphBuffer(pInferenceGraphBuffer,inferenceGraphBufferLength);
//    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer %s",inferenceGraphBuffer.c_str());
//    string inferenceGraphConfigBuffer(pInferenceGraphConfigBuffer,inferenceGraphConfigBufferLength);
//    string objectLabelsBuffer(pObjectLabelsBuffer,objectLabelsBufferLength);
//    ret = ((ArTMaxConfiguratorPlus *) thiz)->SetInferenceGraphBuffer(inferenceGraphBuffer,inferenceGraphConfigBuffer,objectLabelsBuffer);
//    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer %s",objectLabelsBuffer.c_str());
    LOGD("ArTMaxConfiguratorPlus_interop: nativeSetInferenceGraphBuffer END");
    return ret;
}


int nativeGetDetectedClassifiedObjects(long thiz,long rDetectedObjectsOut)
{
    LOGD("ArTMaxConfiguratorPlus_interop: INIT");

    int ret = 0;
    multimap<int, Patch2D> detectedObjects;
    ret = ((ArTMaxConfiguratorPlus *) thiz)->GetDetectedClassifiedObjects(detectedObjects);

    int numDetectedObjects = detectedObjects.size();

    LOGD("ArTMaxConfiguratorPlus_interop: Num Detected Classified Objects: %d", numDetectedObjects);


//    cv::Mat tmp(numDetectedObjects  * (1 + 1), 4, CV_64FC1);

    int k = 0;
    multimap<int, Patch2D>::iterator it = detectedObjects.begin();
    for (it = detectedObjects.begin(); it != detectedObjects.end(); ++it) {
        LOGD("ArTMaxConfiguratorPlus_interop: detected classified Objects : ObjId: %d", it->first);

        ((cv::Mat *) rDetectedObjectsOut)->at<double>((1 + 1) * k, 0) = it->first;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>((1 + 1) * k, 1) =0;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>((1 + 1) * k, 2) =0;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>((1 + 1) * k, 3) =0;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 0) = it->second.shape2D_.GetShape()[0].x_;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 1) = it->second.shape2D_.GetShape()[0].y_;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 2) = it->second.shape2D_.GetShape()[1].x_;
        ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 3) = it->second.shape2D_.GetShape()[1].y_;
        LOGD("ArTMaxConfiguratorPlus_interop: Obj Box: (%d,%d) (%d,%d)",((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 0), ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 1),
             ((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 2),((cv::Mat *) rDetectedObjectsOut)->at<double>(((1 + 1) * k) + 1, 3));
    }

//    *(cv::Mat *) rDetectedObjectsOut = tmp;

    LOGD("ArTMaxConfiguratorPlus_interop: nativeGetDetectedClassifiedObjects END");

    return ret;
}

//TEST - END

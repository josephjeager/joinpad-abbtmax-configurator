/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArTMaxConfiguratorPlus.h
 * Author: marzorati
 *
 * Created on June 21, 2017, 3:48 PM
 */

#ifndef ARTMAXCONFIGURATORPLUS_H
#define ARTMAXCONFIGURATORPLUS_H

#define _USE_MATH_DEFINES

#include <vector>
#include <iostream>
#include <iomanip>
#include <math.h>


#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"


#include "cv/Detector.h"
#include "cv/Feature2D.h"
#include "cv/FMatch.h"
#include "cv/OCVFeatureDetector.h"
#include "cv/OCVFeatureDescriptor.h"
#include "cv/OCVFlannFeatureMatcher.h"
#include "math/Homography.h"
#include "core/Time.h"
#include "core/Thread.h"
#include "cv/Matcher.h"
#include "cv/OCVBFFeatureMatcher.h"
#include "cv/ObjectDetector.h"
#include "cv/MultiObjectDetectorTrackerAndPose.h"
#include "cv/OCVMultiObjectClassificator.h"
#include "cv/MarkerRecognizer.h"
#include "cv/P3PPoseEstimator.h"
#include "core/Info.h"
#include "utils/DrawingTools.h"

#include "ArTMaxConfiguratorPlusConfig.h"

#define LOG_TAG "ArTMaxConfiguratorPlus"

//#define _DRAW

//#ifdef _DEBUG
#ifdef _ANDROID
//#include <android/log.h>
//#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#else
//#define LOGI(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGD(...) printf(__VA_ARGS__);printf("\n")//((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
//#endif
//#else
//#define LOGI(...)
//#define LOGD(...)
#endif

#define FROM_FILE 0
#define FROM_STRING 1


#define INIT_POSE_PHI   -M_PI/2.0
#define INIT_POSE_GAMMA 0
#define INIT_POSE_THETA -M_PI
#define INIT_POSE_X     0
#define INIT_POSE_Y     0
#define INIT_POSE_Z     1


#define MAX_MARKER_LIVES    20

using namespace sapphire;


class Marker
{
public:

    Marker():rtC_M_(4,4,CV_64F)
    {}

    int detectedMarkerID_=-1;
    int markerLives_=-1;
    double timestamp_=-1;
    int numDetections_=0;

    Matrix rtC_M_;

    double dimX_=0;
    double dimY_=0;
};


class ArTMaxConfiguratorPlus {
public:
    ArTMaxConfiguratorPlus(string _configString,int _loadType);

    virtual ~ArTMaxConfiguratorPlus();
    
    int Init();
//    int EvaluateReferenceObjectQuality(Image& _imgObj,int _numHBuckets,int _numVBuckets,vector<Feature2D> & _keypointsOut,vector<int>& _numValuesOut,vector<Point2D<float> >& _meanValuesOut,vector<Point2D<float> > & _varValuesOut);

    int AddNewObjectToDetect(int _objID,std::vector<Point3D<float> > & _shape3D,std::vector< std::vector<int> >& _facesIdxs,std::vector< std::vector<Point2D<float> > >& _shapes2D,Image& _imgObj,string _objName);

    int Configure();
    std::string BuildDefaultConfigString();

    int ProcessFrame(Image& _frame);

    int GetReferenceObjects(vector<Patch2D>& _refObjsOut);
    int GetMeshReferenceObjects(map<int,Mesh>& _refMeshObjsOut);

    int GetDetectedObjects(multimap<int,Shape2D >& _detectedObjsOut);
    int GetHomography(pair<int,Matrix >& _homographyOut);
    int GetRTMatrix(pair<int, Matrix>& _rtMatrixOut);

    int GetMarkersDetectedObjects(multimap<int, Shape2D>& _detectedObjsOut);
    int GetMarkersHomographies(multimap<int, Matrix>& _homographiesOut);
    int GetMarkersRTMatrixes(multimap<int, Matrix>& _rtMatrixesOut);


    //TEST - START
    int GetDetectedClassifiedBoxes(multimap<int, Shape2D>& _detectedBoxesOut);
    int GetDetectedClassifiedObjects(multimap<int, Patch2D>& _detectedObjectsOut);
    int SetInferenceGraphBuffer(char* _pInferenceGraphBuffer,int _inferenceGraphBufferLength,char* _pInferenceGraphConfigBuffer,int _inferenceGraphConfigBufferLength,char* _pObjectLabelsBuffer,int _objectLabelsBufferLength);

    //TEST - END


    int Start();
    int Stop();

    int SetCameraParams(double _fx,double _fy,double _cx, double _cy,int _width,int _height,double _hFOV=0,double _vFOV=0,double _k1=0,double _k2=0,double _k3=0,double _p1=0,double _p2=0);

    int GetImage(Image& _imageOut);

    int SetImage(Image& _image);

private:
    int DetectMarkers(Image & _image,multimap<int,Marker>& _markersOut);
    int UpdateMarkers(multimap<int,Marker>& _markers);
    bool CheckObjectPose(Matrix& _rtW_C,double _angleToll,double _toll,double _maxDistance);
    
private:
    
    std::string configFilename_;
    std::string yamlString_;
    cv::FileStorage configFile_;



    double angleToll_,maxDistance_,toll_;

    MultiObjectDetectorTrackerAndPose multiObjDetectorTrackerAndPose_;
    MarkerRecognizer markerRecognizer_;

    //TEST
    OCVMultiObjectClassificator multiObjectClassificator_;

    vector<Image> frames_;

    Image imgScene_, imgScene_tmp_,imgSceneOut_;
    ObjectDetector objDetector_;
    MultiObjectTracker multiTracker_;
    P3PPoseEstimator poseEstimator_;

    multimap<int,Marker> detectedMarkers_;
    
    std::vector < std::vector<sapphire::Feature2D> > trainKeyPointsVec_;
    std::vector< Matrix > descriptors_objectVec_;

    pair<int, Matrix> detectedRT_;
    pair<int, Matrix> detectedHomography_;
//    pair<int, Patch2D> detectedObject_;
    bool objectDetected_;
    bool isValidRT_;
    bool isValidH_;
//    bool isValidObj_;
   
//    OCVFeatureDetector detectorObj_;
//    OCVFeatureDescriptor descriptorObj_;
//
//    Detector detector_;
//
//    OCVFlannFeatureMatcher keypoint_matcher_;
//
//    Matcher matcher_;

    int numFrame_;
    
//    Camera camera_;

    //cv::VideoCapture video_;
    string fileNameScene_;
    
    std::vector<Patch2D> refObjs_;
    std::map<int,Mesh> meshRefObjs_;
    
    std::map<int,Info> refObjName_;

    bool isTracking_,isInit_,isConfigurated_,isStarted_;
    cv::FileNode fileNode_;
    bool loadType_;

    //OLD
//    Image imgScene_, imgScene_tmp_,imgSceneOut_;
//    ObjectDetector objDetector_;
//    
//    std::vector < std::vector<Feature2D> > trainKeyPointsVec_;
//    std::vector< Matrix > descriptors_objectVec_;
//    vector<Image> imgObjsVec_;
//    
//    OCVFeatureDetector detectorObj_;
//    OCVFeatureDescriptor descriptorObj_;
//
//    Detector detector_;
//    
//    OCVFlannFeatureMatcher keypoint_matcher_;
//
//    Matcher matcher_;
//    
// Camera camera_;
//    ObjectTracker objTracker_;
//    VISPTracker tracker_;
//    VISPPoseEstimator poseEstimator_;
//    
// //   cv::VideoCapture video_;
//    string fileNameScene_;
//    
//    bool isTracking_;
//    bool isInit_,isConfigurated_,isStarted_;

};

#endif /* ARTMAXCONFIGURATORPLUS_H */


//
// Created by marzorati on 1/19/18.
//

#ifndef ARTMAXCONFIGURATORCONFIG_H
#define ARTMAXCONFIGURATORCONFIG_H

#define DEFAULT_CONFIG_STRING  "%YAML:1.0\n"\
                                "---\n"\
                                "artmaxconfigurator:\n"\
                                "   numObjectsToDetect: 0\n"\
                                "   objectModelFilenames: \"/storage/emulated/0/projects/BrainPadRecognizerX/data/board_1_android.yaml\"\n"\
                                "   multiObjDetectorTrackerAndPoseConfigFilename : \"multiobjectdetectortrackerandpose\"\n"\
                                "   objectDetectorConfigFilename: \"objectdetector\"\n"\
                                "   multiObjectTrackerConfigFilename: \"multitracker\"\n"\
                                "   poseEstimatorConfigFilename: \"poseestimator\"\n"\
                                "   enableCheckObjectPose: 1\n"\
                                "   angleToll: 0.2\n"\
                                "   toll: 0.05\n"\
                                "   maxDistance: 14.0\n"\
                                "   cameraFilename: \"camera640x480\"\n"\
                                "camera640x480:\n"\
                                "   fx: 800\n"\
                                "   fy: 800\n"\
                                "   cx: 320\n"\
                                "   cy: 240\n"\
                                "   k1: 0\n"\
                                "   k2: 0\n"\
                                "   k3: 0\n"\
                                "   hFOV: 50\n"\
                                "   vFOV: 50\n"\
                                "   frameWidth: 640\n"\
                                "   frameHeight: 480\n"\
                                "klttracker :\n"\
                                "   maxCount: 500\n"\
                                "   winSize: 10\n"\
                                "   qualityLevel: 0.01\n"\
                                "   minDistance: 5\n"\
                                "   minEigThreshold: 0.0001\n"\
                                "   harrisK: 0.04\n"\
                                "   blockSize: 3\n"\
                                "   useHarrisDetector: 1\n"\
                                "   pyrMaxLevel: 3\n"\
                                "   cornerRefinementEnabled: 0\n"\
                                "   initialGuess: 0\n"\
                                "   maxNumOnlyTrackedProcessedFrame: 500\n"\
                                "   minNumPointsToTrack: 20\n"\
                                "   maxOpticalFlowErrThresh: 10.0\n"\
                                "   minNumPointsToTrackMultiplier: 5\n"\
                                "   maxMillisToFindHomography: 50\n"\
                                "   cameraFilename: \"camera640x480\"\n"\
                                "   enableCheckDetection: 0\n"\
                                "multiobjectdetectortrackerandpose:\n"\
                                "   enableReInitTracking: 0\n"\
                                "multitracker:\n"\
                                "   trackerType: 3\n"\
                                "   trackerConfigFilename: \"klttracker\"\n"\
                                "poseestimator:\n"\
                                "   poseEstimatorMethod: \"ITERATIVE\"\n"\
                                "   cameraFilename: \"camera640x480\"\n"\
                                "objectdetector:\n"\
                                "   featureDetectorType: 1\n"\
                                "   featureDetectorConfigFilename: \"ocvfeaturedetector\"\n"\
                                "   featureDescriptorType: 1\n"\
                                "   featureDescriptorConfigFilename: \"ocvfeaturedescriptor\"\n"\
                                "   featureMatcherType: 2\n"\
                                "   featureMatcherConfigFilename: \"ocvbffeaturematcher\"\n"\
                                "   minValidMatches: 300\n"\
                                "   enableMultiInstance: 0\n"\
                                "   checkDetectionThresh: 5.0\n"\
                                "   enableCheckDetection: 0\n"\
                                "   numMinTrainKeyPoints: 10\n"\
                                "ocvfeaturedescriptor:\n"\
                                "   descriptorType: \"BRISK\"\n"\
                                "   thresh: 30\n"\
                                "   octaves: 3\n"\
                                "   patternScale: 1.0\n"\
                                "lbdfeaturedescriptor:\n"\
                                "   numOfOctave: -1\n"\
                                "   widthOfBand: -1\n"\
                                "   reductionRatio: -1\n"\
                                "ocvfeaturedetector:\n"\
                                "   detectorType: \"BRISK\"\n"\
                                "   thresh: 30\n"\
                                "   octaves: 3\n"\
                                "   patternScale: 1.0\n"\
                                "lbdfeaturedetector:\n"\
                                "   numOfOctave: -1\n"\
                                "   widthOfBand: -1\n"\
                                "   reductionRatio: -1\n"\
                                "ocvflannfeaturematcher:\n"\
                                "   tableNumber: 10\n"\
                                "   keySize: 10\n"\
                                "   multiProbeLevel: 2\n"\
                                "   checks: 50\n"\
                                "   eps: 0\n"\
                                "   sorted: 1\n"\
                                "   numQueryMatches: 2\n"\
                                "ocvbffeaturematcher:\n"\
                                "   normType: 6\n"\
                                "   crossCheck: 1\n"\
                                "lbdfeaturematcher:\n"\
                                "   numQueryMatches: 2"
#endif //ARTMAXCONFIGURATORCONFIG_H
